﻿//############################################################################################

/* Title: Embedded External EEPROM (aka EEEPROM) peripheral driver
See also: <https://www.iqrf.org/DpaTechGuide/>
*/

/*DriverDescription
{ 'ID' : 0x04,
  'Type' : 'Standard',
  'Internal' : false,
  'Versions' : [
    { 'Version' : 0, 'Notes' : [
        'Initial release',
        ]
    }
  ] }
DriverDescription*/

"use strict";
// Namespace: iqrf.embed.eeeprom
namespace( 'iqrf.embed.eeeprom' );

// --------------
/* Function: iqrf.embed.eeeprom.Read_Request
Encodes DPA request to read from memory.

Parameters:
  address - number: Memory address to read from.
  len - number: Length of data to read.

Returns:
  iqrf.DpaRawHdpMessage: DPA request object.
*/
iqrf.embed.eeeprom.Read_Request = function ( address, len )
{
  return new iqrf.DpaRawHdpMessage( iqrf.PNUM_EEEPROM, '02', iqrf.IntToHexStringBytesArray( address, 2 ) + '.' + iqrf.ToHexStringByte( len ) );
};

/* Function: iqrf.embed.eeeprom.Read_Response
Decodes DPA response from reading from memory.

Parameters:
  response - iqrf.DpaRawHdpMessage: DPA response object.

Returns:
  array: Read data.
*/
iqrf.embed.eeeprom.Read_Response = function ( response )
{
  return iqrf.CheckResponsePnumPcmdDlen( response, iqrf.PNUM_EEEPROM, '82' );
};

// --------------
/* Function: iqrf.embed.eeeprom.Write_Request
Encodes DPA request to write to memory.

Parameters:
  address - number: Address to write data to.
  pdata - number: Data to write.

Returns:
  iqrf.DpaRawHdpMessage: DPA request object.
*/
iqrf.embed.eeeprom.Write_Request = function ( address, pdata )
{
  return new iqrf.DpaRawHdpMessage( iqrf.PNUM_EEEPROM, '03', iqrf.IntToHexStringBytesArray( address, 2 ) + iqrf.BytesToHexStringBytesArray( pdata, true ) );
};

/* Function: iqrf.embed.eeeprom.Write_Response
Decodes DPA response from writing to memory.

Parameters:
  response - iqrf.DpaRawHdpMessage: DPA response object.
*/
iqrf.embed.eeeprom.Write_Response = function ( response )
{
  iqrf.CheckResponsePnumPcmdDlen( response, iqrf.PNUM_EEEPROM, '83', 0 );
};

//############################################################################################
