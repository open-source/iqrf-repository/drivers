﻿//############################################################################################

/* Title: Embedded Thermometer peripheral driver
See also: <https://www.iqrf.org/DpaTechGuide/>
*/

/*DriverDescription
{ 'ID' : 0x0A,
  'Type' : 'Standard',
  'Internal' : false,
  'Versions' : [
    { 'Version' : 0, 'Notes' : [
        'Initial release',
        ]
    }
  ] }
DriverDescription*/

"use strict";
// Namespace: iqrf.embed.thermometer
namespace( 'iqrf.embed.thermometer' );

/* Function: iqrf.embed.thermometer.Read_Request
Encodes DPA request to read temperature.

Returns:
  iqrf.DpaRawHdpMessage: DPA request object.
*/
iqrf.embed.thermometer.Read_Request = function ()
{
  return new iqrf.DpaRawHdpMessage( iqrf.PNUM_Thermometer, '00' );
};

/* Function: iqrf.embed.thermometer.Read_Response
Decodes DPA response from reading temperature.

Parameters:
  response - iqrf.DpaRawHdpMessage: DPA response object.

Return:
  number: Temperature value.
*/
iqrf.embed.thermometer.Read_Response = function ( response )
{
  var responseData = iqrf.CheckResponsePnumPcmdDlen( response, iqrf.PNUM_Thermometer, '80', 3 );
  if ( responseData[0] === 0x80 )
    return -128.0;

  return iqrf.Uncomplement( responseData[1] + ( responseData[2] * 0x100 ), 12 ) / 16.0;
};

//############################################################################################
