﻿//############################################################################################

/* Title: Global IQRF driver library routines
See also: <https://www.iqrf.org/DpaTechGuide/>

Because of potential usage of JScript at *Microsoft.ClearScript.Windows.JScriptEngine* the following features cannot be used:

* 'let' instead of 'var' where appropriate
* 'const'
* default parameter values
* binary literals
* enums
*/

/*
ToDo
*/

/*DriverDescription
{ 'ID' : -1,
  'Type' : 'Standard',
  'Internal' : false,
  'Versions' : [
    { 'Version' : 0, 'Notes' : [
        'Initial release' ]
    }
  ] }
DriverDescription*/

"use strict";

// JavaScript 'namespace' helper routine
var globalObject = this;

/* Function: namespace
Declares a new namespace.

Parameters:
  name - string: Namespace name. Use dots for multilevel namespace.
*/
var namespace = function ( name )
{
  var tokens = name.split( '.' );
  var object = globalObject;
  while ( tokens.length > 0 )
  {
    var token = tokens.shift();
    object = object[token] = object[token] || {};
  }
  return object;
};

//############################################################################################

// Array.isArray is not defined at JScript
if ( Array.isArray === undefined )
{
  Array.isArray = function ( obj )
  {
    return obj.constructor === Array;
  };
}

//############################################################################################
// Namespace: iqrf
namespace( 'iqrf' );

/* String: iqrf.PNUM_Coordinator
Coordinator peripheral byte value as a hexadecimal string.
*/
iqrf.PNUM_Coordinator = '00';
/* String: iqrf.PNUM_Node
Node peripheral byte value as a hexadecimal string.
*/
iqrf.PNUM_Node = '01';
/* String: iqrf.PNUM_OS
OS peripheral byte value as a hexadecimal string.
*/
iqrf.PNUM_OS = '02';
/* String: iqrf.PNUM_EEPROM
EEPROM peripheral byte value as a hexadecimal string.
*/
iqrf.PNUM_EEPROM = '03';
/* String: iqrf.PNUM_EEEPROM
External EEPROM peripheral byte value as a hexadecimal string.
*/
iqrf.PNUM_EEEPROM = '04';
/* String: iqrf.PNUM_RAM
RAM peripheral byte value as a hexadecimal string.
*/
iqrf.PNUM_RAM = '05';
/* String: iqrf.PNUM_LEDR
Red LED peripheral byte value as a hexadecimal string.
*/
iqrf.PNUM_LEDR = '06';
/* String: iqrf.PNUM_LEDG
Green LED peripheral byte value as a hexadecimal string.
*/
iqrf.PNUM_LEDG = '07';
/* String: iqrf.PNUM_SPI
SPI peripheral byte value as a hexadecimal string.
*/
iqrf.PNUM_SPI = '08';
/* String: iqrf.PNUM_IO
IO peripheral byte value as a hexadecimal string.
*/
iqrf.PNUM_IO = '09';
/* String: iqrf.PNUM_Thermometer
Thermometer peripheral byte value as a hexadecimal string.
*/
iqrf.PNUM_Thermometer = '0A';
/* String: iqrf.PNUM_PWM
PWM peripheral byte value as a hexadecimal string.
*/
iqrf.PNUM_PWM = '0B';
/* String: iqrf.PNUM_UART
UART peripheral byte value as a hexadecimal string.
*/
iqrf.PNUM_UART = '0C';
/* String: iqrf.PNUM_FRC
FRC peripheral byte value as a hexadecimal string.
*/
iqrf.PNUM_FRC = '0D';
/* String: iqrf.PNUM_Enumeration
Enumeration peripheral byte value as a hexadecimal string.
*/
iqrf.PNUM_Enumeration = 'FF';

// PCMD response flag
iqrf.PCMD_ResponseFlag = 0x80;

/* Constructor: iqrf.DpaRawHdpMessage
Creates a raw-hdp DPA message with specified PNUM and PCMD.

Parameters:
  pnum - string: PNUM value.
  pcmd - string: PCMD value.
  rdata - string: [optional] PDATA value as a string of dot separated bytes.
*/

iqrf.DpaRawHdpMessage = function ( pnum, pcmd, rdata )
{
  // 'Asserts'
  iqrf.ParseStringByte( pnum );
  iqrf.ParseStringByte( pcmd );

  this.pnum = pnum;
  this.pcmd = pcmd;

  this.ctype = 'dpa';
  this.type = 'raw-hdp';
  this.hwpid = 'FfFf';

  if ( rdata !== undefined )
  {
    if ( typeof rdata !== 'string' )
      throw new Error( 'iqrf.DpaRawHdpMessage: Optional string parameter rdata is defined but type is ' + typeof rdata );

    if ( rdata.length !== 0 )
    {
      // 'Assert'
      iqrf.ParseStringBytes( rdata );

      this.rdata = rdata;
    }
  }
};

/* Constructor: iqrf.DpaRawHdpMessageCoordinator
Same as <iqrf.DpaRawHdpMessage> but it also sets nadr to '00'.
*/
iqrf.DpaRawHdpMessageCoordinator = function ( pnum, pcmd, rdata )
{
  var result = new iqrf.DpaRawHdpMessage( pnum, pcmd, rdata );
  result.nadr = '00';
  return result;
};

/* Function: iqrf.stricmp
Compares two string in case insensitive way (not using locale).

Parameters:
  str1 - string: 1st string to compare.
  str2 - string: 2nd string to compare.

Returns:
  boolean: true if strings are equal, otherwise false.
*/
iqrf.stricmp = function ( str1, str2 )
{
  return str1.toUpperCase() === str2.toUpperCase();
};

/* Function: iqrf.indexOf
Returns the index of the first occurrence of a value in an array.
Reason for the implementation is that some engines (JScript) does not support Array.indexOf().

Parameters:
  arr - array: An array.
  val - object: The value to locate in arr.
  useStricmp - boolean: [optional] if true then iqrf.stricmp() is used for the object match. Default is false.

Returns:
  number: The index of the first occurrence of val in the arr, or -1 if val is not found.
*/
iqrf.indexOf = function ( arr, val, useStricmp )
{
  // Test if the indexOf() is supported (e.g. JScriptEngine does not, V8ScriptEngine does)
  if ( arr.indexOf !== undefined && useStricmp !== true )
    return arr.indexOf( val );

  var arr_length = arr.length;
  for ( var indexOfResult = 0; indexOfResult < arr_length; indexOfResult++ )
    if ( useStricmp === true )
    {
      if ( iqrf.stricmp( arr[indexOfResult], val ) )
        return indexOfResult;
    }
    else
    {
      if ( arr[indexOfResult] === val )
        return indexOfResult;
    }

  return -1;
};

/* Function: iqrf.CheckResponsePnumPcmdDlen
Checks DPA Response.

Parameters:
  response - object: DPA Response message (raw-hdp format)
  pnum - string: Required PNUM, e.g. '01'.
  pcmd - string: Required PCMD(s), e.g. '03', '04.05'.
  dlen - string: [optional] Required PDATA (rdata part) length. Negative number specifies a minimum length. When not used (undefined), then no check is performed.

Returns:
  array: Array of bytes containing rdata bytes from the original DPA Response message.
*/
iqrf.CheckResponsePnumPcmdDlen = function ( response, pnum, pcmd, dlen )
{
  if ( response.rcode !== '00' )
    throw new Error( 'iqrf.CheckResponsePnumPcmdDlen: Field response.rcode is not \'00\' but \'' + response.rcode + '\', status=' + response.status );

  if ( !iqrf.stricmp( response.pnum, pnum ) )
    throw new Error( 'iqrf.CheckResponsePnumPcmdDlen: Invalid pnum=' + response.pnum + ', expected ' + pnum );

  if ( -1 === iqrf.indexOf( pcmd.split( '.' ), response.pcmd, true ) )
    throw new Error( 'iqrf.CheckResponsePnumPcmdDlen: Invalid pcmd=' + response.pcmd + ', expected ' + pcmd );

  var result = iqrf.ParseRdata( response );
  if ( dlen !== undefined )
  {
    if ( dlen >= 0 )
    {
      if ( result.length !== dlen )
        throw new Error( 'iqrf.CheckResponsePnumPcmdDlen: Invalid response length=' + result.length + ', expected ' + dlen );
    }
    else
      if ( result.length < -dlen )
        throw new Error( 'iqrf.CheckResponsePnumPcmdDlen: Invalid response length=' + result.length + ', expected at least ' + -dlen );
  }

  return result;
};

/* Function: iqrf.ParseStringByte
Converts hexadecimal byte from string into a number.

Parameters:
  byte - string: string to convert.

Returns:
  number: Byte value of the string.
*/
iqrf.ParseStringByte = function ( byte )
{
  if ( typeof byte !== 'string' )
    throw new Error( 'ParseStringByte: Parameter byte expected string but is ' + typeof byte );

  if ( byte.length !== 2 )
    throw new Error( 'ParseStringByte: Parameter byte expected 2 characters long but is ' + byte.length + ' characters long' );

  var result = parseInt( byte, 16 );
  if ( isNaN( result ) )
    throw new Error( 'ParseStringByte: Parameter byte ' + byte + ' cannot be converted from hexadecimal' );

  return result;
};

/* Function: iqrf.ParseStringBytes
Splits dot separated bytes from string into array of bytes.

Parameters:
  bytes - string: [optional] Input string with bytes.

Returns:
  array: Array of bytes containing bytes from the string. If parameter bytes is not used an ampty array is returned.
*/
iqrf.ParseStringBytes = function ( bytes )
{
  var result = [];
  if ( bytes !== undefined )
  {
    if ( typeof bytes !== 'string' )
      throw new Error( 'iqrf.ParseStringBytes: Type of parameter bytes is not string, but ' + typeof bytes );

    if ( bytes.length !== 0 )
    {
      var bytesSplit = bytes.split( '.' );
      while ( bytesSplit.length > 0 )
        result[result.length] = iqrf.ParseStringByte( bytesSplit.shift() );
    }
  }

  return result;
};

/* Function: iqrf.ParseRdata
Splits rdata from DPA message (raw-hdp format) into array of bytes.

Parameters:
  response - object: DPA message (raw-hdp format).

Returns:
  array: Array of bytes containing rdata bytes from the original DPA message.
*/
iqrf.ParseRdata = function ( response )
{
  return iqrf.ParseStringBytes( response.rdata );
};

/* Function: iqrf.ToHexStringByte
Converts byte value to the 2 character long hexadecimal string.

Parameters:
  value - number: Input byte value.

Returns:
  string: 2 character long hexadecimal string.
*/
iqrf.ToHexStringByte = function ( value )
{
  if ( typeof value !== 'number' )
    throw new Error( 'iqrf.ToHexStringByte: Parameter value is not number but ' + typeof value );

  if ( value < 0 || value > 0xFF )
    throw new Error( 'iqrf.ToHexStringByte: Parameter value ' + String( value ) + ' out of range' );

  var result = value.toString( 16 );
  if ( result.length !== 2 )
    result = '0' + result;
  return result;
};

/* Function: iqrf.IntToHexStringBytesArray
Converts integer value (unsigned by default) to the hexadecimal "dot separated values" string of the specified byte-length containing the bytes the little-endian value consists of.

Parameters:
  value - number: Input integer value.
  length - number: Number of bytes the integer consists of.
  signed - number: [optional] Non-zero or true to allow negative value.

Returns:
  string: String list of the individual bytes.
*/
iqrf.IntToHexStringBytesArray = function ( value, length, signed )
{
  if ( typeof value !== 'number' )
    throw new Error( 'iqrf.IntToHexStringBytesArray: Parameter value is not number but ' + typeof value );

  if ( Boolean( signed ) === false && value < 0 )
    throw new Error( 'iqrf.IntToHexStringBytesArray: Parameter value is negative' );

  if ( length < 1 )
    throw new Error( 'iqrf.IntToHexStringBytesArray: Parameter length is invalid: ' + length );

  var result = '';
  do
  {
    if ( result.length !== 0 )
      result += '.';
    result += iqrf.ToHexStringByte( value & 0xFF );

    value >>= 8;
  }
  while ( --length !== 0 );

  // ToDo: Improve (e.g. 2197816638 >> 8 == -8191995)
  if ( value !== 0 && value !== -1 )
    throw new Error( 'iqrf.IntToHexStringBytesArray: Parameter value is out of range' );

  return result;
};

/* Function: iqrf.BytesToHexStringBytesArray
Converts array of bytes to the string representation (list of dot separated 2 character long byte values).

Parameters:
  bytes - array: Array of bytes.
  prefixDot - boolean: [optional] If true then the result is always prefixed by a dot character. Default is false.
  startIndex - number: [optional] Staring index of the byte to convert from. Default is 0.
  endIndex - number: [optional] Ending index of the byte to convert to. Default is bytes.length - 1.

Returns:
  string: String list of the individual bytes.
*/
iqrf.BytesToHexStringBytesArray = function ( bytes, prefixDot, startIndex, endIndex )
{
  var result = '';
  if ( bytes !== undefined )
  {
    if ( !Array.isArray( bytes ) )
      throw new Error( 'iqrf.BytesToHexStringBytesArray: Parameter bytes is not an array but ' + typeof bytes );

    if ( startIndex === undefined )
      startIndex = 0;

    if ( endIndex === undefined )
      endIndex = bytes.length - 1;

    for ( var index = startIndex; index <= endIndex; index++ )
    {
      if ( result.length !== 0 || prefixDot === true )
        result += '.';
      result += iqrf.ToHexStringByte( bytes[index] );
    }
  }

  return result;
};

/* Function: iqrf.BitmapToIndexes
Returns an array of indexes of set bits in the bitmap.

Parameters:
  bitmap - array: Array of bytes.
  indexFrom - number: Starting index of the byte in the bitmap.
  indexTo - number: Ending index of the byte in the bitmap.
  offset - number: [optional] Value to start indexing from. Default value is 0.

Returns:
  array: Array of integers specifying the offset indexes of bits set in the bitmap.
*/
iqrf.BitmapToIndexes = function ( bitmap, indexFrom, indexTo, offset )
{
  if ( !Array.isArray( bitmap ) )
    throw new Error( 'iqrf.BitmapToIndexes: Parameter bitmap is not an array but ' + typeof bitmap );

  if ( offset === undefined )
    offset = 0;

  var result = [];
  for ( var index = indexFrom; index <= indexTo; index++ )
  {
    var bitmapByte = bitmap[index];
    if ( bitmapByte === 0 )
      offset += 8;
    else
      for ( var bitMask = 0x01; bitMask !== 0x100; bitMask <<= 1 )
      {
        if ( ( bitmapByte & bitMask ) !== 0 )
          result[result.length] = offset;
        offset++;
      }
  }

  return result;
};

/* Function: iqrf.IndexesToBitmap
Returns an array of bytes with the selected indexes.

Parameters:
  indexes - array: array of integers each specifying the index of bit to set in the bitmap.
  bitmapSize - number: Size of the bitmap in bytes.

Returns:
  array: Array of bytes with the bit set according to the indexes parameter.
*/
iqrf.IndexesToBitmap = function ( indexes, bitmapSize )
{
  if ( !Array.isArray( indexes ) )
    throw new Error( 'iqrf.IndexesToBitmap: Parameter bitmap is not an array but ' + typeof indexes );

  var bitmap = [];
  for ( var index = 0; index < bitmapSize; index++ )
    bitmap[index] = 0;

  var indexes_length = indexes.length;
  for ( index = 0; index < indexes_length; index++ )
  {
    var oneIndex = indexes[index];
    var bitmapIndex = Math.floor( oneIndex / 8 );
    if ( bitmapIndex >= bitmapSize )
      throw new Error( 'iqrf.IndexesToBitmap: Index ' + oneIndex + ' out of bitmap size ' + bitmapSize );

    bitmap[bitmapIndex] |= 1 << ( oneIndex % 8 );
  }

  return bitmap;
};

/* Function: iqrf.NormalizeStringByte
Converts string byte into 2 character string for sure.

Parameters:
  byte - string: string byte value to normalize.

Returns:
  string: 2 character long hexadecimal string.
*/
iqrf.NormalizeStringByte = function ( byte )
{
  return iqrf.ToHexStringByte( iqrf.ParseStringByte( byte ) );
};

/* Function: iqrf.UInt16toInt16
Converts one's complement signed value stored at unsigned word (2 bytes or 16 bits) into signed 16 bit value.

Parameters:
  uint16 - number: Input unsigned 16 bit value containing 16 bit signed value.

Returns:
  number: Signed 16 bit value.
*/
iqrf.UInt16toInt16 = function ( uint16 )
{
  if ( uint16 < 0 || uint16 > 0xFfFf )
    throw new Error( 'iqrf.UInt16toInt16: Parameter uint16 = ' + uint16 + ' is out of range' );

  return uint16 < 0x8000 ? uint16 : uint16 - 0x10000;
};

/* Function: iqrf.UInt8toInt8
Converts one's complement signed value stored at unsigned word (1 byte or 8 bits) into signed 8 bit value.

Parameters:
  uint8 - number: Input unsigned 8 bit value containing 8 bit signed value.

Returns:
  number: Signed 8 bit value.
*/
iqrf.UInt8toInt8 = function ( uint8 )
{
  if ( uint8 < 0 || uint8 > 0xFf )
    throw new Error( 'iqrf.UInt8toInt8: Parameter uint8 = ' + uint8 + ' is out of range' );

  return uint8 < 0x80 ? uint8 : uint8 - 0x100;
};

/* Function: iqrf.Round
Rounds number to specified number of decimal places.

Parameters:
  value - number: Input number to round.
  decimals - number: Number of decimal places to round number to.

Returns:
  number: Rounded number.
*/
iqrf.Round = function ( value, decimals )
{
  return Number( Math.round( value + 'e' + decimals ) + 'e-' + decimals );
};

/* Function: iqrf.Uncomplement
Converts two's complement value into number.

Parameters:
  value - number: Input two's complement value.
  bitwidth - number: Number of valid bits of the input value. MSb is the sign bit.

Returns:
  number: Converted number.
*/
iqrf.Uncomplement = function ( value, bitwidth )
{
  var boundary = 1 << bitwidth;
  return ( value & ( 1 << ( bitwidth - 1 ) ) ) ? -boundary + ( value & ( boundary - 1 ) ) : value;
};

//############################################################################################
