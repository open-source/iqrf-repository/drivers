﻿//############################################################################################

/* Title: Embedded Red LED peripheral driver
See also: <https://www.iqrf.org/DpaTechGuide/>
*/

/*DriverDescription
{ 'ID' : 0x06,
  'Type' : 'Standard',
  'Internal' : false,
  'Versions' : [
    { 'Version' : 1.00, 'VersionFlags' : 1, 'Notes' : [
        'Get_Request and Get_Response removed.',
        'Added Flashing command.',
        'Added SetOn and SetOff commands.'
        ]
    },
    { 'Version' : 0, 'Notes' : [
        'Initial release',
        ]
    }
  ] }
DriverDescription*/

"use strict";
// Namespace: iqrf.embed.ledr
namespace( 'iqrf.embed.ledr' );

// --------------

/* Function: iqrf.embed.ledr.SetOn_Request
Encodes DPA request to switch the LED on.

Returns:
  iqrf.DpaRawHdpMessage: DPA request object.
*/
iqrf.embed.ledr.SetOn_Request = function ()
{
  return new iqrf.DpaRawHdpMessage( iqrf.PNUM_LEDR, '01' );
};

/* Function: iqrf.embed.ledr.SetOn_Response
Decodes DPA response from switching the LED on.

Parameters:
  response - iqrf.DpaRawHdpMessage: DPA response object.
*/
iqrf.embed.ledr.SetOn_Response = function ( response )
{
  iqrf.CheckResponsePnumPcmdDlen( response, iqrf.PNUM_LEDR, '81', 0 );
};

/* Function: iqrf.embed.ledr.SetOff_Request
Encodes DPA request to switch the LED off.

Returns:
  iqrf.DpaRawHdpMessage: DPA request object.
*/
iqrf.embed.ledr.SetOff_Request = function ()
{
  return new iqrf.DpaRawHdpMessage( iqrf.PNUM_LEDR, '00' );
};

/* Function: iqrf.embed.ledr.SetOff_Response
Decodes DPA response from switching the LED off.

Parameters:
  response - iqrf.DpaRawHdpMessage: DPA response object.
*/
iqrf.embed.ledr.SetOff_Response = function ( response )
{
  iqrf.CheckResponsePnumPcmdDlen( response, iqrf.PNUM_LEDR, '80', 0 );
};

/* Function: iqrf.embed.ledr.Set_Request
Encodes DPA request to set the LED.

Parameters:
  onOff - boolean: Required LED state. true is on, false is off.

Returns:
  iqrf.DpaRawHdpMessage: DPA request object.
*/
iqrf.embed.ledr.Set_Request = function ( onOff )
{
  if ( typeof onOff !== 'boolean' )
    throw new Error( 'iqrf.embed.ledr.Set_Request: Parameter onOff expected to be boolean but not ' + typeof onOff );

  return new iqrf.DpaRawHdpMessage( iqrf.PNUM_LEDR, iqrf.ToHexStringByte( onOff ? 1 : 0 ) );
};

/* Function: iqrf.embed.ledr.Set_Response
Decodes DPA response from setting the LED.

Parameters:
  response - iqrf.DpaRawHdpMessage: DPA response object.
*/
iqrf.embed.ledr.Set_Response = function ( response )
{
  iqrf.CheckResponsePnumPcmdDlen( response, iqrf.PNUM_LEDR, '80.81', 0 );
};

/* Function: iqrf.embed.ledr.Pulse_Request
Encodes DPA request to pulse the LED.

Returns:
  iqrf.DpaRawHdpMessage: DPA request object.
*/
iqrf.embed.ledr.Pulse_Request = function ()
{
  return new iqrf.DpaRawHdpMessage( iqrf.PNUM_LEDR, '03' );
};

/* Function: iqrf.embed.ledr.Pulse_Response
Decodes DPA response from pulsing the LED.

Parameters:
  response - iqrf.DpaRawHdpMessage: DPA response object.
*/
iqrf.embed.ledr.Pulse_Response = function ( response )
{
  iqrf.CheckResponsePnumPcmdDlen( response, iqrf.PNUM_LEDR, '83', 0 );
};

/* Function: iqrf.embed.ledr.Flashing_Request
Encodes DPA request for flashing the LED.

Returns:
  iqrf.DpaRawHdpMessage: DPA request object.
*/
iqrf.embed.ledr.Flashing_Request = function ()
{
  return new iqrf.DpaRawHdpMessage( iqrf.PNUM_LEDR, '04' );
};

/* Function: iqrf.embed.ledr.Flashing_Response
Decodes DPA response from flashing the LED.

Parameters:
  response - iqrf.DpaRawHdpMessage: DPA response object.
*/
iqrf.embed.ledr.Flashing_Response = function ( response )
{
  iqrf.CheckResponsePnumPcmdDlen( response, iqrf.PNUM_LEDR, '84', 0 );
};

//############################################################################################
