﻿//############################################################################################

/* Title: Embedded Coordinator peripheral driver
See also: <https://www.iqrf.org/DpaTechGuide/>
*/

/*DriverDescription
{ 'ID' : 0x00,
  'Type' : 'Standard',
  'Internal' : false,
  'Versions' : [
    { 'Version' : 3.10, 'VersionFlags' : 1, 'Notes' : [
        'Fix for DPA 4.14+: iqrf.embed.coordinator.SmartConnect_Request fix according to DPA 4.16 SmartConnect bug fix.'
        ]
    },
    { 'Version' : 3.00, 'VersionFlags' : 1, 'Notes' : [
        'Changes for DPA 4.14',
        'Command Authorize bond can now authorize up to 11 [Ns].'
        ]
    },
    { 'Version' : 2.00, 'VersionFlags' : 1, 'Notes' : [
        'Changes for DPA 4.00',
        'Discarded functions for DPA commands CMD_COORDINATOR_READ_REMOTELY_BONDED_MID, CMD_COORDINATOR_CLEAR_REMOTELY_BONDED_MID, CMD_COORDINATOR_ENABLE_REMOTE_BONDING, CMD_COORDINATOR_REBOND_NODE, CMD_COORDINATOR_DISCOVERY_DATA, and CMD_NODE_REMOVE_BOND_ADDRESS.',
        'Parameter bondingMask at BondNode_Request renamed to bondingTestRetries.'
        ]
    },
    { 'Version' : 1.00, 'VersionFlags' : 1, 'Notes' : [
        'Added SmartConnect support for DPA > 3.02.',
        'DiscoveredDevices_Response and BondedDevices_Response ignore addresses above 239.',
        'Added SetMID.'
        ]
    },
    { 'Version' : 0.00, 'Notes' : [
        'Initial release',
        ]
    }
  ] }
DriverDescription*/

"use strict";
// Namespace: iqrf.embed.coordinator
// All prepared DPA requests have nadr set to '00'.
namespace( 'iqrf.embed.coordinator' );

// --------------
/* Function: iqrf.embed.coordinator.AddrInfo_Request
Encodes DPA request to get basic network information.

Returns:
  iqrf.DpaRawHdpMessage: DPA request object.
*/
iqrf.embed.coordinator.AddrInfo_Request = function ()
{
  return new iqrf.DpaRawHdpMessageCoordinator( iqrf.PNUM_Coordinator, '00' );
};

/* Function: iqrf.embed.coordinator.AddrInfo_Response
Decodes DPA response from getting basic network information.

Parameters:
  response - iqrf.DpaRawHdpMessage: DPA response object.

Returns:
  object: Object with the following fields (see DPA documentation https://www.iqrf.org/DpaTechGuide for details):

* devNr - number:
* did - number:
*/
iqrf.embed.coordinator.AddrInfo_Response = function ( response )
{
  var responseData = iqrf.CheckResponsePnumPcmdDlen( response, iqrf.PNUM_Coordinator, '80', 2 );

  var result =
  {
    devNr: responseData[0],
    did: responseData[1]
  };

  return result;
};

// --------------
/* Function: iqrf.embed.coordinator.DiscoveredDevices_Request
Encodes DPA request to get list of discovered devices

Returns:
  iqrf.DpaRawHdpMessage: DPA request object.
*/
iqrf.embed.coordinator.DiscoveredDevices_Request = function ()
{
  return new iqrf.DpaRawHdpMessageCoordinator( iqrf.PNUM_Coordinator, '01' );
};

/* Function: iqrf.embed.coordinator.DiscoveredDevices_Response
Decodes DPA response from getting list of discovered devices.

Parameters:
  response - iqrf.DpaRawHdpMessage: DPA response object.

Returns:
  array: Array if integer values corresponding to the addresses of the discovered devices.
*/
iqrf.embed.coordinator.DiscoveredDevices_Response = function ( response )
{
  return iqrf.BitmapToIndexes( iqrf.CheckResponsePnumPcmdDlen( response, iqrf.PNUM_Coordinator, '81', 32 ), 0, 29 );
};

// --------------
/* Function: iqrf.embed.coordinator.BondedDevices_Request
Encodes DPA request to get list of bonded devices

Returns:
  iqrf.DpaRawHdpMessage: DPA request object.
*/
iqrf.embed.coordinator.BondedDevices_Request = function ()
{
  return new iqrf.DpaRawHdpMessageCoordinator( iqrf.PNUM_Coordinator, '02' );
};

/* Function: iqrf.embed.coordinator.BondedDevices_Response
Decodes DPA response from getting list of bonded devices.

Parameters:
  response - iqrf.DpaRawHdpMessage: DPA response object.

Returns:
  array: Array if integer values corresponding to the addresses of the bonded devices.
*/
iqrf.embed.coordinator.BondedDevices_Response = function ( response )
{
  return iqrf.BitmapToIndexes( iqrf.CheckResponsePnumPcmdDlen( response, iqrf.PNUM_Coordinator, '82', 32 ), 0, 29 );
};

// --------------
/* Function: iqrf.embed.coordinator.ClearAllBonds_Request
Encodes DPA request to clear all bonds.

Returns:
  iqrf.DpaRawHdpMessage: DPA request object.
*/
iqrf.embed.coordinator.ClearAllBonds_Request = function ()
{
  return new iqrf.DpaRawHdpMessageCoordinator( iqrf.PNUM_Coordinator, '03' );
};

/* Function: iqrf.embed.coordinator.ClearAllBonds_Response
Decodes DPA response from clearing all bonds.

Parameters:
  response - iqrf.DpaRawHdpMessage: DPA response object.
*/
iqrf.embed.coordinator.ClearAllBonds_Response = function ( response )
{
  iqrf.CheckResponsePnumPcmdDlen( response, iqrf.PNUM_Coordinator, '83', 0 );
};

// --------------
/* Function: iqrf.embed.coordinator.BondNode_Request
Encodes DPA request to bond a node.

Parameters:
  reqAddr - number: Requested address.
  bondingTestRetries - number: Bonding mask.

Returns:
  iqrf.DpaRawHdpMessage: DPA request object.
*/
iqrf.embed.coordinator.BondNode_Request = function ( reqAddr, bondingTestRetries )
{
  return new iqrf.DpaRawHdpMessageCoordinator( iqrf.PNUM_Coordinator, '04', iqrf.ToHexStringByte( reqAddr ) + '.' + iqrf.ToHexStringByte( bondingTestRetries ) );
};

/* Function: iqrf.embed.coordinator.BondNode_Response
Decodes DPA response from bonding a node.

Parameters:
  response - iqrf.DpaRawHdpMessage: DPA response object.

Returns:
  object: Object with the following fields:

* bondAddr - number: Address of the node newly bonded to the network.
* devNr - number: Number of bonded network nodes.
*/
iqrf.embed.coordinator.BondNode_Response = function ( response )
{
  var responseData = iqrf.CheckResponsePnumPcmdDlen( response, iqrf.PNUM_Coordinator, '84', 2 );

  var result =
  {
    bondAddr: responseData[0],
    devNr: responseData[1]
  };

  return result;
};

// --------------
/* Function: iqrf.embed.coordinator.RemoveBond_Request
Encodes DPA request to remove bond.

Parameters:
  bondAddr - number: Address of the node to remove the bond to.

Returns:
  iqrf.DpaRawHdpMessage: DPA request object.
*/
iqrf.embed.coordinator.RemoveBond_Request = function ( bondAddr )
{
  return new iqrf.DpaRawHdpMessageCoordinator( iqrf.PNUM_Coordinator, '05', iqrf.ToHexStringByte( bondAddr ) );
};

/* Function: iqrf.embed.coordinator.RemoveBond_Response
Decodes DPA response from removing bond.

Parameters:
  response - iqrf.DpaRawHdpMessage: DPA response object.

Returns:
  number: Number of bonded network nodes.
*/
iqrf.embed.coordinator.RemoveBond_Response = function ( response )
{
  var responseData = iqrf.CheckResponsePnumPcmdDlen( response, iqrf.PNUM_Coordinator, '85', 1 );
  return responseData[0];
};

// --------------
/* Function: iqrf.embed.coordinator.Discovery_Request
Encodes DPA request to run discovery.

Parameters:
  txPower - number: TX Power used for discovery.
  maxAddr - number: Nonzero value specifies maximum node address to be part of the discovery process.

Returns:
  iqrf.DpaRawHdpMessage: DPA request object.
*/
iqrf.embed.coordinator.Discovery_Request = function ( txPower, maxAddr )
{
  return new iqrf.DpaRawHdpMessageCoordinator( iqrf.PNUM_Coordinator, '07', iqrf.ToHexStringByte( txPower ) + '.' + iqrf.ToHexStringByte( maxAddr ) );
};

/* Function: iqrf.embed.coordinator.Discovery_Response
Decodes DPA response from running discovery.

Parameters:
  response - iqrf.DpaRawHdpMessage: DPA response object.

Returns:
  number: Number of discovered network nodes.
*/
iqrf.embed.coordinator.Discovery_Response = function ( response )
{
  var responseData = iqrf.CheckResponsePnumPcmdDlen( response, iqrf.PNUM_Coordinator, '87', 1 );
  return responseData[0];
};

// --------------
/* Function: iqrf.embed.coordinator.SetDpaParams_Request
Encodes DPA request to set DPA parameter.

Parameters:
  dpaParam - number: DPA param to set.

Returns:
  iqrf.DpaRawHdpMessage: DPA request object.
*/
iqrf.embed.coordinator.SetDpaParams_Request = function ( dpaParam )
{
  return new iqrf.DpaRawHdpMessageCoordinator( iqrf.PNUM_Coordinator, '08', iqrf.ToHexStringByte( dpaParam ) );
};

/* Function: iqrf.embed.coordinator.SetDpaParams_Response
Decodes DPA response from setting DPA parameter.

Parameters:
  response - iqrf.DpaRawHdpMessage: DPA response object.

Returns:
  number: Previous DPA parameter value.
*/
iqrf.embed.coordinator.SetDpaParams_Response = function ( response )
{
  var responseData = iqrf.CheckResponsePnumPcmdDlen( response, iqrf.PNUM_Coordinator, '88', 1 );
  return responseData[0];
};

// --------------
/* Function: iqrf.embed.coordinator.SetHops_Request
Encodes DPA request to specify number of hops

Parameters:
  requestHops - number: see DPA documentation https://www.iqrf.org/DpaTechGuide for details.
  responseHops - number: see DPA documentation https://www.iqrf.org/DpaTechGuide for details.

Returns:
  iqrf.DpaRawHdpMessage: DPA request object.
*/
iqrf.embed.coordinator.SetHops_Request = function ( requestHops, responseHops )
{
  return new iqrf.DpaRawHdpMessageCoordinator( iqrf.PNUM_Coordinator, '09', iqrf.ToHexStringByte( requestHops ) + '.' + iqrf.ToHexStringByte( responseHops ) );
};

/* Function: iqrf.embed.coordinator.SetHops_Response
Decodes DPA response from specifying number of hops.

Parameters:
  response - iqrf.DpaRawHdpMessage: DPA response object.

Returns:
object: Object with the following fields storing previous values:

* requestHops - number:
* responseHops - number:
*/
iqrf.embed.coordinator.SetHops_Response = function ( response )
{
  var responseData = iqrf.CheckResponsePnumPcmdDlen( response, iqrf.PNUM_Coordinator, '89', 2 );

  var result =
  {
    requestHops: responseData[0],
    responseHops: responseData[1]
  };

  return result;
};

// --------------
/* Function: iqrf.embed.coordinator.Backup_Request
Encodes DPA request to backup coordinator.

Parameters:
  index - number: Index of the block of data.

Returns:
  iqrf.DpaRawHdpMessage: DPA request object.
*/
iqrf.embed.coordinator.Backup_Request = function ( index )
{
  return new iqrf.DpaRawHdpMessageCoordinator( iqrf.PNUM_Coordinator, '0B', iqrf.ToHexStringByte( index ) );
};

/* Function: iqrf.embed.coordinator.Backup_Response
Decodes DPA response from backing up coordinator.

Parameters:
  response - iqrf.DpaRawHdpMessage: DPA response object.

Returns:
  array: Array of bytes with the backup content.
*/
iqrf.embed.coordinator.Backup_Response = function ( response )
{
  return iqrf.CheckResponsePnumPcmdDlen( response, iqrf.PNUM_Coordinator, '8B', 49 );
};

// --------------
/* Function: iqrf.embed.coordinator.Restore_Request
Encodes DPA request to restore coordinator.

Parameters:
  networkData - array: One block of the coordinator network info data previously obtained by a backup command.

Returns:
  iqrf.DpaRawHdpMessage: DPA request object.
*/
iqrf.embed.coordinator.Restore_Request = function ( networkData )
{
  return new iqrf.DpaRawHdpMessageCoordinator( iqrf.PNUM_Coordinator, '0C', iqrf.BytesToHexStringBytesArray( networkData ) );
};

/* Function: iqrf.embed.coordinator.Restore_Response
Decodes DPA response from restoring coordinator.

Parameters:
  response - iqrf.DpaRawHdpMessage: DPA response object.
*/
iqrf.embed.coordinator.Restore_Response = function ( response )
{
  iqrf.CheckResponsePnumPcmdDlen( response, iqrf.PNUM_Coordinator, '8C', 0 );
};

// --------------
/* Function: iqrf.embed.coordinator.AuthorizeBond_Request
Encodes DPA request to authorize previously remotely pre-bonded nodes.

Parameters:
  nodes - array: Array of objects with the following fields (see DPA documentation https://www.iqrf.org/DpaTechGuide for details):

*  reqAddr - number: see DPA documentation https://www.iqrf.org/DpaTechGuide for details.
*  mid - number: see DPA documentation https://www.iqrf.org/DpaTechGuide for details.

Returns:
  iqrf.DpaRawHdpMessage: DPA request object.
*/
iqrf.embed.coordinator.AuthorizeBond_Request = function ( nodes )
{
  var data = '';
  var nodes_length = nodes.length;
  for ( var index = 0; index < nodes_length; index++ )
  {
    var oneNode = nodes[index];
    if ( index !== 0 )
      data += '.';
    data += iqrf.ToHexStringByte( oneNode.reqAddr ) + '.' + iqrf.IntToHexStringBytesArray( oneNode.mid, 4 );
  }

  return new iqrf.DpaRawHdpMessageCoordinator( iqrf.PNUM_Coordinator, '0D', data );
};

/* Function: iqrf.embed.coordinator.AuthorizeBond_Response
Decodes DPA response from authorizing previously remotely pre-bonded node.

Parameters:
  response - iqrf.DpaRawHdpMessage: DPA response object.

Returns:
  object: Object with the following fields:

* bondAddr - number: Address of the node newly bonded to the network.
* devNr - number: Number of bonded network nodes.
*/
iqrf.embed.coordinator.AuthorizeBond_Response = function ( response )
{
  var responseData = iqrf.CheckResponsePnumPcmdDlen( response, iqrf.PNUM_Coordinator, '8D', 2 );

  var result =
  {
    bondAddr: responseData[0],
    devNr: responseData[1]
  };

  return result;
};

// --------------
/* Function: iqrf.embed.coordinator.SmartConnect_Request
Encodes DPA request for Smart Connect.
From version 1.00 for DPA > 3.02.

Parameters:
  reqAddr - number: see DPA documentation https://www.iqrf.org/DpaTechGuide for details.
  bondingTestRetries - number: see DPA documentation https://www.iqrf.org/DpaTechGuide for details.
  ibk - array: see DPA documentation https://www.iqrf.org/DpaTechGuide for details.
  mid - number: see DPA documentation https://www.iqrf.org/DpaTechGuide for details.
  virtualDeviceAddress - number: see DPA documentation https://www.iqrf.org/DpaTechGuide for details.
  userData - array:  see DPA documentation https://www.iqrf.org/DpaTechGuide for details.

Returns:
  iqrf.DpaRawHdpMessage: DPA request object.
*/

iqrf.embed.coordinator.SmartConnect_Request = function ( reqAddr, bondingTestRetries, ibk, mid, virtualDeviceAddress, userData )
{
  return new iqrf.DpaRawHdpMessageCoordinator( iqrf.PNUM_Coordinator, '12',
    iqrf.ToHexStringByte( reqAddr ) + '.' +
    iqrf.ToHexStringByte( bondingTestRetries ) + '.' +
    iqrf.BytesToHexStringBytesArray( ibk ) + '.' +
    iqrf.IntToHexStringBytesArray( mid, 4 ) +
    '.00.' +
    iqrf.ToHexStringByte( virtualDeviceAddress ) + '.' +
    iqrf.BytesToHexStringBytesArray( userData ) +
    '.00.00.00.00.00.00.00.00.00.00' );
};

/* Function: iqrf.embed.coordinator.SmartConnect_Response
Decodes DPA response from Smart Connect.
From version 1.00 for DPA > 3.02.

Parameters:
  response - iqrf.DpaRawHdpMessage: DPA response object.

Returns:
  object: Object with the following fields:

* bondAddr - number: Address of the node newly bonded to the network.
* devNr - number: Number of bonded network nodes.
*/
iqrf.embed.coordinator.SmartConnect_Response = function ( response )
{
  var responseData = iqrf.CheckResponsePnumPcmdDlen( response, iqrf.PNUM_Coordinator, '92', 2 );

  var result =
  {
    bondAddr: responseData[0],
    devNr: responseData[1]
  };

  return result;
};

// --------------
/* Function: iqrf.embed.coordinator.SetMID_Request
Encodes DPA request to set the MID.
From version 1.00 for DPA > 3.02.

Parameters:
  bondAddr - number: Address of the node.
  mid - number: MID to write to the Coordinator's database.

Returns:
  iqrf.DpaRawHdpMessage: DPA request object.
*/
iqrf.embed.coordinator.SetMID_Request = function ( bondAddr, mid )
{
  return new iqrf.DpaRawHdpMessageCoordinator( iqrf.PNUM_Coordinator, '13', iqrf.IntToHexStringBytesArray( mid, 4 ) + '.' + iqrf.ToHexStringByte( bondAddr ) );
};

/* Function: iqrf.embed.coordinator.SetMID_Response
Decodes DPA response from setting the MID.
From version 1.00 for DPA > 3.02.

Parameters:
  response - iqrf.DpaRawHdpMessage: DPA response object.
*/
iqrf.embed.coordinator.SetMID_Response = function ( response )
{
  iqrf.CheckResponsePnumPcmdDlen( response, iqrf.PNUM_Coordinator, '93', 0 );
};

//############################################################################################
