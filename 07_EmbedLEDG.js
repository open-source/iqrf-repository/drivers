﻿//############################################################################################

/* Title: Embedded Green LED peripheral driver
See also: <https://www.iqrf.org/DpaTechGuide/>
*/

/*DriverDescription
{ 'ID' : 0x07,
  'Type' : 'Standard',
  'Internal' : false,
  'Versions' : [
    { 'Version' : 1.00, 'VersionFlags' : 1, 'Notes' : [
        'Get_Request and Get_Response removed.',
        'Added Flashing command.',
        'Added SetOn and SetOff commands.'
        ]
    },
    { 'Version' : 0, 'Notes' : [
        'Initial release',
        ]
    }
  ] }
DriverDescription*/

"use strict";
// Namespace: iqrf.embed.ledg
namespace( 'iqrf.embed.ledg' );

// --------------

/* Function: iqrf.embed.ledg.SetOn_Request
Encodes DPA request to switch the LED on.

Returns:
  iqrf.DpaRawHdpMessage: DPA request object.
*/
iqrf.embed.ledg.SetOn_Request = function ()
{
  return new iqrf.DpaRawHdpMessage( iqrf.PNUM_LEDG, '01' );
};

/* Function: iqrf.embed.ledg.SetOn_Response
Decodes DPA response from switching the LED on.

Parameters:
  response - iqrf.DpaRawHdpMessage: DPA response object.
*/
iqrf.embed.ledg.SetOn_Response = function ( response )
{
  iqrf.CheckResponsePnumPcmdDlen( response, iqrf.PNUM_LEDG, '81', 0 );
};

/* Function: iqrf.embed.ledg.SetOff_Request
Encodes DPA request to switch the LED off.

Returns:
  iqrf.DpaRawHdpMessage: DPA request object.
*/
iqrf.embed.ledg.SetOff_Request = function ()
{
  return new iqrf.DpaRawHdpMessage( iqrf.PNUM_LEDG, '00' );
};

/* Function: iqrf.embed.ledg.SetOff_Response
Decodes DPA response from switching the LED off.

Parameters:
  response - iqrf.DpaRawHdpMessage: DPA response object.
*/
iqrf.embed.ledg.SetOff_Response = function ( response )
{
  iqrf.CheckResponsePnumPcmdDlen( response, iqrf.PNUM_LEDG, '80', 0 );
};

/* Function: iqrf.embed.ledg.Set_Request
Encodes DPA request to set the LED.

Parameters:
  onOff - boolean: Required LED state. true is on, false is off.

Returns:
  iqrf.DpaRawHdpMessage: DPA request object.
*/
iqrf.embed.ledg.Set_Request = function ( onOff )
{
  if ( typeof onOff !== 'boolean' )
    throw new Error( 'iqrf.embed.ledg.Set_Request: Parameter onOff expected to be boolean but not ' + typeof onOff );

  return new iqrf.DpaRawHdpMessage( iqrf.PNUM_LEDG, iqrf.ToHexStringByte( onOff ? 1 : 0 ) );
};

/* Function: iqrf.embed.ledg.Set_Response
Decodes DPA response from setting the LED.

Parameters:
  response - iqrf.DpaRawHdpMessage: DPA response object.
*/
iqrf.embed.ledg.Set_Response = function ( response )
{
  iqrf.CheckResponsePnumPcmdDlen( response, iqrf.PNUM_LEDG, '80.81', 0 );
};

/* Function: iqrf.embed.ledg.Pulse_Request
Encodes DPA request to pulse the LED.

Returns:
  iqrf.DpaRawHdpMessage: DPA request object.
*/
iqrf.embed.ledg.Pulse_Request = function ()
{
  return new iqrf.DpaRawHdpMessage( iqrf.PNUM_LEDG, '03' );
};

/* Function: iqrf.embed.ledg.Pulse_Response
Decodes DPA response from pulsing the LED.

Parameters:
  response - iqrf.DpaRawHdpMessage: DPA response object.
*/
iqrf.embed.ledg.Pulse_Response = function ( response )
{
  iqrf.CheckResponsePnumPcmdDlen( response, iqrf.PNUM_LEDG, '83', 0 );
};

/* Function: iqrf.embed.ledg.Flashing_Request
Encodes DPA request for flashing the LED.

Returns:
  iqrf.DpaRawHdpMessage: DPA request object.
*/
iqrf.embed.ledg.Flashing_Request = function ()
{
  return new iqrf.DpaRawHdpMessage( iqrf.PNUM_LEDG, '04' );
};

/* Function: iqrf.embed.ledg.Flashing_Response
Decodes DPA response from flashing the LED.

Parameters:
  response - iqrf.DpaRawHdpMessage: DPA response object.
*/
iqrf.embed.ledg.Flashing_Response = function ( response )
{
  iqrf.CheckResponsePnumPcmdDlen( response, iqrf.PNUM_LEDG, '84', 0 );
};

//############################################################################################
