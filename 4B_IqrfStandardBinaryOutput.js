﻿//############################################################################################

/* Title: IQRF Standards Binary Output driver
See also: <https://www.iqrfalliance.org/techDocs/>
*/

/*DriverDescription
{ 'ID' : 0x4B,
  'Type' : 'Standard',
  'Internal' : false,
  'Versions' : [
    { 'Version' : 4, 'Notes' : [
        'Initial release' ]
    }
  ] }
DriverDescription*/

"use strict";
// Namespace: iqrf.binaryoutput
namespace( 'iqrf.binaryoutput' );

// IQRF Standards Binary Output PNUM value
iqrf.binaryoutput.PNUM = '4b';

/* Function: iqrf.binaryoutput.Enumerate_Request
Encodes DPA request to enumerate binary outputs.

Returns:
  iqrf.DpaRawHdpMessage: DPA request object.
*/
iqrf.binaryoutput.Enumerate_Request = function ()
{
  return new iqrf.DpaRawHdpMessage( iqrf.binaryoutput.PNUM, '3e' );
};

/* Function: iqrf.binaryoutput.Enumerate_Response
Decodes DPA response from binary outputs enumeration.

Parameters:
  response - iqrf.DpaRawHdpMessage: DPA response object.

Returns:
  number: Number of implemented binary outputs.
*/
iqrf.binaryoutput.Enumerate_Response = function ( response )
{
  var responseData = iqrf.CheckResponsePnumPcmdDlen( response, iqrf.binaryoutput.PNUM, 'be', 1 );
  return responseData[0];
};

/* Function: iqrf.binaryoutput.SetOutput_Request
Encodes DPA request to set binary outputs.

Parameters:
  outputs - array: Array of objects with the following fields (see DPA documentation https://www.iqrf.org/DpaTechGuide for details):

* index - number: Zero based index of the output to set.
* state - boolean: true to set ON state, false to set OFF state.
* time - number: [optional] If state is true, then time in seconds to keep the output at ON state. Allowed values are <1;127> or 60*<1;127>.

Returns:
  iqrf.DpaRawHdpMessage: DPA request object.
*/
iqrf.binaryoutput.SetOutput_Request = function ( outputs )
{
  var bitmap = 0x0000;
  var data = '';
  var outputs_length = outputs.length;
  for ( var index = 0; index < outputs_length; index++ )
  {
    var oneOutput = outputs[index];
    bitmap |= 1 << oneOutput.index;

    var oneData;
    if ( oneOutput.state === true )
    {
      if ( oneOutput.time === undefined )
        oneData = 1;
      else
      {
        if ( oneOutput.time >= 1 && oneOutput.time <= 127 )
          oneData = oneOutput.time | 0x80;
        else
        {
          var minutes = Math.floor( oneOutput.time / 60 );
          if ( oneOutput.time % 60 !== 0 || minutes < 2 || minutes > 127 )
            throw new Error( 'iqrf.binaryoutput.SetOutput_Request: Invalid time ' + oneOutput.time + ' specified for the output ' + oneOutput.index );

          oneData = minutes;
        }
      }
    }
    else
    {
      if ( oneOutput.state !== false )
        throw new Error( 'iqrf.binaryoutput.SetOutput_Request: Invalid state ' + oneOutput.state + ' specified for the output ' + oneOutput.index );

      if ( oneOutput.time !== undefined )
        throw new Error( 'iqrf.binaryoutput.SetOutput_Request: Time ' + oneOutput.time + ' specified but the output ' + oneOutput.index + ' is not to be set' );

      oneData = 0;
    }

    data += '.' + iqrf.ToHexStringByte( oneData );
  }

  return new iqrf.DpaRawHdpMessage( iqrf.binaryoutput.PNUM, '00', iqrf.IntToHexStringBytesArray( bitmap, 4, true ) + data );
};

/* Function: iqrf.binaryoutput.SetOutput_Response
Decodes DPA response from setting binary outputs.

Parameters:
  response - iqrf.DpaRawHdpMessage: DPA response object.

Returns:
  array: Array of boolean true/false values specifying the previous state of each binary output. false is returned for unimplemented binary outputs.
*/
iqrf.binaryoutput.SetOutput_Response = function ( response )
{
  var responseData = iqrf.CheckResponsePnumPcmdDlen( response, iqrf.binaryoutput.PNUM, '80', 4 );
  var bitmap = responseData[0] + ( responseData[1] * 0x100 ) + ( responseData[2] * 0x10000 ) + ( responseData[3] * 0x1000000 );

  var result = [];
  for ( var index = 0; index < 32; index++ )
    result[index] = ( bitmap & ( 1 << index ) ) !== 0;

  return result;
};

//############################################################################################
