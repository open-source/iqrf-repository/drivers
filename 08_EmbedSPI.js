﻿//############################################################################################

/* Title: Embedded SPI peripheral driver
See also: <https://www.iqrf.org/DpaTechGuide/>
*/

/*DriverDescription
{ 'ID' : 0x08,
  'Type' : 'Standard',
  'Internal' : false,
  'Versions' : [
    { 'Version' : 0, 'Notes' : [
        'Initial release',
        ]
    }
  ] }
DriverDescription*/

"use strict";
// Namespace: iqrf.embed.spi
namespace( 'iqrf.embed.spi' );

// --------------
/* Function: iqrf.embed.spi.WriteRead_Request
Encodes DPA request to write and/or read data to/from SPI peripheral.

Parameters:
  readTimeout - number:
  writtenData - array: Array of bytes.

Returns:
  iqrf.DpaRawHdpMessage: DPA request object.
*/
iqrf.embed.spi.WriteRead_Request = function ( readTimeout, writtenData )
{
  return new iqrf.DpaRawHdpMessage( iqrf.PNUM_SPI, '00', iqrf.ToHexStringByte( readTimeout ) + iqrf.BytesToHexStringBytesArray( writtenData, true ) );
};

/* Function: iqrf.embed.spi.WriteRead_Response
Decodes DPA response from writing and/or reading data to/from SPI peripheral.

Parameters:
  response - iqrf.DpaRawHdpMessage: DPA response object.

Returns:
  array: Read data.
*/
iqrf.embed.spi.WriteRead_Response = function ( response )
{
  return iqrf.CheckResponsePnumPcmdDlen( response, iqrf.PNUM_SPI, '80' );
};

//############################################################################################
