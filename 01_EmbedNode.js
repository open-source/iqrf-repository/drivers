﻿//############################################################################################

/* Title: Embedded Node peripheral driver
See also: <https://www.iqrf.org/DpaTechGuide/>
*/

/*DriverDescription
{ 'ID' : 0x01,
  'Type' : 'Standard',
  'Internal' : false,
  'Versions' : [
    { 'Version' : 1.00, 'VersionFlags' : 1, 'Notes' : [
        'Changes for DPA 4.00',
        'Discarded functions for DPA commands CMD_NODE_READ_REMOTELY_BONDED_MID, CMD_NODE_CLEAR_REMOTELY_BONDED_MID, and CMD_NODE_ENABLE_REMOTE_BONDING.'
        ]
    },
    { 'Version' : 0, 'Notes' : [
        'Initial release',
        ]
    }
  ] }
DriverDescription*/

"use strict";
// Namespace: iqrf.embed.node
namespace( 'iqrf.embed.node' );

// --------------
/* Function: iqrf.embed.node.Read_Request
Encodes DPA request to read Node information.

Returns:
  iqrf.DpaRawHdpMessage: DPA request object.
*/
iqrf.embed.node.Read_Request = function ()
{
  return new iqrf.DpaRawHdpMessage( iqrf.PNUM_Node, '00' );
};

/* Function: iqrf.embed.node.Read_Response
Decodes DPA response from reading Node information.

Parameters:
  response - iqrf.DpaRawHdpMessage: DPA response object.

Returns:
  object: Object with the following fields (see DPA documentation https://www.iqrf.org/DpaTechGuide/index.html?page=NodeRead.html for details):

* ntwADDR - number:
* ntwVRN - number:
* ntwZIN - number:
* ntwDID - number:
* ntwPVRN - number:
* ntwUSERADDRESS - number:
* ntwID - number:
* ntwVRNFNZ - number:
* ntwCFG - number:
* flags - number:
*/
iqrf.embed.node.Read_Response = function ( response )
{
  var responseData = iqrf.CheckResponsePnumPcmdDlen( response, iqrf.PNUM_Node, '80', 12 );

  var result =
  {
    ntwADDR: responseData[0],
    ntwVRN: responseData[1],
    ntwZIN: responseData[2],
    ntwDID: responseData[3],
    ntwPVRN: responseData[4],
    ntwUSERADDRESS: responseData[5] + ( responseData[6] * 0x100 ),
    ntwID: responseData[7] + ( responseData[8] * 0x100 ),
    ntwVRNFNZ: responseData[9],
    ntwCFG: responseData[10],
    flags: responseData[11]
  };

  return result;
};

// --------------
/* Function: iqrf.embed.node.RemoveBond_Request
Encodes DPA request to remove bond.

Returns:
  iqrf.DpaRawHdpMessage: DPA request object.
*/
iqrf.embed.node.RemoveBond_Request = function ()
{
  return new iqrf.DpaRawHdpMessage( iqrf.PNUM_Node, '01' );
};

/* Function: iqrf.embed.node.RemoveBond_Response
Decodes DPA response from removing bond.

Parameters:
  response - iqrf.DpaRawHdpMessage: DPA response object.
*/
iqrf.embed.node.RemoveBond_Response = function ( response )
{
  iqrf.CheckResponsePnumPcmdDlen( response, iqrf.PNUM_Node, '81', 0 );
};

// --------------
/* Function: iqrf.embed.node.RemoveBondAddress_Request
Encodes DPA request to remove bond address.

Returns:
  iqrf.DpaRawHdpMessage: DPA request object.
*/
iqrf.embed.node.RemoveBondAddress_Request = function ()
{
  return new iqrf.DpaRawHdpMessage( iqrf.PNUM_Node, '05' );
};

/* Function: iqrf.embed.node.RemoveBondAddress_Response
Decodes DPA response from removing bond address.

Parameters:
  response - iqrf.DpaRawHdpMessage: DPA response object.
*/
iqrf.embed.node.RemoveBondAddress_Response = function ( response )
{
  iqrf.CheckResponsePnumPcmdDlen( response, iqrf.PNUM_Node, '85', 0 );
};

// --------------
/* Function: iqrf.embed.node.Backup_Request
Encodes DPA request to backup node.

Parameters:
  index - number: Index of the block of data.

Returns:
  iqrf.DpaRawHdpMessage: DPA request object.
*/
iqrf.embed.node.Backup_Request = function ( index )
{
  return new iqrf.DpaRawHdpMessage( iqrf.PNUM_Node, '06', iqrf.ToHexStringByte( index ) );
};

/* Function: iqrf.embed.node.Backup_Response
Decodes DPA response from backing up node.

Parameters:
  response - iqrf.DpaRawHdpMessage: DPA response object.

Returns:
  array: Array of bytes with the backup content.
*/
iqrf.embed.node.Backup_Response = function ( response )
{
  return iqrf.CheckResponsePnumPcmdDlen( response, iqrf.PNUM_Node, '86', 49 );
};

// --------------
/* Function: iqrf.embed.node.Restore_Request
Encodes DPA request to restore node.

Parameters:
  networkData - array: One block of the node network info data previously obtained by a backup command.

Returns:
  iqrf.DpaRawHdpMessage: DPA request object.
*/
iqrf.embed.node.Restore_Request = function ( networkData )
{
  return new iqrf.DpaRawHdpMessage( iqrf.PNUM_Node, '07', iqrf.BytesToHexStringBytesArray( networkData ) );
};

/* Function: iqrf.embed.node.Restore_Response
Decodes DPA response from restoring node.

Parameters:
  response - iqrf.DpaRawHdpMessage: DPA response object.
*/
iqrf.embed.node.Restore_Response = function ( response )
{
  iqrf.CheckResponsePnumPcmdDlen( response, iqrf.PNUM_Node, '87', 0 );
};

// --------------
/* Function: iqrf.embed.node.ValidateBonds_Request
Encodes DPA request to validate bonds.

Parameters:
  nodes - array: Array of objects with the following fields (see DPA documentation https://www.iqrf.org/DpaTechGuide for details):

*  bondAddr - number: Address of the node to validate.
*  mid - number: MID to check against.

Returns:
  iqrf.DpaRawHdpMessage: DPA request object.
*/
iqrf.embed.node.ValidateBonds_Request = function ( nodes )
{
  var data = '';
  var nodes_length = nodes.length;
  for ( var index = 0; index < nodes_length; index++ )
  {
    var oneNode = nodes[index];
    if ( index !== 0 )
      data += '.';
    data += iqrf.ToHexStringByte( oneNode.bondAddr ) + '.' + iqrf.IntToHexStringBytesArray( oneNode.mid, 4 );
  }

  return new iqrf.DpaRawHdpMessage( iqrf.PNUM_Node, '08', data );
};

/* Function: iqrf.embed.node.ValidateBonds_Response
Decodes DPA response from validating bonds.

Parameters:
  response - iqrf.DpaRawHdpMessage: DPA response object.
*/
iqrf.embed.node.ValidateBonds_Response = function ( response )
{
  iqrf.CheckResponsePnumPcmdDlen( response, iqrf.PNUM_Node, '88', 0 );
};

//############################################################################################
