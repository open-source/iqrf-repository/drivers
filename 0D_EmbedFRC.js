﻿//############################################################################################

/* Title: Embedded FRC peripheral driver
See also: <https://www.iqrf.org/DpaTechGuide/>
*/

/*DriverDescription
{ 'ID' : 0x0D,
  'Type' : 'Standard',
  'Internal' : false,
  'Versions' : [
    { 'Version' : 0, 'Notes' : [
        'Initial release',
        'At iqrf.embed.frc.SetParams??? parameter frcResponseTime renamed to frcParams.'
        ]
    }
  ] }
DriverDescription*/

"use strict";
// Namespace: iqrf.embed.frc
// All prepared DPA requests have nadr set to '00'.
namespace( 'iqrf.embed.frc' );

// --------------
/* Function: iqrf.embed.frc.Send_Request
Encodes DPA request to send a FRC command

Parameters:
  frcCommand - number: Specifies data to be collected.
  userData - array: User data.

Returns:
  iqrf.DpaRawHdpMessage: DPA request object.
*/
iqrf.embed.frc.Send_Request = function ( frcCommand, userData )
{
  return new iqrf.DpaRawHdpMessageCoordinator( iqrf.PNUM_FRC, '00', iqrf.ToHexStringByte( frcCommand ) + iqrf.BytesToHexStringBytesArray( userData, true ) );
};

/* Function: iqrf.embed.frc.Send_Response
Decodes DPA response from sending a FRC command. It also handles response from iqrf.embed.frc.SendSelective_Request.

Parameters:
  response - iqrf.DpaRawHdpMessage: DPA response object.

Returns:
  object: Object with the following fields (see DPA documentation https://www.iqrf.org/DpaTechGuide/ for details):

* status - number
* frcData - array: Array of bytes that corresponds to the array of bytes returned by the low level IQRF OS function sendFrc. Please note that there is always a dummy FRC value (2 bits, 1 byte, 2 bytes, 4 bytes, according to the FRC command) at the "index" 0 corresponding to the coordinator.
*/
iqrf.embed.frc.Send_Response = function ( response )
{
  var responseData = iqrf.CheckResponsePnumPcmdDlen( response, iqrf.PNUM_FRC, '80.82', -2 );

  var result =
  {
    status: responseData[0],
    frcData: responseData.slice( 1, responseData.length )
  };

  return result;
};

// --------------
/* Function: iqrf.embed.frc.ExtraResult_Request
Encodes DPA request to read remaining bytes of the FRC result.

Returns:
  iqrf.DpaRawHdpMessage: DPA request object.
*/
iqrf.embed.frc.ExtraResult_Request = function ()
{
  return new iqrf.DpaRawHdpMessageCoordinator( iqrf.PNUM_FRC, '01' );
};

/* Function: iqrf.embed.frc.ExtraResult_Response
Decodes DPA response from reading remaining bytes of the FRC result.

Parameters:
  response - iqrf.DpaRawHdpMessage: DPA response object.

Returns:
  array: Remaining FRC data. See iqrf.embed.frc.Send_Response and DPA documentation https://www.iqrf.org/DpaTechGuide/ for details.
*/
iqrf.embed.frc.ExtraResult_Response = function ( response )
{
  return iqrf.CheckResponsePnumPcmdDlen( response, iqrf.PNUM_FRC, '81', -1 );
};

// --------------
/* Function: iqrf.embed.frc.SendSelective_Request
Encodes DPA request to send a selective FRC command.

Parameters:
  frcCommand - number: Specifies data to be collected.
  selectedNodes - array: Array if integer values corresponding to the selected nodes.
  userData - array: User data.

Returns:
  iqrf.DpaRawHdpMessage: DPA request object.
*/
iqrf.embed.frc.SendSelective_Request = function ( frcCommand, selectedNodes, userData )
{
  return new iqrf.DpaRawHdpMessageCoordinator( iqrf.PNUM_FRC, '02',
    iqrf.ToHexStringByte( frcCommand ) +
    iqrf.BytesToHexStringBytesArray( iqrf.IndexesToBitmap( selectedNodes, 30 ), true ) +
    iqrf.BytesToHexStringBytesArray( userData, true ) );
};

/* Function: iqrf.embed.frc.SendSelective_Response
Decodes DPA response from sending a selective FRC command.  It also handles response from iqrf.embed.frc.Send_Request.

Parameters:
  response - iqrf.DpaRawHdpMessage: DPA response object.

Returns:
  Object with the following fields (see DPA documentation https://www.iqrf.org/DpaTechGuide/ for details):

* status - number
* frcData - array:  Array of bytes that corresponds to the array of bytes returned by the low level IQRF OS function sendFrc. Please note that there is always a dummy FRC value (2 bits, 1 byte, 2 bytes, 4 bytes, according to the FRC command) at the "index" 0. Other indexes correspond to the index of node address in the selectedNodes (sorted) array parameter of iqrf.embed.frc.SendSelective_Request plus one.
*/
iqrf.embed.frc.SendSelective_Response = function ( response )
{
  return iqrf.embed.frc.Send_Response( response );
};

// --------------
/* Function: iqrf.embed.frc.SetParams_Request
Encodes DPA request to set global FRC parameters.

Parameters:
  frcParams - number: See DPA documentation https://www.iqrf.org/DpaTechGuide for details.

Returns:
  iqrf.DpaRawHdpMessage: DPA request object.
*/
iqrf.embed.frc.SetParams_Request = function ( frcParams )
{
  return new iqrf.DpaRawHdpMessageCoordinator( iqrf.PNUM_FRC, '03', iqrf.ToHexStringByte( frcParams ) );
};

/* Function: iqrf.embed.frc.SetParams_Response
Decodes DPA response from setting global FRC parameters.

Parameters:
  response - iqrf.DpaRawHdpMessage: DPA response object.

Returns:
  number: Previous FRCparams value.
*/
iqrf.embed.frc.SetParams_Response = function ( response )
{
  var responseData = iqrf.CheckResponsePnumPcmdDlen( response, iqrf.PNUM_FRC, '83', 1 );
  return responseData[0];
};

/* Function: iqrf.embed.frc.ParseResponses
Decodes DPA responses returned by iqrf.embed.frc.Send[Selective]_Response and iqrf.embed.frc.ExtraResult_Response

Parameters:
  frcCommand - number: FRC command passed to iqrf.embed.frc.Send[Selective]_Request
  sendResponse - object: Return value from iqrf.embed.frc.Send[Selective]_Response
  extraResultResponse - array: [optional] Return value from iqrf.embed.frc.ExtraResult_Response. For smaller networks extra FRC result might not be needed.

Returns:
  array: Based on the FRC command the items of the array are 2 bits, 1 byte, 2-byte or 4-byte numbers. Please note that there is always a dummy FRC value at the index 0 corresponding to the coordinator.

--- Text
Example:

* Input
// Both sendResponse.frcData[] and extraResultResponse full of 0s except:
sendResponse.frcData[2]  = 6
sendResponse.frcData[34] = 2

* Result (all other array items are 0)
// for 2 bits FRC command
{
  "17": 3,
  "18": 1
}

// for 1 byte FRC command
{
  "2": 6,
  "34": 2
}

// for 2 bytes FRC command
{
  "1": 6,
  "17": 2
}

// for 4 bytes FRC command
{
  "8": 131072
}
---
*/
iqrf.embed.frc.ParseResponses = function ( frcCommand, sendResponse, extraResultResponse )
{
  if ( frcCommand < 0 || frcCommand > 0xff )
    throw new Error( 'iqrf.embed.frc.ParseResponses: Parameter frcCommand = ' + frcCommand + ' is out of range.' );

  if ( sendResponse.status === 0xFD || sendResponse.status === 0xFE )
    throw new Error( 'iqrf.embed.frc.ParseResponses: Cannot parse FRC values because the error status code ' + sendResponse.status + ' from iqrf.embed.frc.Send[Selective]_Request' );

  var frcData;
  if ( extraResultResponse !== undefined )
    frcData = sendResponse.frcData.concat( extraResultResponse );
  else
    frcData = sendResponse.frcData;

  var result = [];
  var emptyNetwork = sendResponse.status === 0xFF;

  var frcData_length = frcData.length;
  if ( frcCommand <= 0x7F )
  {
    // 2 bits FRC
    for ( var index = 0; index <= 239; index++ )
    {
      var mask = 1 << ( index % 8 );
      var valueIndex = Math.floor( index / 8 );

      if ( ( valueIndex + 32 ) >= frcData_length )
        break;

      var frcValue = 0;
      if ( !emptyNetwork )
      {
        if ( ( frcData[valueIndex] & mask ) !== 0 )
          frcValue = 0x01;

        if ( ( frcData[valueIndex + 32] & mask ) !== 0 )
          frcValue |= 0x02;
      }

      result[index] = frcValue;
    }
  }
  else if ( frcCommand <= 0xDF )
    // 1 byte FRC
    for ( var byteIndex = 0; byteIndex < frcData_length; byteIndex++ )
    {
      var frcValue1B = !emptyNetwork ? frcData[byteIndex] : 0;
      result[byteIndex] = frcValue1B;
    }
  else if ( frcCommand <= 0xF7 )
    // 2 bytes FRC
    for ( var byte2index = 2; byte2index < frcData_length - 1; byte2index += 2 )
    {
      var frcValue2B = !emptyNetwork ? ( frcData[byte2index] + ( frcData[byte2index + 1] * 0x100 ) ) : 0;
      result[byte2index / 2] = frcValue2B;
    }
  else
    // 4 bytes FRC
    for ( var byte4index = 4; byte4index < frcData_length - 3; byte4index += 4 )
    {
      var frcValue4B = !emptyNetwork ? ( frcData[byte4index] + ( frcData[byte4index + 1] * 0x100 ) + ( frcData[byte4index + 2] * 0x10000 ) + ( frcData[byte4index + 3] * 0x1000000 ) ) : 0;
      result[byte4index / 4] = frcValue4B;
    }

  // Make sure there are no data from coordinator for sure
  result[0] = 0;
  return result;
};

//############################################################################################
