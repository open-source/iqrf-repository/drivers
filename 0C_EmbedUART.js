﻿//############################################################################################

/* Title: Embedded UART peripheral driver
See also: <https://www.iqrf.org/DpaTechGuide/>
*/

/*DriverDescription
{ 'ID' : 0x0C,
  'Type' : 'Standard',
  'Internal' : false,
  'Versions' : [
    { 'Version' : 0, 'Notes' : [
        'Initial release',
        ]
    }
  ] }
DriverDescription*/

"use strict";
// Namespace: iqrf.embed.uart
namespace( 'iqrf.embed.uart' );

// Baudrates
/* Const: iqrf.embed.uart.DpaBaud_1200
*/
iqrf.embed.uart.DpaBaud_1200 = 0x00;
/* Const: iqrf.embed.uart.DpaBaud_2400
*/
iqrf.embed.uart.DpaBaud_2400 = 0x01;
/* Const: iqrf.embed.uart.DpaBaud_4800
*/
iqrf.embed.uart.DpaBaud_4800 = 0x02;
/* Const: iqrf.embed.uart.DpaBaud_9600
*/
iqrf.embed.uart.DpaBaud_9600 = 0x03;
/* Const: iqrf.embed.uart.DpaBaud_19200
*/
iqrf.embed.uart.DpaBaud_19200 = 0x04;
/* Const: iqrf.embed.uart.DpaBaud_38400
*/
iqrf.embed.uart.DpaBaud_38400 = 0x05;
/* Const: iqrf.embed.uart.DpaBaud_57600
*/
iqrf.embed.uart.DpaBaud_57600 = 0x06;
/* Const: iqrf.embed.uart.DpaBaud_115200
*/
iqrf.embed.uart.DpaBaud_115200 = 0x07;
/* Const: iqrf.embed.uart.DpaBaud_230400
*/
iqrf.embed.uart.DpaBaud_230400 = 0x08;

// --------------
/* Function: iqrf.embed.uart.Open_Request
Encodes DPA request to open UART peripheral.

Parameters:
  baudRate - number: BaudRate value. See iqrf.embed.uart.DpaBaud_??? constants.

Returns:
  iqrf.DpaRawHdpMessage: DPA request object.
*/
iqrf.embed.uart.Open_Request = function ( baudRate )
{
  return new iqrf.DpaRawHdpMessage( iqrf.PNUM_UART, '00', iqrf.ToHexStringByte( baudRate ) );
};

/* Function: iqrf.embed.uart.Open_Response
Decodes DPA response from opening UART peripheral

Parameters:
  response - iqrf.DpaRawHdpMessage: DPA response object.
*/
iqrf.embed.uart.Open_Response = function ( response )
{
  iqrf.CheckResponsePnumPcmdDlen( response, iqrf.PNUM_UART, '80', 0 );
};

// --------------
/* Function: iqrf.embed.uart.Close_Request
Encodes DPA request to close UART peripheral.

Returns:
  iqrf.DpaRawHdpMessage: DPA request object.
*/
iqrf.embed.uart.Close_Request = function ()
{
  return new iqrf.DpaRawHdpMessage( iqrf.PNUM_UART, '01' );
};

/* Function: iqrf.embed.uart.Close_Response
Decodes DPA response from closing UART peripheral.

Parameters:
  response - iqrf.DpaRawHdpMessage: DPA response object.
*/
iqrf.embed.uart.Close_Response = function ( response )
{
  iqrf.CheckResponsePnumPcmdDlen( response, iqrf.PNUM_UART, '81', 0 );
};

// --------------
/* Function: iqrf.embed.uart.WriteRead_Request
Encodes DPA request to write and/or read data to/from UART peripheral.

Parameters:
  readTimeout - number:
  writtenData - array: Array of bytes.

Returns:
  iqrf.DpaRawHdpMessage: DPA request object.
*/
iqrf.embed.uart.WriteRead_Request = function ( readTimeout, writtenData )
{
  return new iqrf.DpaRawHdpMessage( iqrf.PNUM_UART, '02', iqrf.ToHexStringByte( readTimeout ) + iqrf.BytesToHexStringBytesArray( writtenData, true ) );
};

/* Function: iqrf.embed.uart.WriteRead_Response
Decodes DPA response from writing and/or reading data to/from UART peripheral.

Parameters:
  response - iqrf.DpaRawHdpMessage: DPA response object.

Returns:
  Read data.
*/
iqrf.embed.uart.WriteRead_Response = function ( response )
{
  return iqrf.CheckResponsePnumPcmdDlen( response, iqrf.PNUM_UART, '82' );
};

// --------------
/* Function: iqrf.embed.uart.ClearWriteRead_Request
Encodes DPA request to clear rx buffer and then write and/or read data to/from UART peripheral.

Parameters:
  readTimeout - number:
  writtenData - array: Array of bytes.

Returns:
  iqrf.DpaRawHdpMessage: DPA request object.
*/
iqrf.embed.uart.ClearWriteRead_Request = function ( readTimeout, writtenData )
{
  return new iqrf.DpaRawHdpMessage( iqrf.PNUM_UART, '03', iqrf.ToHexStringByte( readTimeout ) + iqrf.BytesToHexStringBytesArray( writtenData, true ) );
};

/* Function: iqrf.embed.uart.ClearWriteRead_Response
Decodes DPA response from clearing rx buffer and writing and/or reading data to/from UART peripheral.

Parameters:
  response - iqrf.DpaRawHdpMessage: DPA response object.

Returns:
  array: Read data.
*/
iqrf.embed.uart.ClearWriteRead_Response = function ( response )
{
  return iqrf.CheckResponsePnumPcmdDlen( response, iqrf.PNUM_UART, '83' );
};

//############################################################################################
