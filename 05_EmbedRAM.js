﻿//############################################################################################

/* Title: Embedded RAM peripheral driver
See also: <https://www.iqrf.org/DpaTechGuide/>
*/

/*DriverDescription
{ 'ID' : 0x05,
  'Type' : 'Standard',
  'Internal' : false,
  'Versions' : [
    { 'Version' : 0, 'Notes' : [
        'Initial release',
        ]
    }
  ] }
DriverDescription*/

"use strict";
// Namespace: iqrf.embed.ram
namespace( 'iqrf.embed.ram' );

// --------------
/* Function: iqrf.embed.ram.Read_Request
Encodes DPA request to read from memory.

Parameters:
  address - number: Memory address to read from.
  len - number: Length of data to read.

Returns:
  iqrf.DpaRawHdpMessage: DPA request object.
*/
iqrf.embed.ram.Read_Request = function ( address, len )
{
  return new iqrf.DpaRawHdpMessage( iqrf.PNUM_RAM, '00', iqrf.ToHexStringByte( address ) + '.' + iqrf.ToHexStringByte( len ) );
};

/* Function: iqrf.embed.ram.Read_Response
Decodes DPA response from reading from memory.

Parameters:
  response - iqrf.DpaRawHdpMessage: DPA response object.

Returns:
  array: Read data.
*/
iqrf.embed.ram.Read_Response = function ( response )
{
  return iqrf.CheckResponsePnumPcmdDlen( response, iqrf.PNUM_RAM, '80' );
};

// --------------
/* Function: iqrf.embed.ram.Write_Request
Encodes DPA request to write to memory.

Parameters:
  address - number: Address to write data to.
  pdata - array: Data to write.

Returns:
  iqrf.DpaRawHdpMessage: DPA request object.
*/
iqrf.embed.ram.Write_Request = function ( address, pdata )
{
  return new iqrf.DpaRawHdpMessage( iqrf.PNUM_RAM, '01', iqrf.ToHexStringByte( address ) + iqrf.BytesToHexStringBytesArray( pdata, true ) );
};

/* Function: iqrf.embed.ram.Write_Response
Decodes DPA response from writing to memory.

Parameters:
  response - iqrf.DpaRawHdpMessage: DPA response object.
*/
iqrf.embed.ram.Write_Response = function ( response )
{
  iqrf.CheckResponsePnumPcmdDlen( response, iqrf.PNUM_RAM, '81', 0 );
};

//############################################################################################
