﻿//############################################################################################

/* Title: IQRF Standards Light driver
See also: <https://www.iqrfalliance.org/techDocs/>
*/

/*DriverDescription
{ 'ID' : 0x4A,
  'Type' : 'Standard',
  'Internal' : false,
  'Versions' : [
    { 'Version' : 1, 'Notes' : [
        'Based on former and depreciated DALI standard V0' ]
    }
  ] }
DriverDescription*/

"use strict";
// Namespace: iqrf.light
namespace( 'iqrf.light' );

// IQRF Standards Light PNUM value
iqrf.light.PNUM = '4A';

// FRC commands
/* Const: iqrf.light.FRC_STD_LIGHT_LDI_SEND
FRC command to return answer of the provided LDI command.
*/
iqrf.light.FRC_STD_LIGHT_LDI_SEND = 0xE0;

/* Const: iqrf.light.FRC_STD_LIGHT_LAI_READ
FRC command to return values at LAI.
*/
iqrf.light.FRC_STD_LIGHT_LAI_READ = 0xE1;

iqrf.light.LightSendLdiCmds_Request = function ( commands, pcmd )
{
  var data = '';
  var commands_length = commands.length;
  if ( commands_length < 1 )
    throw new Error( 'iqrf.light.LightSendLdiCmds_Request: No LDI commands specified.' );

  for ( var index = 0; index < commands_length; index++ )
  {
    var oneCmd = commands[index];
    if ( typeof oneCmd !== 'number' || oneCmd < 0 || oneCmd > 0xFfFf )
      throw new Error( 'iqrf.light.LightSendLdiCmds_Request: Invalid LDI command value ' + oneCmd );

    if ( data.length !== 0 )
      data += '.';
    data += iqrf.ToHexStringByte( oneCmd >> 8 ) + '.' + iqrf.ToHexStringByte( oneCmd & 0xFF );
  }
  return new iqrf.DpaRawHdpMessage( iqrf.light.PNUM, pcmd, data );
};

/* Function: iqrf.light.SendLdiCommands_Request
Send LDI commands and returns answers synchronously.

Parameters:
  commands - array: Array of 2 bytes (16 bits) wide values, each representing one LDI (DALI) command at the standard format YAAAAAAS+DDDDDDDD.

Returns:
  iqrf.DpaRawHdpMessage: DPA request object.
*/
iqrf.light.SendLdiCommands_Request = function ( commands )
{
  return iqrf.light.LightSendLdiCmds_Request( commands, '00' );
};

/* Function: iqrf.light.SendLdiCommands_Response
Decodes DPA response from sending LDI commands synchronously.

Parameters:
  response - iqrf.DpaRawHdpMessage: DPA response object.

Returns:
  array: Array of objects each representing answer to the respective LDI (DALI) command.

      * status - number: See IQRF Light standard for details.
      * value - number: See IQRF Light standard for details.
*/
iqrf.light.SendLdiCommands_Response = function ( response )
{
  var responseData = iqrf.CheckResponsePnumPcmdDlen( response, iqrf.light.PNUM, '80', -2 );

  var result = [];
  var responseData_length = responseData.length;
  for ( var index = 0; index < responseData_length; )
  {
    result[result.length] =
    {
      status: responseData[index++],
      value: responseData[index++]
    };
  }

  return result;
};

/* Function: iqrf.light.SendLdiCommandsAsync_Request
Send LDI commands and returns answers asynchronously.

Parameters:
  commands - array: Array of 2 bytes (16 bits) wide values, each representing one LDI (DALI) command at the standard format YAAAAAAS+DDDDDDDD.

Returns:
  iqrf.DpaRawHdpMessage: DPA request object.
*/
iqrf.light.SendLdiCommandsAsync_Request = function ( commands )
{
  return iqrf.light.LightSendLdiCmds_Request( commands, '01' );
};

/* Function: iqrf.light.SendLdiCommandsAsync_Response
Decodes DPA response from sending LDI commands synchronously.

Parameters:
  response - iqrf.DpaRawHdpMessage: DPA response object.
*/
iqrf.light.SendLdiCommandsAsync_Response = function ( response )
{
  iqrf.CheckResponsePnumPcmdDlen( response, iqrf.light.PNUM, '81', 0 );
};

/* Function: iqrf.light.SetLai_Request
Sets the voltage of the 0-10 V lighting analog interface (LAI).

Parameters:
  value - The desired voltage at LAI at the Extra-low Voltage format. If the value is 0x8000 then the voltage is not set. If the value is out of the interval 0-10 V then ERROR_FAIL is returned.

Returns:
  iqrf.DpaRawHdpMessage: DPA request object.
*/
iqrf.light.SetLai_Request = function ( value )
{
  return new iqrf.DpaRawHdpMessage( iqrf.light.PNUM, '02', iqrf.IntToHexStringBytesArray( value, 2 ) );
};

/* Function: iqrf.light.SetLai_Response
Decodes DPA response from seting the voltage of the 0-10 V lighting analog interface (LAI).

Parameters:
  response - iqrf.DpaRawHdpMessage: DPA response object.

Returns:
  number: Voltage at the LAI at the Extra-low Voltage format before the request was received.
*/
iqrf.light.SetLai_Response = function ( response )
{
  var responseData = iqrf.CheckResponsePnumPcmdDlen( response, iqrf.light.PNUM, '82', -2 );
  return responseData[0] + ( responseData[1] * 0x100 );
};

/* Function: iqrf.light.FrcLdiSend_Request
Prepares FRC request to execute LDI FRC command.
_Requires FRC embedded peripheral driver_.

Parameters:
  command - number: LDI command, see <iqrf.light.SendLdiCommands_Request>.
  selectedNodes - array: [optional] Array if integer values corresponding to the selected nodes. Use default value or empty array to select all nodes.

Returns:
  array: 2 items long array. 1st item is a prepared request to initiate the RFC. 2nd item is a prepared request to get additional FRC data from the network. For smaller networks extra FRC result might not be needed.
*/
iqrf.light.FrcLdiSend_Request = function ( command, selectedNodes )
{
  var userData = [0x4A, command >> 8, command & 0xFF, 0];

  var result = [];
  result[0] = selectedNodes === undefined || selectedNodes.length === 0 ? iqrf.embed.frc.Send_Request( iqrf.light.FRC_STD_LIGHT_LDI_SEND, userData ) : iqrf.embed.frc.SendSelective_Request( iqrf.light.FRC_STD_LIGHT_LDI_SEND, selectedNodes, userData );
  result[1] = iqrf.embed.frc.ExtraResult_Request();
  return result;
};

/* Function: iqrf.light.FrcLdiSend_Response
Parses FRC response into LDI answer values.
_Requires FRC embedded peripheral driver_.

Parameters:
  responseFrcSend - iqrf.DpaRawHdpMessage: Parameter value to be passed to iqrf.embed.frc.Send_Response
  responseFrcExtraResult - iqrf.DpaRawHdpMessage: [optional] Value to be passed to iqrf.embed.frc.ExtraResult_Response. For smaller networks extra FRC result might not be needed.

Returns:
  array: Array of objects for every LDI answer. Object array index represent the node address (or index+1 in case selectedNodes parameter was used). The object has the same fields as object at the return  value of <iqrf.light.SendLdiCommands_Response>. Please see iqrf.embed.frc.ParseResponses for details.
*/
iqrf.light.FrcLdiSend_Response = function ( responseFrcSend, responseFrcExtraResult )
{
  var responseFrcExtraResultParsed;
  if ( responseFrcExtraResult !== undefined )
    responseFrcExtraResultParsed = iqrf.embed.frc.ExtraResult_Response( responseFrcExtraResult );
  var parsedFrc = iqrf.embed.frc.ParseResponses( iqrf.light.FRC_STD_LIGHT_LDI_SEND, iqrf.embed.frc.Send_Response( responseFrcSend ), responseFrcExtraResultParsed );

  var result = [];
  for ( var index in parsedFrc )
  {
    var frcValue = parsedFrc[index];
    result[index] =
    {
      status: frcValue & 0xFF,
      value: frcValue >> 8
    };
  }

  return result;
};

/* Function: iqrf.light.FrcLaiRead_Request
Prepares FRC request to execute LDA FRC command.
_Requires FRC embedded peripheral driver_.

Parameters:
  selectedNodes - array: [optional] Array if integer values corresponding to the selected nodes. Use default value or empty array to select all nodes.

Returns:
  array: 2 items long array. 1st item is a prepared request to initiate the RFC. 2nd item is a prepared request to get additional FRC data from the network. For smaller networks extra FRC result might not be needed.
*/
iqrf.light.FrcLaiRead_Request = function ( selectedNodes )
{
  var userData = [0x4A, 0];

  var result = [];
  result[0] = selectedNodes === undefined || selectedNodes.length === 0 ? iqrf.embed.frc.Send_Request( iqrf.light.FRC_STD_LIGHT_LAI_READ, userData ) : iqrf.embed.frc.SendSelective_Request( iqrf.light.FRC_STD_LIGHT_LAI_READ, selectedNodes, userData );
  result[1] = iqrf.embed.frc.ExtraResult_Request();
  return result;
};

/* Function: iqrf.light.FrcLaiRead_Response
Parses FRC response into LDA answer values.
_Requires FRC embedded peripheral driver_.

Parameters:
  responseFrcSend - iqrf.DpaRawHdpMessage: Parameter value to be passed to iqrf.embed.frc.Send_Response
  responseFrcExtraResult - iqrf.DpaRawHdpMessage: [optional] Value to be passed to iqrf.embed.frc.ExtraResult_Response. For smaller networks extra FRC result might not be needed.

Returns:
  array: Array of values for every LDA read answer. Value array index represent the node address (or index+1 in case selectedNodes parameter was used).
*/
iqrf.light.FrcLaiRead_Response = function ( responseFrcSend, responseFrcExtraResult )
{
  var responseFrcExtraResultParsed;
  if ( responseFrcExtraResult !== undefined )
    responseFrcExtraResultParsed = iqrf.embed.frc.ExtraResult_Response( responseFrcExtraResult );
  var parsedFrc = iqrf.embed.frc.ParseResponses( iqrf.light.FRC_STD_LIGHT_LAI_READ, iqrf.embed.frc.Send_Response( responseFrcSend ), responseFrcExtraResultParsed );

  var result = [];
  for ( var index in parsedFrc )
  {
    var frcValue = parsedFrc[index];
    result[index] = frcValue === 0 ? NaN : iqrf.UInt16toInt16( frcValue ^ 0x8000 ) / 1000.0/*from mV to V*/;
  }

  return result;
};

//############################################################################################
