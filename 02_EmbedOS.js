//############################################################################################

/* Title: Embedded OS peripheral driver
See also: <https://www.iqrf.org/DpaTechGuide/>
*/

/*DriverDescription
{ 'ID' : 0x02,
  'Type' : 'Standard',
  'Internal' : false,
  'Versions' : [
    { 'Version' : 2.01, 'VersionFlags' : 0, 'Notes' : [
        'Indicate added'
        ]
    },
    { 'Version' : 2.00, 'VersionFlags' : 1, 'Notes' : [
        'Read_Response newly returns enumeration fields compatible with DPA >= 4.10',
        'FactorySettings added'
        ]
    },
    { 'Version' : 1.00, 'VersionFlags' : 1, 'Notes' : [
        'Read_Response returns ibk field compatible with DPA > 3.02',
        'WriteCfg_Request - checksum parameter removed',
        'TestRfSignal added'
        ]
    },
    { 'Version' : 0.00, 'Notes' : [
        'Initial release',
        ]
    }
  ] }
DriverDescription*/

"use strict";
// Namespace: iqrf.embed.os
namespace( 'iqrf.embed.os' );

/* Function: iqrf.embed.os.Read_Request
Encodes DPA request to read OS.

Returns:
  iqrf.DpaRawHdpMessage: DPA request object.
*/
iqrf.embed.os.Read_Request = function ()
{
  return new iqrf.DpaRawHdpMessage( iqrf.PNUM_OS, '00' );
};

/* Function: iqrf.embed.os.Read_Response
Decodes DPA response from reading OS.
The method can be (intentionally) run on older DPA versions. At DPA < 3.03 it returns fields up to (including) "slotLimits". At DPA < 4.10 it returns fields up to (including) "ibk".

Parameters:
  response - iqrf.DpaRawHdpMessage: DPA response object.

Returns:
  object: Object with the following fields (see DPA documentation https://www.iqrf.org/DpaTechGuide/ for details):

* mid - number:
* osVersion - number:
* trMcuType - number:
* osBuild - number:
* rssi - number:
* supplyVoltage - number: contains real voltage value
* flags - number:
* slotLimits - number:
* ibk - array: (from version 1.00 for DPA 3.03+)
* dpaVer - number: (from version 2.00 for DPA 4.10+)
* perNr - number: (from version 2.00 for DPA 4.10+)
* embeddedPers - array: (from version 2.00 for DPA 4.10+)
* hwpid - number: (from version 2.00 for DPA 4.10+)
* hwpidVer - number: (from version 2.00 for DPA 4.10+)
* flagsEnum - number: (from version 2.00 for DPA 4.10+)
* userPer - array: (from version 2.00 for DPA 4.10+)
*/
iqrf.embed.os.Read_Response = function ( response )
{
  var responseData = iqrf.CheckResponsePnumPcmdDlen( response, iqrf.PNUM_OS, '80', -12 );

  var result =
  {
    mid: responseData[0] + ( responseData[1] * 0x100 ) + ( responseData[2] * 0x10000 ) + ( responseData[3] * 0x1000000 ),
    osVersion: responseData[4],
    trMcuType: responseData[5],
    osBuild: responseData[6] + ( responseData[7] * 0x100 ),
    rssi: responseData[8],
    supplyVoltage: 261.12 / ( 127 - responseData[9] ),
    flags: responseData[10],
    slotLimits: responseData[11]
  };

  // False at DPA < 3.03
  if ( responseData.length > 12 )
    result.ibk = responseData.slice( 12, 28 );

  // Only in DSM the next condition would be false or at DPA < 4.10
  if ( responseData.length > 28 )
  {
    result.dpaVer = responseData[28 + 0] + ( responseData[28 + 1] * 0x100 );
    result.perNr = responseData[28 + 2];
    result.embeddedPers = iqrf.BitmapToIndexes( responseData, 28 + 3, 28 + 6 );
    result.hwpid = responseData[28 + 7] + ( responseData[28 + 8] * 0x100 );
    result.hwpidVer = responseData[28 + 9] + ( responseData[28 + 10] * 0x100 );
    result.flagsEnum = responseData[28 + 11];
    result.userPer = iqrf.BitmapToIndexes( responseData, 28 + 12, responseData.length - 1, 0x20 );
  }

  return result;
};

// --------------
/* Function: iqrf.embed.os.Reset_Request
Encodes DPA request to reset.

Returns:
  iqrf.DpaRawHdpMessage: DPA request object.
*/
iqrf.embed.os.Reset_Request = function ()
{
  return new iqrf.DpaRawHdpMessage( iqrf.PNUM_OS, '01' );
};

/* Function: iqrf.embed.os.Reset_Response
Decodes DPA response from reset.

Parameters:
  response - iqrf.DpaRawHdpMessage: DPA response object.
*/
iqrf.embed.os.Reset_Response = function ( response )
{
  iqrf.CheckResponsePnumPcmdDlen( response, iqrf.PNUM_OS, '81', 0 );
};

// --------------
/* Function: iqrf.embed.os.ReadCfg_Request
Encodes DPA request to read configuration.

Returns:
  iqrf.DpaRawHdpMessage: DPA request object.
*/
iqrf.embed.os.ReadCfg_Request = function ()
{
  return new iqrf.DpaRawHdpMessage( iqrf.PNUM_OS, '02' );
};

/* Function: iqrf.embed.os.ReadCfg_Response
Decodes DPA response from reading configuration.

Parameters:
  response - iqrf.DpaRawHdpMessage: DPA response object.

Returns:
  object: Object with the following fields (see DPA documentation https://www.iqrf.org/DpaTechGuide for details):

* checksum - number: The Checksum byte XORed with all Configuration bytes gives 0x5F.
* configuration - array: Content the configuration memory block from address 0x01 to 0x1F.
* rfpgm - number: See the parameter of setupRFPGM IQRF OS function.
* initphy - number: See Read TR Configuration at DPA documentation.
*/
iqrf.embed.os.ReadCfg_Response = function ( response )
{
  var responseData = iqrf.CheckResponsePnumPcmdDlen( response, iqrf.PNUM_OS, '82', 34 );

  var result =
  {
    checksum: responseData[0],
    configuration: responseData.slice( 1, 32 ),
    rfpgm: responseData[32],
    initphy: responseData[33]
  };

  return result;
};

// --------------
/* Function: iqrf.embed.os.Rfpgm_Request
Encodes DPA request to start RFPGM.

Returns:
  iqrf.DpaRawHdpMessage: DPA request object.
*/
iqrf.embed.os.Rfpgm_Request = function ()
{
  return new iqrf.DpaRawHdpMessage( iqrf.PNUM_OS, '03' );
};

/* Function: iqrf.embed.os.Rfpgm_Response
Decodes DPA response from starting RFPGM.

Parameters:
  response - iqrf.DpaRawHdpMessage: DPA response object.
*/
iqrf.embed.os.Rfpgm_Response = function ( response )
{
  iqrf.CheckResponsePnumPcmdDlen( response, iqrf.PNUM_OS, '83', 0 );
};

// --------------
/* Function: iqrf.embed.os.Sleep_Request
Encodes DPA request to sleep.

Parameters:
  time - number: See DPA documentation https://www.iqrf.org/DpaTechGuide for details.
  control - number: See DPA documentation https://www.iqrf.org/DpaTechGuide for details.

Returns:
  iqrf.DpaRawHdpMessage: DPA request object.
*/
iqrf.embed.os.Sleep_Request = function ( time, control )
{
  return new iqrf.DpaRawHdpMessage( iqrf.PNUM_OS, '04', iqrf.IntToHexStringBytesArray( time, 2 ) + '.' + iqrf.ToHexStringByte( control ) );
};

/* Function: iqrf.embed.os.Sleep_Response
Decodes DPA response from sleeping.

Parameters:
  response - iqrf.DpaRawHdpMessage: DPA response object.
*/
iqrf.embed.os.Sleep_Response = function ( response )
{
  iqrf.CheckResponsePnumPcmdDlen( response, iqrf.PNUM_OS, '84', 0 );
};

iqrf.embed.os.Batch_ConstructRequests = function ( requests )
{
  var data = '';
  var requests_length = requests.length;
  for ( var index = 0; index < requests_length; index++ )
  {
    var oneRequest = requests[index];
    var hwpid = parseInt( oneRequest.hwpid, 16 );
    if ( isNaN( hwpid ) )
      hwpid = 0xFfFf;
    var oneData =
      iqrf.NormalizeStringByte( oneRequest.pnum ) + '.' +
      iqrf.NormalizeStringByte( oneRequest.pcmd ) + '.' +
      iqrf.ToHexStringByte( hwpid & 0xFF ) + '.' +
      iqrf.ToHexStringByte( hwpid >> 8 );

    if ( oneRequest.rdata !== undefined )
      oneData += '.' + iqrf.BytesToHexStringBytesArray( iqrf.ParseStringBytes( oneRequest.rdata ) );

    oneData = iqrf.ToHexStringByte( 1 + ( 1 + oneData.length ) / 3 ) + '.' + oneData;

    if ( data.length !== 0 )
      data += '.';
    data += oneData;
  }

  return data + '.00';
};

// --------------
/* Function: iqrf.embed.os.Batch_Request
Encodes DPA request to execute a batch.

Parameters:
  requests - array: Array of objects (requests) created by other ?_Request functions.

Returns:
  iqrf.DpaRawHdpMessage: DPA request object.
*/
iqrf.embed.os.Batch_Request = function ( requests )
{
  return new iqrf.DpaRawHdpMessage( iqrf.PNUM_OS, '05', iqrf.embed.os.Batch_ConstructRequests( requests ) );
};

/* Function: iqrf.embed.os.Batch_Response
Decodes DPA response from executing a batch.

Parameters:
  response - iqrf.DpaRawHdpMessage: DPA response object.
*/
iqrf.embed.os.Batch_Response = function ( response )
{
  iqrf.CheckResponsePnumPcmdDlen( response, iqrf.PNUM_OS, '85', 0 );
};

// --------------
/* Function: iqrf.embed.os.SetSecurity_Request
Encodes DPA request to set various security parameters.

Parameters:
  type - number: see DPA documentation https://www.iqrf.org/DpaTechGuide for details.
  data - array: see DPA documentation https://www.iqrf.org/DpaTechGuide for details.

Returns:
  iqrf.DpaRawHdpMessage: DPA request object.
*/
iqrf.embed.os.SetSecurity_Request = function ( type, data )
{
  return new iqrf.DpaRawHdpMessage( iqrf.PNUM_OS, '06', iqrf.ToHexStringByte( type ) + iqrf.BytesToHexStringBytesArray( data, true ) );
};

/* Function: iqrf.embed.os.SetSecurity_Response
Decodes DPA response from setting various security parameters.

Parameters:
  response - iqrf.DpaRawHdpMessage: DPA response object.
*/
iqrf.embed.os.SetSecurity_Response = function ( response )
{
  iqrf.CheckResponsePnumPcmdDlen( response, iqrf.PNUM_OS, '86', 0 );
};

// --------------
/* Function: iqrf.embed.os.Restart_Request
Encodes DPA request to restart device.

Returns:
  iqrf.DpaRawHdpMessage: DPA request object.
*/
iqrf.embed.os.Restart_Request = function ()
{
  return new iqrf.DpaRawHdpMessage( iqrf.PNUM_OS, '08' );
};

/* Function: iqrf.embed.os.Restart_Response
Decodes DPA response from restarting device.

Parameters:
  response - iqrf.DpaRawHdpMessage: DPA response object.
*/
iqrf.embed.os.Restart_Response = function ( response )
{
  iqrf.CheckResponsePnumPcmdDlen( response, iqrf.PNUM_OS, '88', 0 );
};

// --------------
/* Function: iqrf.embed.os.WriteCfgByte_Request
Encodes DPA request to write configuration bytes.

Parameters:
  bytes - array: Array of objects with the following fields (see DPA documentation https://www.iqrf.org/DpaTechGuide for details):

* address - number:
* value - number:
* mask - number:

Returns:
  iqrf.DpaRawHdpMessage: DPA request object.
*/
iqrf.embed.os.WriteCfgByte_Request = function ( bytes )
{
  var data = '';
  var bytes_length = bytes.length;
  for ( var index = 0; index < bytes_length; index++ )
  {
    if ( data.length !== 0 )
      data += '.';

    var oneByte = bytes[index];
    data +=
      iqrf.ToHexStringByte( oneByte.address ) + '.' +
      iqrf.ToHexStringByte( oneByte.value ) + '.' +
      iqrf.ToHexStringByte( oneByte.mask );
  }

  return new iqrf.DpaRawHdpMessage( iqrf.PNUM_OS, '09', data );
};

/* Function: iqrf.embed.os.WriteCfgByte_Response
Decodes DPA response from writing configuration bytes.

Parameters:
  response - iqrf.DpaRawHdpMessage: DPA response object.
*/
iqrf.embed.os.WriteCfgByte_Response = function ( response )
{
  iqrf.CheckResponsePnumPcmdDlen( response, iqrf.PNUM_OS, '89', 0 );
};

// --------------
/* Function: iqrf.embed.os.LoadCode_Request
Encodes DPA request to load code.

Parameters:
  flags - number: see DPA documentation https://www.iqrf.org/DpaTechGuide for details.
  address - number: see DPA documentation https://www.iqrf.org/DpaTechGuide for details.
  length - number: see DPA documentation https://www.iqrf.org/DpaTechGuide for details.
  checkSum - number: see DPA documentation https://www.iqrf.org/DpaTechGuide for details.

Returns:
  iqrf.DpaRawHdpMessage: DPA request object.
*/
iqrf.embed.os.LoadCode_Request = function ( flags, address, length, checkSum )
{
  return new iqrf.DpaRawHdpMessage( iqrf.PNUM_OS, '0A',
    iqrf.ToHexStringByte( flags ) + '.' +
    iqrf.IntToHexStringBytesArray( address, 2 ) + '.' +
    iqrf.IntToHexStringBytesArray( length, 2 ) + '.' +
    iqrf.IntToHexStringBytesArray( checkSum, 2 ) );
};

/* Function: iqrf.embed.os.LoadCode_Response
Decodes DPA response from loading code.

Parameters:
  response - iqrf.DpaRawHdpMessage: DPA response object.

Returns:
  number: Loading code result value.
*/
iqrf.embed.os.LoadCode_Response = function ( response )
{
  var responseData = iqrf.CheckResponsePnumPcmdDlen( response, iqrf.PNUM_OS, '8A', 1 );
  return responseData[0];
};

// --------------
/* Function: iqrf.embed.os.SelectiveBatch_Request
Encodes DPA request to execute a selective batch.

Parameters:
  selectedNodes - array: Array if integer values corresponding to the selected nodes.
  requests - array: Array of objects (requests) created by other ?_Request functions.

Returns:
  iqrf.DpaRawHdpMessage: DPA request object.
*/
iqrf.embed.os.SelectiveBatch_Request = function ( selectedNodes, requests )
{
  return new iqrf.DpaRawHdpMessage( iqrf.PNUM_OS, '0B',
    iqrf.BytesToHexStringBytesArray( iqrf.IndexesToBitmap( selectedNodes, 30 ) ) + '.' + iqrf.embed.os.Batch_ConstructRequests( requests ) );
};

/* Function: iqrf.embed.os.SelectiveBatch_Response
Decodes DPA response from executing a selective batch.

Parameters:
  response - iqrf.DpaRawHdpMessage: DPA response object.
*/
iqrf.embed.os.SelectiveBatch_Response = function ( response )
{
  iqrf.CheckResponsePnumPcmdDlen( response, iqrf.PNUM_OS, '8B', 0 );
};

// --------------
/* Function: iqrf.embed.os.TestRfSignal_Request
Encodes DPA request to test RF signal.

Parameters:
  channel - number: The channel to test.
  rxFilter - number: RX filter value passed as a parameter to checkRF().
  time  - number: Time interval to test the signal. Unit is 10 ms.

Returns:
  iqrf.DpaRawHdpMessage: DPA request object.
*/
iqrf.embed.os.TestRfSignal_Request = function ( channel, rxFilter, time )
{
  return new iqrf.DpaRawHdpMessage( iqrf.PNUM_OS, '0C',
    iqrf.ToHexStringByte( channel ) + '.' + iqrf.ToHexStringByte( rxFilter ) + '.' + iqrf.IntToHexStringBytesArray( time, 2 ) );
};

/* Function: iqrf.embed.os.TestRfSignal_Response
Decodes DPA response from testing RF signal.

Parameters:
  response - iqrf.DpaRawHdpMessage: DPA response object.

Returns:
  number: See FRC_TestRFsignal FRC command documentation.
*/
iqrf.embed.os.TestRfSignal_Response = function ( response )
{
  var responseData = iqrf.CheckResponsePnumPcmdDlen( response, iqrf.PNUM_OS, '8C', 1 );

  return responseData[0];
};

// --------------
/* Function: iqrf.embed.os.FactorySettings_Request
Encodes DPA request for factory settings.

Returns:
  iqrf.DpaRawHdpMessage: DPA request object.
*/
iqrf.embed.os.FactorySettings_Request = function ()
{
  return new iqrf.DpaRawHdpMessage( iqrf.PNUM_OS, '0D' );
};

/* Function: iqrf.embed.os.FactorySettings_Response
Decodes DPA response from factory settings.

Parameters:
  response - iqrf.DpaRawHdpMessage: DPA response object.
*/
iqrf.embed.os.FactorySettings_Response = function ( response )
{
  iqrf.CheckResponsePnumPcmdDlen( response, iqrf.PNUM_OS, '8D', 0 );
};

// --------------
/* Function: iqrf.embed.os.WriteCfg_Request
Encodes DPA request to write configuration.

Parameters:
  configuration - array: see DPA documentation https://www.iqrf.org/DpaTechGuide for details.
  rfpgm - number: see DPA documentation https://www.iqrf.org/DpaTechGuide for details.

Returns:
  iqrf.DpaRawHdpMessage: DPA request object.
*/
iqrf.embed.os.WriteCfg_Request = function ( configuration, rfpgm )
{
  return new iqrf.DpaRawHdpMessage( iqrf.PNUM_OS, '0F', '00' + iqrf.BytesToHexStringBytesArray( configuration, true ) + '.' + iqrf.ToHexStringByte( rfpgm ) );
};

/* Function: iqrf.embed.os.WriteCfg_Response
Decodes DPA response from writing configuration.

Parameters:
  response - iqrf.DpaRawHdpMessage: DPA response object.
*/
iqrf.embed.os.WriteCfg_Response = function ( response )
{
  iqrf.CheckResponsePnumPcmdDlen( response, iqrf.PNUM_OS, '8F', 0 );
};

// --------------
/* Function: iqrf.embed.os.Indicate_Request
Encodes DPA request indicate the node.

Parameters:
  control - number: Indication control bits.

Returns:
  iqrf.DpaRawHdpMessage: DPA request object.
*/
iqrf.embed.os.Indicate_Request = function ( control )
{
  return new iqrf.DpaRawHdpMessage( iqrf.PNUM_OS, '07', iqrf.ToHexStringByte( control ) );
};

/* Function: iqrf.embed.os.Indicate_Response
Decodes DPA response from indicating the node.

Parameters:
  response - iqrf.DpaRawHdpMessage: DPA response object.
*/
iqrf.embed.os.Indicate_Response = function ( response )
{
  iqrf.CheckResponsePnumPcmdDlen( response, iqrf.PNUM_OS, '87', 0 );
};

//############################################################################################
