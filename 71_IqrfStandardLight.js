﻿//############################################################################################

/* Title: IQRF Standards Light driver
See also: <https://www.iqrfalliance.org/techDocs/>
*/

/*DriverDescription
{ 'ID' : 0x71,
  'Type' : 'Standard',
  'Internal' : false,
  'Versions' : [
    { 'Version' : 5, 'Notes' : [
        'Initial release' ]
    }
  ] }
DriverDescription*/

"use strict";
// Namespace: iqrf.light
namespace( 'iqrf.light' );

// IQRF Standards Light PNUM value
iqrf.light.PNUM = '71';

// FRC commands
/* Const: iqrf.light.STD_LIGHT_ONOFF_FRC
FRC command to return 2-bits light On/Off state.
*/
iqrf.light.STD_LIGHT_ONOFF_FRC = 0x10;
// FRC commands
/* Const: iqrf.light.STD_LIGHT_ALARM_FRC
FRC command to return 2-bits light alarm state.
*/
iqrf.light.STD_LIGHT_ALARM_FRC = 0x11;

/* Function: iqrf.light.Enumerate_Request
Encodes DPA request to enumerate lights.

Returns:
  iqrf.DpaRawHdpMessage: DPA request object.
*/
iqrf.light.Enumerate_Request = function ()
{
  return new iqrf.DpaRawHdpMessage( iqrf.light.PNUM, '3e' );
};

/* Function: iqrf.light.Enumerate_Response
Decodes DPA response from lights enumeration.

Parameters:
  response - iqrf.DpaRawHdpMessage: DPA response object.

Returns:
  number: Number of implemented lights.
*/
iqrf.light.Enumerate_Response = function ( response )
{
  var responseData = iqrf.CheckResponsePnumPcmdDlen( response, iqrf.light.PNUM, 'be', 1 );
  return responseData[0];
};

iqrf.light.LightCmd_Request = function ( lights, pcmd )
{
  var bitmap = 0x0000;
  var data = '';
  var lights_length = lights.length;
  for ( var index = 0; index < lights_length; index++ )
  {
    var oneLight = lights[index];
    bitmap |= 1 << oneLight.index;

    if ( oneLight.power !== 0x7F && ( oneLight.power < 0 || oneLight.power > 100 ) )
      throw new Error( 'iqrf.light.LightCmd_Request: Invalid power ' + oneLight.power + ' specified for the light ' + oneLight.index );

    var oneTime = undefined;
    var onePower = oneLight.power;
    if ( oneLight.time !== undefined )
    {
      if ( oneLight.time >= 1 && oneLight.time <= 127 )
        oneTime = oneLight.time | 0x80;
      else
      {
        var minutes = Math.floor( oneLight.time / 60 );
        if ( oneLight.time % 60 !== 0 || minutes < 2 || minutes > 127 )
          throw new Error( 'iqrf.light.LightCmd_Request: Invalid time ' + oneLight.time + ' specified for the light ' + oneLight.index );

        oneTime = minutes;
      }

      onePower |= 0x80;
    }

    data += '.' + iqrf.ToHexStringByte( onePower );
    if ( oneTime !== undefined )
      data += '.' + iqrf.ToHexStringByte( oneTime );
  }

  return new iqrf.DpaRawHdpMessage( iqrf.light.PNUM, pcmd, iqrf.IntToHexStringBytesArray( bitmap, 4, true ) + data );
};

/* Function: iqrf.light.SetPower_Request
Encodes DPA request to set power of lights.

Parameters:
  lights - array: Array of objects with the following fields (see documentation at https://www.iqrfalliance.org/techDocs/ for details):

* index - number: Zero based index of the light to set.
* power - number: Power level of the light from range <0;100>. Value 127 keeps current power level allowing to only report power level.
* time - number: [optional] Time in seconds to keep the light on. Allowed values are <1;127> or 60*<1;127>.

Returns:
  iqrf.DpaRawHdpMessage: DPA request object.
*/
iqrf.light.SetPower_Request = function ( lights )
{
  return iqrf.light.LightCmd_Request( lights, '00' );
};

/* Function: iqrf.light.SetPower_Response
Decodes DPA response from setting power of lights.

Parameters:
  response - iqrf.DpaRawHdpMessage: DPA response object.

Returns:
  array: Array integers corresponding to the previous power level of each selected light. If the unimplemented light was selected the returned value is 0.
*/
iqrf.light.SetPower_Response = function ( response )
{
  return iqrf.CheckResponsePnumPcmdDlen( response, iqrf.light.PNUM, '80', -1 );
};

/* Function: iqrf.light.IncrementPower_Request
Encodes DPA request to increment power of lights.
Same as <iqrf.light.SetPower_Request> but increases power level. See documentation for details.
*/
iqrf.light.IncrementPower_Request = function ( lights )
{
  return iqrf.light.LightCmd_Request( lights, '01' );
};

/* Function: iqrf.light.IncrementPower_Response
Decodes DPA response from incrementing power of lights.

Parameters:
  response - iqrf.DpaRawHdpMessage: DPA response object.

Returns:
  array: Array integers corresponding to the previous power level of each selected light. If the unimplemented light was selected the returned value is 0.
*/
iqrf.light.IncrementPower_Response = function ( response )
{
  return iqrf.CheckResponsePnumPcmdDlen( response, iqrf.light.PNUM, '81', -1 );
};

/* Function: iqrf.light.DecrementPower_Request
Encodes DPA request to decrement power of lights.
Same as <iqrf.light.SetPower_Request> but decreases power level. See documentation for details.
*/
iqrf.light.DecrementPower_Request = function ( lights )
{
  return iqrf.light.LightCmd_Request( lights, '02' );
};

/* Function: iqrf.light.DecrementPower_Response
Decodes DPA response from decrementing power of lights.

Parameters:
  response - iqrf.DpaRawHdpMessage: DPA response object.

Returns:
  array: Array integers corresponding to the previous power level of each selected light. If the unimplemented light was selected the returned value is 0.
*/
iqrf.light.DecrementPower_Response = function ( response )
{
  return iqrf.CheckResponsePnumPcmdDlen( response, iqrf.light.PNUM, '82', -1 );
};

//############################################################################################
