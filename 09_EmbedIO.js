﻿//############################################################################################

/* Title: Embedded IO peripheral driver
See also: <https://www.iqrf.org/DpaTechGuide/>
*/

/*DriverDescription
{ 'ID' : 0x09,
  'Type' : 'Standard',
  'Internal' : false,
  'Versions' : [
    { 'Version' : 0, 'Notes' : [
        'Initial release' ]
    }
  ] }
DriverDescription*/

"use strict";
// Namespace: iqrf.embed.io
namespace( 'iqrf.embed.io' );

// --------------
/* Function: iqrf.embed.io.Direction_Request
Encodes DPA request to set direction of IOs.

Parameters:
  ports - attay: Array of objects with the following fields (see DPA documentation https://www.iqrf.org/DpaTechGuide for details):

* port - number:
* mask - number:
* value - number:

Returns:
  iqrf.DpaRawHdpMessage: DPA request object.
*/
iqrf.embed.io.Direction_Request = function ( ports )
{
  var data = '';
  var ports_length = ports.length;
  for ( var index = 0; index < ports_length; index++ )
  {
    if ( data.length !== 0 )
      data += '.';

    var onePort = ports[index];
    data +=
      iqrf.ToHexStringByte( onePort.port ) + '.' +
      iqrf.ToHexStringByte( onePort.mask ) + '.' +
      iqrf.ToHexStringByte( onePort.value );
  }

  return new iqrf.DpaRawHdpMessage( iqrf.PNUM_IO, '00', data );
};

/* Function: iqrf.embed.io.Direction_Response
Decodes DPA response from setting direction of IOs.

Parameters:
  response - iqrf.DpaRawHdpMessage: DPA response object.
*/
iqrf.embed.io.Direction_Response = function ( response )
{
  iqrf.CheckResponsePnumPcmdDlen( response, iqrf.PNUM_IO, '80', 0 );
};

// --------------
/* Function: iqrf.embed.io.Set_Request
Encodes DPA request to set IOs.

Parameters:
  ports - array: Array of objects with the following fields (see DPA documentation https://www.iqrf.org/DpaTechGuide for details):

* port - number:
* mask - number:
* value - number:

Returns:
  iqrf.DpaRawHdpMessage: DPA request object.
*/
iqrf.embed.io.Set_Request = function ( ports )
{
  var data = '';
  var ports_length = ports.length;
  for ( var index = 0; index < ports_length; index++ )
  {
    if ( data.length !== 0 )
      data += '.';

    var onePort = ports[index];
    data +=
      iqrf.ToHexStringByte( onePort.port ) + '.' +
      iqrf.ToHexStringByte( onePort.mask ) + '.' +
      iqrf.ToHexStringByte( onePort.value );
  }
  return new iqrf.DpaRawHdpMessage( iqrf.PNUM_IO, '01', data );
};

/* Function: iqrf.embed.io.Set_Response
Decodes DPA response from setting IOs.

Parameters:
  response - iqrf.DpaRawHdpMessage: DPA response object.
*/
iqrf.embed.io.Set_Response = function ( response )
{
  iqrf.CheckResponsePnumPcmdDlen( response, iqrf.PNUM_IO, '81', 0 );
};

// --------------
/* Function: iqrf.embed.io.Get_Request
Encodes DPA request to read the input state of all supported the MCU ports.

Returns:
  iqrf.DpaRawHdpMessage: DPA request object.
*/
iqrf.embed.io.Get_Request = function ()
{
  return new iqrf.DpaRawHdpMessage( iqrf.PNUM_IO, '02' );
};

/* Function: iqrf.embed.io.Get_Response
Decodes DPA response from reading the input state of all supported the MCU ports.

Parameters:
  response - iqrf.DpaRawHdpMessage: DPA response object.

Returns:
  array: Ports values (see DPA documentation https://www.iqrf.org/DpaTechGuide for details).
*/
iqrf.embed.io.Get_Response = function ( response )
{
  return iqrf.CheckResponsePnumPcmdDlen( response, iqrf.PNUM_IO, '82', -5 );
};

//############################################################################################
