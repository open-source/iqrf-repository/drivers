﻿//############################################################################################

/* Title: IQRF Standards DALI driver
See also: <https://www.iqrfalliance.org/techDocs/>
*/

/*DriverDescription
{ 'ID' : 0x4A,
  'Type' : 'Standard',
  'Internal' : false,
  'Versions' : [
    { 'Version' : 0, 'Notes' : [
        'Initial release' ]
    }
  ] }
DriverDescription*/

"use strict";
// Namespace: iqrf.dali
namespace( 'iqrf.dali' );

// IQRF Standards DALI PNUM value
iqrf.dali.PNUM = '4A';

// FRC commands
/* Const: iqrf.dali.STD_DALI_SEND_FRC
FRC command to return answer of the provided DALI command.
*/
iqrf.dali.STD_DALI_SEND_FRC = 0xE0;

iqrf.dali.DaliSendCmds_Request = function ( commands, pcmd )
{
  var data = '';
  var commands_length = commands.length;
  if ( commands_length < 1 )
    throw new Error( 'iqrf.dali.DaliSendCmds_Request: No DALI commands specified.' );

  for ( var index = 0; index < commands_length; index++ )
  {
    var oneCmd = commands[index];
    if ( typeof oneCmd !== 'number' || oneCmd < 0 || oneCmd > 0xFfFf )
      throw new Error( 'iqrf.dali.DaliSendCmds_Request: Invalid DALI command value ' + oneCmd );

    if ( data.length !== 0 )
      data += '.';
    data += iqrf.ToHexStringByte( oneCmd >> 8 ) + '.' + iqrf.ToHexStringByte( oneCmd & 0xFF );
  }
  return new iqrf.DpaRawHdpMessage( iqrf.dali.PNUM, pcmd, data );
};

/* Function: iqrf.dali.SendCommands_Request
Send DALI commands and returns answers synchronously.

Parameters:
  commands - array: Array of 2 bytes (16 bits) wide values, each representing one DALI command at the standard DALI format YAAAAAAS+DDDDDDDD.

Returns:
  iqrf.DpaRawHdpMessage: DPA request object.
*/
iqrf.dali.SendCommands_Request = function ( commands )
{
  return iqrf.dali.DaliSendCmds_Request( commands, '00' );
};

/* Function: iqrf.dali.SendCommands_Response
Decodes DPA response from sending DALI commands synchronously.

Parameters:
  response - iqrf.DpaRawHdpMessage: DPA response object.

Returns:
  array: Array of objects each representing answer to the respective DALI command.

      * status - number: See IQRF DALI standard for details.
      * value - number: See IQRF DALI standard for details.
*/
iqrf.dali.SendCommands_Response = function ( response )
{
  var responseData = iqrf.CheckResponsePnumPcmdDlen( response, iqrf.dali.PNUM, '80', -2 );

  var result = [];
  var responseData_length = responseData.length;
  for ( var index = 0; index < responseData_length; )
  {
    result[result.length] =
    {
      status: responseData[index++],
      value: responseData[index++]
    };
  }

  return result;
};

/* Function: iqrf.dali.SendCommandsAsync_Request
Send DALI commands and returns answers asynchronously.

Parameters:
  commands - array: Array of 2 bytes (16 bits) wide values, each representing one DALI command at the standard DALI format YAAAAAAS+DDDDDDDD.

Returns:
  iqrf.DpaRawHdpMessage: DPA request object.
*/
iqrf.dali.SendCommandsAsync_Request = function ( commands )
{
  return iqrf.dali.DaliSendCmds_Request( commands, '01' );
};

/* Function: iqrf.dali.SendCommandsAsync_Response
Decodes DPA response from sending DALI commands synchronously.

Parameters:
  response - iqrf.DpaRawHdpMessage: DPA response object.
*/
iqrf.dali.SendCommandsAsync_Response = function ( response )
{
  iqrf.CheckResponsePnumPcmdDlen( response, iqrf.dali.PNUM, '81', 0 );
};

/* Function: iqrf.dali.Frc_Request
Prepares FRC request to execute DALI FRC command.
_Requires FRC embedded peripheral driver_.

Parameters:
  command - number: DALI command, see <iqrf.dali.SendCommands_Request>.
  selectedNodes - array: [optional] Array if integer values corresponding to the selected nodes. Use default value or empty array to select all nodes.

Returns:
  array: 2 items long array. 1st item is a prepared request to initiate the RFC. 2nd item is a prepared request to get additional FRC data from the network. For smaller networks extra FRC result might not be needed.
*/
iqrf.dali.Frc_Request = function ( command, selectedNodes )
{
  var userData = [0x4A, command >> 8, command & 0xFF, 0];

  var result = [];
  result[0] = selectedNodes === undefined || selectedNodes.length === 0 ? iqrf.embed.frc.Send_Request( iqrf.dali.STD_DALI_SEND_FRC, userData ) : iqrf.embed.frc.SendSelective_Request( iqrf.dali.STD_DALI_SEND_FRC, selectedNodes, userData );
  result[1] = iqrf.embed.frc.ExtraResult_Request();
  return result;
};

/* Function: iqrf.dali.Frc_Response
Parses FRC response into DALI answer values.
_Requires FRC embedded peripheral driver_.

Parameters:
  responseFrcSend - iqrf.DpaRawHdpMessage: Parameter value to be passed to iqrf.embed.frc.Send_Response
  responseFrcExtraResult - iqrf.DpaRawHdpMessage: [optional] Value to be passed to iqrf.embed.frc.ExtraResult_Response. For smaller networks extra FRC result might not be needed.

Returns:
  array: Array of objects for every DALI answer. Object array index represent the node address (or index+1 in case selectedNodes parameter was used). The object has the same fields as object at the return  value of <iqrf.dali.SendCommands_Response>. Please see iqrf.embed.frc.ParseResponses for details.
*/
iqrf.dali.Frc_Response = function ( responseFrcSend, responseFrcExtraResult )
{
  var responseFrcExtraResultParsed;
  if ( responseFrcExtraResult !== undefined )
    responseFrcExtraResultParsed = iqrf.embed.frc.ExtraResult_Response( responseFrcExtraResult );
  var parsedFrc = iqrf.embed.frc.ParseResponses( iqrf.dali.STD_DALI_SEND_FRC, iqrf.embed.frc.Send_Response( responseFrcSend ), responseFrcExtraResultParsed );

  var result = [];
  for ( var index in parsedFrc )
  {
    var frcValue = parsedFrc[index];
    result[index] =
    {
      status: frcValue & 0xFF,
      value: frcValue >> 8
    };
  }

  return result;
};

//############################################################################################
