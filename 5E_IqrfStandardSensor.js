﻿//############################################################################################

/* Title: IQRF Standards Sensor driver
See also: <https://www.iqrfalliance.org/techDocs/>
*/

/*DriverDescription
{ 'ID' : 0x5E,
  'Type' : 'Standard',
  'Internal' : false,
  'Versions' : [
    { 'Version' : 15, 'Notes' : [
        '+ New quantities added (TimeSpan, Illuminance, NO2 (nitrogen dioxide), SO2 (sulfur dioxide), CO (carbon monoxide), O3 (ozone), Atmospheric Pressure, Color Temperature, TimeSpanLong, UV Index, Sound Pressure Level, Particulates PM1+PM2.5+PM4+PM10, Data Block, Altitude, Acceleration, pH, Ammonia, Methane, RSSI, TVOC, NOX, Length(float), Temperature(Float), Activity Concentration, Particulates PM40 ).',
        '+ 4 bytes FRC added.',
        '! Fixed negative numbers calculation for signed quantities.' ,
        '! Fixed computation of Earth’s Magnetic Field value.' ,
        '! Fixed FRC conversion of Consumption, TimeSpanLong and DateTime.' ,
        '+ Error sensor values detected and returned as NaN.',
        '+ Sensor enumeration gives list of supported FRC commands.',
        '+ iqrf.sensor.Frc* functions added.',
        '+ Optional parameter writtenData at iqrf.sensor.ReadSensorsWithTypes_Request.',
        '+ iqrf.sensor.ReadSensorsWithTypesFrcValue_AsyncResponse added.',
        '! Fixed Frc calculation of Altitude, Acceleration, Latitude, Longitude.',
        '! id of LowVoltage and ExtraLowVoltage unified to VOLTAGE.',
        '! Short name of Power and Temperature(Float) fixed to P and T respectively.']
    }
  ] }
DriverDescription*/

"use strict";
// Namespace: iqrf.sensor
namespace( 'iqrf.sensor' );

// IQRF Standards Sensor PNUM value
iqrf.sensor.PNUM = '5e';

// IQRF Sensor standard peripheral - sensor types
// 2 bytes
/* Const: iqrf.sensor.STD_SENSOR_TYPE_TEMPERATURE
Temperature sensor.
*/
iqrf.sensor.STD_SENSOR_TYPE_TEMPERATURE = 0x01;
/* Const: iqrf.sensor.STD_SENSOR_TYPE_CO2
Carbon dioxide sensor.
*/
iqrf.sensor.STD_SENSOR_TYPE_CO2 = 0x02;
/* Const: iqrf.sensor.STD_SENSOR_TYPE_VOC
Volatile organic compounds sensor.
*/
iqrf.sensor.STD_SENSOR_TYPE_VOC = 0x03;
/* Const: iqrf.sensor.STD_SENSOR_TYPE_EXTRA_LOW_VOLTAGE
Extra-low voltage sensor.
*/
iqrf.sensor.STD_SENSOR_TYPE_EXTRA_LOW_VOLTAGE = 0x04;
/* Const: iqrf.sensor.STD_SENSOR_TYPE_EARTHS_MAGNETIC_FIELD
Earth’s magnetic field sensor.
*/
iqrf.sensor.STD_SENSOR_TYPE_EARTHS_MAGNETIC_FIELD = 0x05;
/* Const: iqrf.sensor.STD_SENSOR_TYPE_LOW_VOLTAGE
Low voltage sensor.
*/
iqrf.sensor.STD_SENSOR_TYPE_LOW_VOLTAGE = 0x06;
/* Const: iqrf.sensor.STD_SENSOR_TYPE_CURRENT
Current sensor.
*/
iqrf.sensor.STD_SENSOR_TYPE_CURRENT = 0x07;
/* Const: iqrf.sensor.STD_SENSOR_TYPE_POWER
Power sensor.
*/
iqrf.sensor.STD_SENSOR_TYPE_POWER = 0x08;
/* Const: iqrf.sensor.STD_SENSOR_TYPE_MAINS_FREQUENCY
Mains frequency sensor.
*/
iqrf.sensor.STD_SENSOR_TYPE_MAINS_FREQUENCY = 0x09;
/* Const: iqrf.sensor.STD_SENSOR_TYPE_TIMESPAN
Timespan value.
*/
iqrf.sensor.STD_SENSOR_TYPE_TIMESPAN = 0x0A;
/* Const: iqrf.sensor.STD_SENSOR_TYPE_ILLUMINANCE
Illuminance sensor.
*/
iqrf.sensor.STD_SENSOR_TYPE_ILLUMINANCE = 0x0B;
/* Const: iqrf.sensor.STD_SENSOR_TYPE_NO2
Nitrogen dioxide sensor.
*/
iqrf.sensor.STD_SENSOR_TYPE_NO2 = 0x0C;
/* Const: iqrf.sensor.STD_SENSOR_TYPE_SO2
Sulfur dioxide sensor.
*/
iqrf.sensor.STD_SENSOR_TYPE_SO2 = 0x0D;
/* Const: iqrf.sensor.STD_SENSOR_TYPE_CO
Carbon monoxide sensor.
*/
iqrf.sensor.STD_SENSOR_TYPE_CO = 0x0E;
/* Const: iqrf.sensor.STD_SENSOR_TYPE_O3
Ozone sensor.
*/
iqrf.sensor.STD_SENSOR_TYPE_O3 = 0x0F;
/* Const: iqrf.sensor.STD_SENSOR_TYPE_ATMOSPHERIC_PRESSURE
Atmospheric pressure sensor.
*/
iqrf.sensor.STD_SENSOR_TYPE_ATMOSPHERIC_PRESSURE = 0x10;
/* Const: iqrf.sensor.STD_SENSOR_TYPE_COLOR_TEMPERATURE
Color temperature sensor.
*/
iqrf.sensor.STD_SENSOR_TYPE_COLOR_TEMPERATURE = 0x11;
/* Const: iqrf.sensor.STD_SENSOR_TYPE_PARTICULATES_PM25
Particulates PM2.5 sensor.
*/
iqrf.sensor.STD_SENSOR_TYPE_PARTICULATES_PM25 = 0x12;
/* Const: iqrf.sensor.STD_SENSOR_TYPE_SOUND_PRESSURE_LEVEL
Sound pressure level sensor.
*/
iqrf.sensor.STD_SENSOR_TYPE_SOUND_PRESSURE_LEVEL = 0x13;
/* Const: iqrf.sensor.STD_SENSOR_TYPE_ALTITUDE
Altitude sensor.
*/
iqrf.sensor.STD_SENSOR_TYPE_ALTITUDE = 0x14;
/* Const: iqrf.sensor.STD_SENSOR_TYPE_ACCELERATION
Acceleration sensor.
*/
iqrf.sensor.STD_SENSOR_TYPE_ACCELERATION = 0x15;
/* Const: iqrf.sensor.STD_SENSOR_TYPE_NH3
Ammonia sensor.
*/
iqrf.sensor.STD_SENSOR_TYPE_NH3 = 0x16;
/* Const: iqrf.sensor.STD_SENSOR_TYPE_METHANE
Methane sensor.
*/
iqrf.sensor.STD_SENSOR_TYPE_METHANE = 0x17;
/* Const: iqrf.sensor.STD_SENSOR_TYPE_SHORT_LENGTH
Short length sensor.
*/
iqrf.sensor.STD_SENSOR_TYPE_SHORT_LENGTH = 0x18;
/* Const: iqrf.sensor.STD_SENSOR_TYPE_PARTICULATES_PM1
Particulates PM1 sensor.
*/
iqrf.sensor.STD_SENSOR_TYPE_PARTICULATES_PM1 = 0x19;
/* Const: iqrf.sensor.STD_SENSOR_TYPE_PARTICULATES_PM4
Particulates PM4 sensor.
*/
iqrf.sensor.STD_SENSOR_TYPE_PARTICULATES_PM4 = 0x1A;
/* Const: iqrf.sensor.STD_SENSOR_TYPE_PARTICULATES_PM10
Particulates PM10 sensor.
*/
iqrf.sensor.STD_SENSOR_TYPE_PARTICULATES_PM10 = 0x1B;
/* Const: iqrf.sensor.STD_SENSOR_TYPE_TVOC
Total volatile organic compound TVOC sensor.
*/
iqrf.sensor.STD_SENSOR_TYPE_TVOC = 0x1C;
/* Const: iqrf.sensor.STD_SENSOR_TYPE_NOX
Nitrogen oxides NOX.
*/
iqrf.sensor.STD_SENSOR_TYPE_NOX = 0x1D;
/* Const: iqrf.sensor.STD_SENSOR_TYPE_ACTIVITY_CONCENTRATION
Activity concentration.
*/
iqrf.sensor.STD_SENSOR_TYPE_ACTIVITY_CONCENTRATION = 0x1E;
/* Const: iqrf.sensor.STD_SENSOR_TYPE_PARTICULATES_PM40
Particulates PM40 sensor.
*/
iqrf.sensor.STD_SENSOR_TYPE_PARTICULATES_PM40 = 0x20;

//  1 byte
/* Const: iqrf.sensor.STD_SENSOR_TYPE_RELATIVE_HUMIDITY
Relative humidity sensor.
*/
iqrf.sensor.STD_SENSOR_TYPE_RELATIVE_HUMIDITY = 0x80;
/* Const: iqrf.sensor.STD_SENSOR_TYPE_BINARYDATA7
7 bits binary data sensor.
*/
iqrf.sensor.STD_SENSOR_TYPE_BINARYDATA7 = 0x81;
/* Const: iqrf.sensor.STD_SENSOR_TYPE_POWER_FACTOR
Power factor sensor.
*/
iqrf.sensor.STD_SENSOR_TYPE_POWER_FACTOR = 0x82;
/* Const: iqrf.sensor.STD_SENSOR_TYPE_UV_INDEX
UV index sensor.
*/
iqrf.sensor.STD_SENSOR_TYPE_UV_INDEX = 0x83;
/* Const: iqrf.sensor.STD_SENSOR_TYPE_PH
pH sensor.
*/
iqrf.sensor.STD_SENSOR_TYPE_PH = 0x84;
/* Const: iqrf.sensor.STD_SENSOR_TYPE_RSSI
RSSI sensor.
*/
iqrf.sensor.STD_SENSOR_TYPE_RSSI = 0x85;

//  4 bytes
/* Const: iqrf.sensor.STD_SENSOR_TYPE_BINARYDATA30
30 bits binary data sensor.
*/
iqrf.sensor.STD_SENSOR_TYPE_BINARYDATA30 = 0xA0;
/* Const: iqrf.sensor.STD_SENSOR_TYPE_CONSUMPTION
Consumption sensor.
*/
iqrf.sensor.STD_SENSOR_TYPE_CONSUMPTION = 0xA1;
/* Const: iqrf.sensor.STD_SENSOR_TYPE_DATETIME
Date and time value.
*/
iqrf.sensor.STD_SENSOR_TYPE_DATETIME = 0xA2;
/* Const: iqrf.sensor.STD_SENSOR_TYPE_TIMESPAN_LONG
Long timespan value.
*/
iqrf.sensor.STD_SENSOR_TYPE_TIMESPAN_LONG = 0xA3;
/* Const: iqrf.sensor.STD_SENSOR_TYPE_LATITUDE
Latitude value.
*/
iqrf.sensor.STD_SENSOR_TYPE_LATITUDE = 0xA4;
/* Const: iqrf.sensor.STD_SENSOR_TYPE_LONGITUDE
Longitude value.
*/
iqrf.sensor.STD_SENSOR_TYPE_LONGITUDE = 0xA5;
/* Const: iqrf.sensor.STD_SENSOR_TYPE_TEMPERATURE_FLOAT
Temperature sensor.
*/
iqrf.sensor.STD_SENSOR_TYPE_TEMPERATURE_FLOAT = 0xA6;
/* Const: iqrf.sensor.STD_SENSOR_TYPE_LENGTH
Length sensor.
*/
iqrf.sensor.STD_SENSOR_TYPE_LENGTH = 0xA7;

//  Multiple bytes
/* Const: iqrf.sensor.STD_SENSOR_TYPE_DATA_BLOCK
Data block containing proprietary data.
*/
iqrf.sensor.STD_SENSOR_TYPE_DATA_BLOCK = 0xC0;

// FRC commands
/* Const: iqrf.sensor.STD_SENSOR_FRC_2BITS
FRC command to return 2-bits sensor data of the supporting sensor types.
*/
iqrf.sensor.STD_SENSOR_FRC_2BITS = 0x10;
/* Const: iqrf.sensor.STD_SENSOR_FRC_1BYTE
FRC command to return 1-byte wide sensor data of the supporting sensor types.
*/
iqrf.sensor.STD_SENSOR_FRC_1BYTE = 0x90;
/* Const: iqrf.sensor.STD_SENSOR_FRC_2BYTES
FRC command to return 2-bytes wide sensor data of the supporting sensor types.
*/
iqrf.sensor.STD_SENSOR_FRC_2BYTES = 0xE0;
/* Const: iqrf.sensor.STD_SENSOR_FRC_4BYTES
FRC command to return 4-bytes wide sensor data of the supporting sensor types.
*/
iqrf.sensor.STD_SENSOR_FRC_4BYTES = 0xF9;

// Sensor types
iqrf.sensor.SensorTypes = [];
// 2 bytes
iqrf.sensor.SensorTypes[iqrf.sensor.STD_SENSOR_TYPE_TEMPERATURE] =
{
  id: 'TEMPERATURE',
  name: 'Temperature',
  iqrfName: 'Temperature',
  shortName: 'T',
  unit: '°C',
  decimalPlaces: 4,
  frcs: [iqrf.sensor.STD_SENSOR_FRC_1BYTE, iqrf.sensor.STD_SENSOR_FRC_2BYTES],
  helpPage: '0x01-temperature.html'
};
iqrf.sensor.SensorTypes[iqrf.sensor.STD_SENSOR_TYPE_CO2] =
{
  id: 'CO2',
  name: 'Carbon dioxide',
  iqrfName: 'Carbon dioxide',
  shortName: 'CO2',
  unit: 'ppm',
  decimalPlaces: 0,
  frcs: [iqrf.sensor.STD_SENSOR_FRC_1BYTE, iqrf.sensor.STD_SENSOR_FRC_2BYTES],
  helpPage: '0x02-co2-carbon-dioxide.html'
};
iqrf.sensor.SensorTypes[iqrf.sensor.STD_SENSOR_TYPE_VOC] =
{
  id: 'VOC',
  name: 'Volatile organic compound',
  iqrfName: 'Volatile organic compound',
  shortName: 'VOC',
  unit: 'ppm',
  decimalPlaces: 0,
  frcs: [iqrf.sensor.STD_SENSOR_FRC_1BYTE, iqrf.sensor.STD_SENSOR_FRC_2BYTES],
  helpPage: '0x03-voc-volatile-organic-compounds.html'
};
iqrf.sensor.SensorTypes[iqrf.sensor.STD_SENSOR_TYPE_EXTRA_LOW_VOLTAGE] =
{
  id: 'VOLTAGE',
  name: 'Extra-low voltage',
  iqrfName: 'Extra-low voltage',
  shortName: 'U',
  unit: 'V',
  decimalPlaces: 3,
  frcs: [iqrf.sensor.STD_SENSOR_FRC_2BYTES],
  helpPage: '0x04-extra-low-voltage.html'
};
iqrf.sensor.SensorTypes[iqrf.sensor.STD_SENSOR_TYPE_EARTHS_MAGNETIC_FIELD] =
{
  id: 'EARTHS_MAGNETIC_FIELD',
  name: 'Earth’s magnetic field',
  iqrfName: 'Earth’s magnetic field',
  shortName: 'B',
  unit: 'T',
  decimalPlaces: 7,
  frcs: [iqrf.sensor.STD_SENSOR_FRC_2BYTES],
  helpPage: '0x05-earths-magnetic-field.html'
};
iqrf.sensor.SensorTypes[iqrf.sensor.STD_SENSOR_TYPE_LOW_VOLTAGE] =
{
  id: 'VOLTAGE',
  name: 'Low voltage',
  iqrfName: 'Low voltage',
  shortName: 'U',
  unit: 'V',
  decimalPlaces: 4,
  frcs: [iqrf.sensor.STD_SENSOR_FRC_2BYTES],
  helpPage: '0x06-low-voltage.html'
};
iqrf.sensor.SensorTypes[iqrf.sensor.STD_SENSOR_TYPE_CURRENT] =
{
  id: 'CURRENT',
  name: 'Current',
  iqrfName: 'Current',
  shortName: 'I',
  unit: 'A',
  decimalPlaces: 3,
  frcs: [iqrf.sensor.STD_SENSOR_FRC_2BYTES],
  helpPage: '0x07-current.html'
};
iqrf.sensor.SensorTypes[iqrf.sensor.STD_SENSOR_TYPE_POWER] =
{
  id: 'POWER',
  name: 'Power',
  iqrfName: 'Power',
  shortName: 'P',
  unit: 'W',
  decimalPlaces: 2,
  frcs: [iqrf.sensor.STD_SENSOR_FRC_2BYTES],
  helpPage: '0x08-power.html'
};
iqrf.sensor.SensorTypes[iqrf.sensor.STD_SENSOR_TYPE_MAINS_FREQUENCY] =
{
  id: 'MAINS_FREQUENCY',
  name: 'Mains frequency',
  iqrfName: 'Mains frequency',
  shortName: 'f',
  unit: 'Hz',
  decimalPlaces: 3,
  frcs: [iqrf.sensor.STD_SENSOR_FRC_2BYTES],
  helpPage: '0x09-mains-frequency.html'
};
iqrf.sensor.SensorTypes[iqrf.sensor.STD_SENSOR_TYPE_TIMESPAN] =
{
  id: 'TIMESPAN',
  name: 'Timespan',
  iqrfName: 'Timespan',
  shortName: 't',
  unit: 's',
  decimalPlaces: 0,
  frcs: [iqrf.sensor.STD_SENSOR_FRC_2BYTES],
  helpPage: '0x0a-timespan.html'
};
iqrf.sensor.SensorTypes[iqrf.sensor.STD_SENSOR_TYPE_ILLUMINANCE] =
{
  id: 'ILLUMINANCE',
  name: 'Illuminance',
  iqrfName: 'Illuminance',
  shortName: 'Ev',
  unit: 'lx',
  decimalPlaces: 0,
  frcs: [iqrf.sensor.STD_SENSOR_FRC_2BYTES],
  helpPage: '0x0b-illuminance.html'
};
iqrf.sensor.SensorTypes[iqrf.sensor.STD_SENSOR_TYPE_NO2] =
{
  id: 'NO2',
  name: 'Nitrogen dioxide',
  iqrfName: 'Nitrogen dioxide',
  shortName: 'NO2',
  unit: 'ppm',
  decimalPlaces: 3,
  frcs: [iqrf.sensor.STD_SENSOR_FRC_2BYTES],
  helpPage: '0x0c-no2-nitrogen-dioxide.html'
};
iqrf.sensor.SensorTypes[iqrf.sensor.STD_SENSOR_TYPE_SO2] =
{
  id: 'SO2',
  name: 'Sulfur dioxide',
  iqrfName: 'Sulfur dioxide',
  shortName: 'SO2',
  unit: 'ppm',
  decimalPlaces: 3,
  frcs: [iqrf.sensor.STD_SENSOR_FRC_2BYTES],
  helpPage: '0x0d-so2-sulfur-dioxide.html'
};
iqrf.sensor.SensorTypes[iqrf.sensor.STD_SENSOR_TYPE_CO] =
{
  id: 'CO',
  name: 'Carbon monoxide',
  iqrfName: 'Carbon monoxide',
  shortName: 'CO',
  unit: 'ppm',
  decimalPlaces: 2,
  frcs: [iqrf.sensor.STD_SENSOR_FRC_2BYTES],
  helpPage: '0x0e-co-carbon-monoxide.html'
};
iqrf.sensor.SensorTypes[iqrf.sensor.STD_SENSOR_TYPE_O3] =
{
  id: 'O3',
  name: 'Ozone',
  iqrfName: 'Ozone',
  shortName: 'O3',
  unit: 'ppm',
  decimalPlaces: 4,
  frcs: [iqrf.sensor.STD_SENSOR_FRC_2BYTES],
  helpPage: '0x0f-o3-ozone.html'
};
iqrf.sensor.SensorTypes[iqrf.sensor.STD_SENSOR_TYPE_ATMOSPHERIC_PRESSURE] =
{
  id: 'ATMOSPHERIC_PRESSURE',
  name: 'Atmospheric pressure',
  iqrfName: 'Atmospheric pressure',
  shortName: 'p',
  unit: 'hPa',
  decimalPlaces: 4,
  frcs: [iqrf.sensor.STD_SENSOR_FRC_2BYTES],
  helpPage: '0x10-atmospheric-pressure.html'
};
iqrf.sensor.SensorTypes[iqrf.sensor.STD_SENSOR_TYPE_COLOR_TEMPERATURE] =
{
  id: 'COLOR_TEMPERATURE',
  name: 'Color temperature',
  iqrfName: 'Color temperature',
  shortName: 'Tc',
  unit: 'K',
  decimalPlaces: 0,
  frcs: [iqrf.sensor.STD_SENSOR_FRC_2BYTES],
  helpPage: '0x11-color-temperature.html'
};
iqrf.sensor.SensorTypes[iqrf.sensor.STD_SENSOR_TYPE_PARTICULATES_PM25] =
{
  id: 'PARTICULATES_PM25',
  name: 'Particulates PM2.5',
  iqrfName: 'Particulates PM2.5',
  shortName: 'PM2.5',
  unit: 'µg/m3',
  decimalPlaces: 2,
  frcs: [iqrf.sensor.STD_SENSOR_FRC_2BYTES],
  helpPage: '0x12-particulates-pm25.html'
};
iqrf.sensor.SensorTypes[iqrf.sensor.STD_SENSOR_TYPE_SOUND_PRESSURE_LEVEL] =
{
  id: 'SOUND_PRESSURE_LEVEL',
  name: 'Sound pressure level',
  iqrfName: 'Sound pressure level',
  shortName: 'Lp',
  unit: 'dB',
  decimalPlaces: 4,
  frcs: [iqrf.sensor.STD_SENSOR_FRC_2BYTES],
  helpPage: '0x13-sound-pressure-level.html'
};
iqrf.sensor.SensorTypes[iqrf.sensor.STD_SENSOR_TYPE_ALTITUDE] =
{
  id: 'ALTITUDE',
  name: 'Altitude',
  iqrfName: 'Altitude',
  shortName: 'h',
  unit: 'm',
  decimalPlaces: 2,
  frcs: [iqrf.sensor.STD_SENSOR_FRC_2BYTES],
  helpPage: '0x14-altitude.html'
};
iqrf.sensor.SensorTypes[iqrf.sensor.STD_SENSOR_TYPE_ACCELERATION] =
{
  id: 'ACCELERATION',
  name: 'Acceleration',
  iqrfName: 'Acceleration',
  shortName: 'a',
  unit: 'm/s2',
  decimalPlaces: 8,
  frcs: [iqrf.sensor.STD_SENSOR_FRC_2BYTES],
  helpPage: '0x15-acceleration.html'
};
iqrf.sensor.SensorTypes[iqrf.sensor.STD_SENSOR_TYPE_NH3] =
{
  id: 'NH3',
  name: 'Ammonia',
  iqrfName: 'Ammonia',
  shortName: 'NH3',
  unit: 'ppm',
  decimalPlaces: 1,
  frcs: [iqrf.sensor.STD_SENSOR_FRC_2BYTES],
  helpPage: '0x16-nh3-ammonia-in-the-air.html'
};
iqrf.sensor.SensorTypes[iqrf.sensor.STD_SENSOR_TYPE_METHANE] =
{
  id: 'METHANE',
  name: 'Methane',
  iqrfName: 'Methane',
  shortName: 'CH4',
  unit: '%',
  decimalPlaces: 3,
  frcs: [iqrf.sensor.STD_SENSOR_FRC_2BYTES],
  helpPage: '0x17-methane.html'
};
iqrf.sensor.SensorTypes[iqrf.sensor.STD_SENSOR_TYPE_SHORT_LENGTH] =
{
  id: 'LENGTH',
  name: 'Length',
  iqrfName: 'Short length',
  shortName: 'l',
  unit: 'm',
  decimalPlaces: 3,
  frcs: [iqrf.sensor.STD_SENSOR_FRC_2BYTES],
  helpPage: '0x18-short-length.html'
};
iqrf.sensor.SensorTypes[iqrf.sensor.STD_SENSOR_TYPE_PARTICULATES_PM1] =
{
  id: 'PARTICULATES_PM1',
  name: 'Particulates PM1',
  iqrfName: 'Particulates PM1',
  shortName: 'PM1',
  unit: 'µg/m3',
  decimalPlaces: 2,
  frcs: [iqrf.sensor.STD_SENSOR_FRC_2BYTES],
  helpPage: '0x19-particulates-pm1.html'
};
iqrf.sensor.SensorTypes[iqrf.sensor.STD_SENSOR_TYPE_PARTICULATES_PM4] =
{
  id: 'PARTICULATES_PM4',
  name: 'Particulates PM4',
  iqrfName: 'Particulates PM4',
  shortName: 'PM4',
  unit: 'µg/m3',
  decimalPlaces: 2,
  frcs: [iqrf.sensor.STD_SENSOR_FRC_2BYTES],
  helpPage: '0x1a-particulates-pm4.html'
};
iqrf.sensor.SensorTypes[iqrf.sensor.STD_SENSOR_TYPE_PARTICULATES_PM10] =
{
  id: 'PARTICULATES_PM10',
  name: 'Particulates PM10',
  iqrfName: 'Particulates PM10',
  shortName: 'PM10',
  unit: 'µg/m3',
  decimalPlaces: 2,
  frcs: [iqrf.sensor.STD_SENSOR_FRC_2BYTES],
  helpPage: '0x1b-particulates-pm10.html'
};
iqrf.sensor.SensorTypes[iqrf.sensor.STD_SENSOR_TYPE_TVOC] =
{
  id: 'TVOC',
  name: 'Total volatile organic compound',
  iqrfName: 'Total volatile organic compound',
  shortName: 'TVOC',
  unit: 'µg/m3',
  decimalPlaces: 0,
  frcs: [iqrf.sensor.STD_SENSOR_FRC_2BYTES],
  helpPage: '0x1c-tvoc-total-volatile-organic-compound.html'
};
iqrf.sensor.SensorTypes[iqrf.sensor.STD_SENSOR_TYPE_NOX] =
{
  id: 'NOX',
  name: 'Nitrogen oxides',
  iqrfName: 'Nitrogen oxides',
  shortName: 'NOX',
  unit: '',
  decimalPlaces: 0,
  frcs: [iqrf.sensor.STD_SENSOR_FRC_2BYTES],
  helpPage: '0x1d-nox-nitrogen-oxides.html'
};
iqrf.sensor.SensorTypes[iqrf.sensor.STD_SENSOR_TYPE_ACTIVITY_CONCENTRATION] =
{
  id: 'CA',
  name: 'Activity concentration',
  iqrfName: 'Activity concentration',
  shortName: 'CA',
  unit: 'Bq/m3',
  decimalPlaces: 0,
  frcs: [iqrf.sensor.STD_SENSOR_FRC_2BYTES],
  helpPage: '0x1e-activity-concentration.html'
};
iqrf.sensor.SensorTypes[iqrf.sensor.STD_SENSOR_TYPE_PARTICULATES_PM40] =
{
  id: 'PARTICULATES_PM40',
  name: 'Particulates PM40',
  iqrfName: 'Particulates PM40',
  shortName: 'PM40',
  unit: 'µg/m3',
  decimalPlaces: 2,
  frcs: [iqrf.sensor.STD_SENSOR_FRC_2BYTES],
  helpPage: '0x20-particulates-pm40.html'
};

// 1 byte
iqrf.sensor.SensorTypes[iqrf.sensor.STD_SENSOR_TYPE_RELATIVE_HUMIDITY] =
{
  id: 'RELATIVE_HUMIDITY',
  name: 'Relative humidity',
  iqrfName: 'Relative humidity',
  shortName: 'RH',
  unit: '%',
  decimalPlaces: 1,
  frcs: [iqrf.sensor.STD_SENSOR_FRC_1BYTE],
  helpPage: '0x80-relative-humidity.html'
};
iqrf.sensor.SensorTypes[iqrf.sensor.STD_SENSOR_TYPE_BINARYDATA7] =
{
  id: 'BINARYDATA7',
  name: 'Binary data7',
  iqrfName: 'Binary data7',
  shortName: 'bin7',
  unit: '?',
  decimalPlaces: 0,
  frcs: [iqrf.sensor.STD_SENSOR_FRC_2BITS, iqrf.sensor.STD_SENSOR_FRC_1BYTE],
  helpPage: '0x81-binary-data7.html'
};
iqrf.sensor.SensorTypes[iqrf.sensor.STD_SENSOR_TYPE_POWER_FACTOR] =
{
  id: 'POWER_FACTOR',
  name: 'Power factor',
  iqrfName: 'Power factor',
  shortName: 'cosφ',
  unit: '',
  decimalPlaces: 3,
  frcs: [iqrf.sensor.STD_SENSOR_FRC_1BYTE],
  helpPage: '0x82-power-factor.html'
};
iqrf.sensor.SensorTypes[iqrf.sensor.STD_SENSOR_TYPE_UV_INDEX] =
{
  id: 'UV_INDEX',
  name: 'UV index',
  iqrfName: 'UV index',
  shortName: 'UV',
  unit: '',
  decimalPlaces: 3,
  frcs: [iqrf.sensor.STD_SENSOR_FRC_1BYTE],
  helpPage: '0x83-uv-index.html'
};
iqrf.sensor.SensorTypes[iqrf.sensor.STD_SENSOR_TYPE_PH] =
{
  id: 'PH',
  name: 'pH',
  iqrfName: 'pH',
  shortName: 'pH',
  unit: '',
  decimalPlaces: 4,
  frcs: [iqrf.sensor.STD_SENSOR_FRC_1BYTE],
  helpPage: '0x84-ph.html'
};
iqrf.sensor.SensorTypes[iqrf.sensor.STD_SENSOR_TYPE_RSSI] =
{
  id: 'RSSI',
  name: 'RSSI',
  iqrfName: 'RSSI',
  shortName: 'RSSI',
  unit: 'dBm',
  decimalPlaces: 1,
  frcs: [iqrf.sensor.STD_SENSOR_FRC_1BYTE],
  helpPage: '0x85-rssi.html'
};

// 4 bytes
iqrf.sensor.SensorTypes[iqrf.sensor.STD_SENSOR_TYPE_BINARYDATA30] =
{
  id: 'BINARYDATA30',
  name: 'Binary data30',
  iqrfName: 'Binary data30',
  shortName: 'bin30',
  unit: '?',
  decimalPlaces: 0,
  frcs: [iqrf.sensor.STD_SENSOR_FRC_2BYTES, iqrf.sensor.STD_SENSOR_FRC_4BYTES],
  helpPage: '0xa0-binary-data30.html'
};
iqrf.sensor.SensorTypes[iqrf.sensor.STD_SENSOR_TYPE_CONSUMPTION] =
{
  id: 'CONSUMPTION',
  name: 'Consumption',
  iqrfName: 'Consumption',
  shortName: 'E',
  unit: 'Wh',
  decimalPlaces: 0,
  frcs: [iqrf.sensor.STD_SENSOR_FRC_4BYTES],
  helpPage: '0xa1-consumption.html'
};
iqrf.sensor.SensorTypes[iqrf.sensor.STD_SENSOR_TYPE_DATETIME] =
{
  id: 'DATETIME',
  name: 'DateTime',
  iqrfName: 'DateTime',
  shortName: 'DateTime',
  unit: '',
  decimalPlaces: 0,
  frcs: [iqrf.sensor.STD_SENSOR_FRC_4BYTES],
  helpPage: '0xa2-datetime.html'
};
iqrf.sensor.SensorTypes[iqrf.sensor.STD_SENSOR_TYPE_TIMESPAN_LONG] =
{
  id: 'TIMESPAN',
  name: 'Timespan',
  iqrfName: 'Timespan long',
  shortName: 't',
  unit: 's',
  decimalPlaces: 4,
  frcs: [iqrf.sensor.STD_SENSOR_FRC_4BYTES],
  helpPage: '0xa3-timespanlong.html'
};
iqrf.sensor.SensorTypes[iqrf.sensor.STD_SENSOR_TYPE_LATITUDE] =
{
  id: 'LATITUDE',
  name: 'Latitude',
  iqrfName: 'Latitude',
  shortName: 'Lat',
  unit: '°',
  decimalPlaces: 7,
  frcs: [iqrf.sensor.STD_SENSOR_FRC_4BYTES],
  helpPage: '0xa4-latitude.html'
};
iqrf.sensor.SensorTypes[iqrf.sensor.STD_SENSOR_TYPE_LONGITUDE] =
{
  id: 'LONGITUDE',
  name: 'Longitude',
  iqrfName: 'Longitude',
  shortName: 'Long',
  unit: '°',
  decimalPlaces: 7,
  frcs: [iqrf.sensor.STD_SENSOR_FRC_4BYTES],
  helpPage: '0xa5-longitude.html'
};
iqrf.sensor.SensorTypes[iqrf.sensor.STD_SENSOR_TYPE_TEMPERATURE_FLOAT] =
{
  id: 'TEMPERATURE',
  name: 'Temperature',
  iqrfName: 'Temperature float',
  shortName: 'T',
  unit: '°C',
  decimalPlaces: 7,
  frcs: [iqrf.sensor.STD_SENSOR_FRC_4BYTES],
  helpPage: '0xa6-temperature-float.html'
};
iqrf.sensor.SensorTypes[iqrf.sensor.STD_SENSOR_TYPE_LENGTH] =
{
  id: 'LENGTH',
  name: 'Length',
  iqrfName: 'Length',
  shortName: 'l',
  unit: 'm',
  decimalPlaces: 7,
  frcs: [iqrf.sensor.STD_SENSOR_FRC_4BYTES],
  helpPage: '0xa7-length.html'
};

// Multiple bytes
iqrf.sensor.SensorTypes[iqrf.sensor.STD_SENSOR_TYPE_DATA_BLOCK] =
{
  id: 'DATA_BLOCK',
  name: 'Data block',
  iqrfName: 'Data block',
  shortName: 'datablock',
  unit: '?',
  decimalPlaces: 0,
  frcs: [],
  helpPage: '0xc0-data-block.html'
};

/* Function: iqrf.sensor.Enumerate_Request
Encodes DPA request to enumerate sensors.

Returns:
  iqrf.DpaRawHdpMessage: DPA request object.
*/
iqrf.sensor.Enumerate_Request = function ()
{
  return new iqrf.DpaRawHdpMessage( iqrf.sensor.PNUM, '3e' );
};

/* Function: iqrf.sensor.Enumerate_Response
Decodes DPA response from sensor enumeration.

Parameters:
  response - iqrf.DpaRawHdpMessage: DPA response object.

Returns:
  array: Array of objects describing each sensor. The object has the following fields:

* id - string: Value type as "identifier" text.
* type - number: Value type of the sensor (quantity). See IQRF Sensor standard for details.
* name - string: Name of the sensor (quantity).
* shortName - string: Short name of the sensor (quantity). Typically it is a symbol used at physics.
* unit - string: Unit of the quantity. Dimensionless quantity has empty string "".
* decimalPlaces - number: Number of valid decimal places.
* frcs - array: Array of FRC commands supported by the sensor.
* breakdown - array: [optional] see <iqrf.sensor.ReadSensorsWithTypes_Response> for more information.
*/
iqrf.sensor.Enumerate_Response = function ( response )
{
  var responseData = iqrf.CheckResponsePnumPcmdDlen( response, iqrf.sensor.PNUM, 'be', -1 );

  var result = [];
  for ( var index in responseData )
  {
    var sensorType = responseData[index];
    var sensorObj = iqrf.sensor.SensorTypes[sensorType];
    if ( sensorObj === undefined )
      throw new Error( 'iqrf.sensor.Enumerate_Response: Unknown sensor type ' + sensorType );

    var sensorOut =
    {
      id: sensorObj.id,
      type: sensorType,
      name: sensorObj.name,
      shortName: sensorObj.shortName,
      unit: sensorObj.unit,
      decimalPlaces: sensorObj.decimalPlaces,
      frcs: sensorObj.frcs
    };

    var sensorOutIndex = result.length;
    result[sensorOutIndex] = sensorOut;

    if ( iqrf.sensor.FinalizeSensor !== undefined )
      iqrf.sensor.FinalizeSensor( sensorOut, sensorOutIndex );
  }

  return result;
};

iqrf.sensor.Indexes2bitmap = function ( sensorIndexes )
{
  var bitmap = 0;
  for ( var index in sensorIndexes )
    bitmap |= 1 << sensorIndexes[index];

  return iqrf.IntToHexStringBytesArray( bitmap, 4, true );
};

/* Function: iqrf.sensor.ReadSensorsWithTypes_Request
Encodes DPA request to read sensors values.

Parameters:
  sensorIndexes - Specifies sensors to read data from:

* undefined: Missing parameter: reads 1st sensor.
* number: -1: reads all sensors.
* array: array of indexes: specifies sensors to read.

  writtenData - array: [optional] array of 5-bytes-arrays (for each sensor). See IQRF Sensor standards for details.

Returns:
  iqrf.DpaRawHdpMessage: DPA request object.
*/
iqrf.sensor.ReadSensorsWithTypes_Request = function ( sensorIndexes, writtenData )
{
  var result = new iqrf.DpaRawHdpMessage( iqrf.sensor.PNUM, '01' );
  if ( sensorIndexes !== undefined )
  {
    if ( sensorIndexes === -1 )
      result.rdata = 'ff.ff.ff.ff';
    else
      result.rdata = iqrf.sensor.Indexes2bitmap( sensorIndexes );
  }

  if ( writtenData !== undefined )
  {
    var writtenData_length = writtenData.length;
    for ( var index = 0; index < writtenData_length; index++ )
    {
      var oneData = writtenData[index];
      if ( oneData === undefined )
        throw new Error( 'iqrf.sensor.ReadSensorsWithTypes_Request: Written data item at index ' + index + ' are undefined.' );

      result.rdata += iqrf.BytesToHexStringBytesArray( oneData, true );
    }
  }

  return result;
};

/* Function: iqrf.sensor.ReadSensorsWithTypes_Response
Decodes DPA response from sensor value reading.

Parameters:
  response - iqrf.DpaRawHdpMessage: DPA response object.
  request - string: [optional] request used to generate specified "response" parameter. It is a return value from <iqrf.sensor.ReadSensorsWithTypes_Request>. Although this parameter is optional it is needed for breakdown of the "anonymous" sensor types "Binary Data7", "Binary Data30" and "Data Block" by the product specific driver. Please see <iqrf.sensor.FinalizeSensor> for more information.

Returns:
  array: Array of objects for every read sensor. The object has the following fields:

* id - string: Value type as "identifier" text.
* type - number: Value type of the sensor (quantity). See IQRF Sensor standard for details.
* name - string: Name of the sensor (quantity).
* shortName - string: Short name of the sensor (quantity). Typically it is a symbol used at physics.
* value - number: Value of the sensor. It equals to NaN to indicate an error value.
* unit - string: Unit of the quantity. Dimensionless quantity has empty string "".
* decimalPlaces - number: Number of valid decimal places.
* breakdown - array: [optional] See <iqrf.sensor.FinalizeSensor> for more information.

Example:
--- Text
[
  {
    "id" : "TEMPERATURE",
    "type": 1,
    "name": "Temperature",
    "shortName": "T",
    "value": 22.3,
    "unit": "°C",
    "decimalPlaces": 4
  },
  {
    "id" : "RELATIVE_HUMIDITY",
    "type": 128,
    "name": "Relative humidity",
    "shortName": "RH",
    "value": 35,
    "unit": "%",
    "decimalPlaces": 1
  },
  {
    "id" : "MAINS_FREQUENCY",
    "type": 9,
    "name": "Mains frequency",
    "shortName": "f",
    "value": 49.98,
    "unit": "Hz", 3,
    "decimalPlaces": 4
  },
  {
    "id" : "BINARYDATA7",
    "type": 129,
    "name": "Binary data7",
    "shortName": "bin7",
    "value": 88,
    "unit": "?",
    "decimalPlaces": 0,
    "breakdown":
    [
      {
        "id" : "BINARYDATA7",
        "type": 129,
        "name": "Potentiometer",
        "shortName": "pot",
        "value": 30.7,
        "unit": "%",
        "decimalPlaces": 1
      }
    ]
  }
]
---
*/
iqrf.sensor.ReadSensorsWithTypes_Response = function ( response, request )
{
  var responseData = iqrf.CheckResponsePnumPcmdDlen( response, iqrf.sensor.PNUM, '81' );

  if ( request !== undefined )
  {
    var requestData = iqrf.ParseStringBytes( request.rdata );
    var RequestedIndexes = iqrf.BitmapToIndexes( requestData, 0, 3 );
    // If no sensor was requested, then actually the 1st was requested
    if ( RequestedIndexes.length === 0 )
      RequestedIndexes[0] = 0;
  }

  var result = [];
  var responseData_length = responseData.length;
  for ( var index = 0; index < responseData_length; )
  {
    var sensorType = responseData[index++];
    var sensorObj = iqrf.sensor.SensorTypes[sensorType];
    if ( sensorObj === undefined )
      throw new Error( 'iqrf.sensor.ReadSensorsWithTypes_Response: Unknown sensor type ' + sensorType );

    var sensorValue;

    switch ( sensorType )
    {
      default:
        throw new Error( 'iqrf.sensor.ReadSensorsWithTypes_Response: Unimplemented sensor type value ' + sensorType );

      // 2 bytes
      // ----------------
      case iqrf.sensor.STD_SENSOR_TYPE_TEMPERATURE:
      case iqrf.sensor.STD_SENSOR_TYPE_LOW_VOLTAGE:
        sensorValue = responseData[index] + ( responseData[index + 1] * 0x100 );
        sensorValue = sensorValue === 0x8000 ? NaN : iqrf.UInt16toInt16( sensorValue ) / 16.0;
        break;

      case iqrf.sensor.STD_SENSOR_TYPE_ATMOSPHERIC_PRESSURE:
        sensorValue = responseData[index] + ( responseData[index + 1] * 0x100 );
        sensorValue = sensorValue === 0xFfFf ? NaN : sensorValue / 16.0;
        break;

      case iqrf.sensor.STD_SENSOR_TYPE_CO2:
      case iqrf.sensor.STD_SENSOR_TYPE_VOC:
      case iqrf.sensor.STD_SENSOR_TYPE_COLOR_TEMPERATURE:
        sensorValue = responseData[index] + ( responseData[index + 1] * 0x100 );
        if ( sensorValue === 0x8000 )
          sensorValue = NaN;
        break;

      case iqrf.sensor.STD_SENSOR_TYPE_TIMESPAN:
      case iqrf.sensor.STD_SENSOR_TYPE_ILLUMINANCE:
      case iqrf.sensor.STD_SENSOR_TYPE_TVOC:
      case iqrf.sensor.STD_SENSOR_TYPE_NOX:
      case iqrf.sensor.STD_SENSOR_TYPE_ACTIVITY_CONCENTRATION:
        sensorValue = responseData[index] + ( responseData[index + 1] * 0x100 );
        if ( sensorValue === 0xFfFf )
          sensorValue = NaN;
        break;

      case iqrf.sensor.STD_SENSOR_TYPE_EXTRA_LOW_VOLTAGE:
      case iqrf.sensor.STD_SENSOR_TYPE_CURRENT:
        sensorValue = responseData[index] + ( responseData[index + 1] * 0x100 );
        sensorValue = sensorValue === 0x8000 ? NaN : iqrf.UInt16toInt16( sensorValue ) / 1000.0;
        break;

      case iqrf.sensor.STD_SENSOR_TYPE_MAINS_FREQUENCY:
      case iqrf.sensor.STD_SENSOR_TYPE_NO2:
      case iqrf.sensor.STD_SENSOR_TYPE_SO2:
      case iqrf.sensor.STD_SENSOR_TYPE_METHANE:
      case iqrf.sensor.STD_SENSOR_TYPE_SHORT_LENGTH:
        sensorValue = responseData[index] + ( responseData[index + 1] * 0x100 );
        sensorValue = sensorValue === 0xFfFf ? NaN : sensorValue / 1000.0;
        break;

      case iqrf.sensor.STD_SENSOR_TYPE_EARTHS_MAGNETIC_FIELD:
        sensorValue = responseData[index] + ( responseData[index + 1] * 0x100 );
        sensorValue = sensorValue === 0x8000 ? NaN : iqrf.UInt16toInt16( sensorValue ) / 10000000.0;
        break;

      case iqrf.sensor.STD_SENSOR_TYPE_POWER:
        sensorValue = responseData[index] + ( responseData[index + 1] * 0x100 );
        sensorValue = sensorValue === 0xFfFf ? NaN : sensorValue / 4.0;
        break;

      case iqrf.sensor.STD_SENSOR_TYPE_CO:
        sensorValue = responseData[index] + ( responseData[index + 1] * 0x100 );
        sensorValue = sensorValue === 0xFfFf ? NaN : sensorValue / 100.0;
        break;

      case iqrf.sensor.STD_SENSOR_TYPE_O3:
        sensorValue = responseData[index] + ( responseData[index + 1] * 0x100 );
        sensorValue = sensorValue === 0xFfFf ? NaN : sensorValue / 10000.0;
        break;

      case iqrf.sensor.STD_SENSOR_TYPE_PARTICULATES_PM25:
      case iqrf.sensor.STD_SENSOR_TYPE_PARTICULATES_PM1:
      case iqrf.sensor.STD_SENSOR_TYPE_PARTICULATES_PM4:
      case iqrf.sensor.STD_SENSOR_TYPE_PARTICULATES_PM10:
      case iqrf.sensor.STD_SENSOR_TYPE_PARTICULATES_PM40:
        sensorValue = responseData[index] + ( responseData[index + 1] * 0x100 );
        sensorValue = sensorValue === 0x8000 ? NaN : sensorValue / 4.0;
        break;

      case iqrf.sensor.STD_SENSOR_TYPE_SOUND_PRESSURE_LEVEL:
        sensorValue = responseData[index] + ( responseData[index + 1] * 0x100 );
        sensorValue = sensorValue === 0x8000 ? NaN : sensorValue / 16.0;
        break;

      case iqrf.sensor.STD_SENSOR_TYPE_ALTITUDE:
        sensorValue = responseData[index] + ( responseData[index + 1] * 0x100 );
        sensorValue = sensorValue === 0xFfFf ? NaN : sensorValue / 4.0 - 1024.0;
        break;

      case iqrf.sensor.STD_SENSOR_TYPE_ACCELERATION:
        sensorValue = responseData[index] + ( responseData[index + 1] * 0x100 );
        sensorValue = sensorValue === 0x8000 ? NaN : iqrf.UInt16toInt16( sensorValue ) / 256.0;
        break;

      case iqrf.sensor.STD_SENSOR_TYPE_NH3:
        sensorValue = responseData[index] + ( responseData[index + 1] * 0x100 );
        sensorValue = sensorValue === 0xFfFf ? NaN : sensorValue / 10.0;
        break;

      // 1 byte
      // ----------------
      case iqrf.sensor.STD_SENSOR_TYPE_RELATIVE_HUMIDITY:
        sensorValue = responseData[index] === 0xEE ? NaN : ( responseData[index] / 2.0 );
        break;

      case iqrf.sensor.STD_SENSOR_TYPE_BINARYDATA7:
        sensorValue = ( responseData[index] & 0x80 ) !== 0 ? NaN : responseData[index];
        break;

      case iqrf.sensor.STD_SENSOR_TYPE_POWER_FACTOR:
        sensorValue = responseData[index] === 0xEE ? NaN : responseData[index] / 200.0;
        break;

      case iqrf.sensor.STD_SENSOR_TYPE_UV_INDEX:
        sensorValue = responseData[index] === 0xFF ? NaN : responseData[index] / 8.0;
        break;

      case iqrf.sensor.STD_SENSOR_TYPE_PH:
        sensorValue = responseData[index] === 0xFF ? NaN : responseData[index] / 16.0;
        break;

      case iqrf.sensor.STD_SENSOR_TYPE_RSSI:
        sensorValue = responseData[index] === 0xFF ? NaN : ( responseData[index] - 254 ) / 2.0;
        break;

      // 4 bytes
      // ----------------
      case iqrf.sensor.STD_SENSOR_TYPE_BINARYDATA30:
        sensorValue = ( responseData[index + 3] & 0x80 ) !== 0 ? NaN : ( responseData[index] + ( responseData[index + 1] * 0x100 ) + ( responseData[index + 2] * 0x10000 ) + ( responseData[index + 3] * 0x1000000 ) );
        break;

      case iqrf.sensor.STD_SENSOR_TYPE_CONSUMPTION:
        sensorValue = responseData[index] + ( responseData[index + 1] * 0x100 ) + ( responseData[index + 2] * 0x10000 ) + ( responseData[index + 3] * 0x1000000 );
        if ( sensorValue === 0xFfFfFfFf )
          sensorValue = NaN;
        break;

      case iqrf.sensor.STD_SENSOR_TYPE_DATETIME:
        sensorValue = responseData[index] + ( responseData[index + 1] * 0x100 ) + ( responseData[index + 2] * 0x10000 ) + ( responseData[index + 3] * 0x1000000 );
        if ( sensorValue === 0xFfFfFfFf )
          sensorValue = NaN;
        break;

      case iqrf.sensor.STD_SENSOR_TYPE_TIMESPAN_LONG:
        sensorValue = responseData[index] + ( responseData[index + 1] * 0x100 ) + ( responseData[index + 2] * 0x10000 ) + ( responseData[index + 3] * 0x1000000 );
        sensorValue = sensorValue === 0xFfFfFfFf ? NaN : sensorValue / 16.0;
        break;

      case iqrf.sensor.STD_SENSOR_TYPE_LATITUDE:
      case iqrf.sensor.STD_SENSOR_TYPE_LONGITUDE:
        if ( responseData[index + 3] === 0xFF || ( responseData[index + 2] & 0x40 ) === 0x00 )
          sensorValue = NaN;
        else
        {
          sensorValue = responseData[index + 3] + ( ( responseData[index + 2] & 0x3F ) + ( responseData[index] + ( responseData[index + 1] * 0x100 ) ) / 10000 ) / 60;
          if ( ( responseData[index + 2] & 0x80 ) !== 0x00 )
            sensorValue = -sensorValue;
        }
        break;

      case iqrf.sensor.STD_SENSOR_TYPE_TEMPERATURE_FLOAT:
      case iqrf.sensor.STD_SENSOR_TYPE_LENGTH:
        {
          var hex = responseData[index] + ( responseData[index + 1] * 0x100 ) + ( responseData[index + 2] * 0x10000 ) + ( responseData[index + 3] * 0x1000000 );
          var float32 = new Float32Array( 1 );
          var bytes = new Uint8Array( float32.buffer );
          bytes[0] = ( hex >>> 0 ) & 0xFF;
          bytes[1] = ( hex >>> 8 ) & 0xFF;
          bytes[2] = ( hex >>> 16 ) & 0xFF;
          bytes[3] = ( hex >>> 24 ) & 0xFF;
          sensorValue = float32[0];
          break;
        }

      // Multiple bytes
      // ----------------
      case iqrf.sensor.STD_SENSOR_TYPE_DATA_BLOCK:
        {
          sensorValue = [];
          var length = responseData[index];
          for ( var dataIndex = 1; dataIndex <= length; dataIndex++ )
            sensorValue[sensorValue.length] = responseData[index + dataIndex];
          break;
        }
    }

    var oneSensor =
    {
      id: sensorObj.id,
      type: sensorType,
      name: sensorObj.name,
      shortName: sensorObj.shortName,
      value: sensorValue,
      unit: sensorObj.unit,
      decimalPlaces: sensorObj.decimalPlaces
    };

    if ( iqrf.sensor.FinalizeSensor !== undefined )
      iqrf.sensor.FinalizeSensor( oneSensor, RequestedIndexes !== undefined ? RequestedIndexes[result.length] : undefined );

    result[result.length] = oneSensor;

    var sensorDataLength;
    if ( ( sensorType & 0x80 ) === 0 )
      sensorDataLength = 2;
    else if ( ( sensorType & 0xE0 ) === 0x80 )
      sensorDataLength = 1;
    else if ( ( sensorType & 0xE0 ) === 0xA0 )
      sensorDataLength = 4;
    else
      sensorDataLength = responseData[index] + 1;

    index += sensorDataLength;
  }

  return result;
};

/* Function: iqrf.sensor.Frc_Request
Prepares FRC request to read standard sensor values.
_Requires FRC embedded peripheral driver_.

Parameters:
  sensorType - number: Type of sensor (quantity) to read values of. Use iqrf.sensor.STD_SENSOR_TYPE_* constant. Use 0 to specify the sensor only by sensorIndex parameter.
  sensorIndex - number: Index of the sensor. If the sensor type is specified, then it is an index among of all sensors of the specified type. If sensorType is 0 then it is overall sensor index. Some FRC commands might use top 3 bits of this parameter for passing additional data.
  frcCommand - number: One of predefined standard sensor FRC commands. See iqrf.sensor.STD_SENSOR_FRC_* constants.
  selectedNodes - array: [optional] Array if integer values corresponding to the selected nodes. Use default value or empty array to select all nodes and use sleepAfterFrc parameter.
  sleepAfterFrc - object: [optional] Object with the following fields to put the nodes into sleep mode after the FRC is finished:
      * time - number: See DPA documentation https://www.iqrf.org/DpaTechGuide for details.
      * control - number: See DPA documentation https://www.iqrf.org/DpaTechGuide for details.

Returns:
  array: 2 items long array. 1st item is a prepared request to initiate the RFC. 2nd item is a prepared request to get additional FRC data from the network. For smaller networks extra FRC result might not be needed.
*/
iqrf.sensor.Frc_Request = function ( sensorType, sensorIndex, frcCommand, selectedNodes, sleepAfterFrc )
{
  var userData = [0x5E, sensorType, sensorIndex];
  if ( sleepAfterFrc === undefined )
    userData[userData.length] = 0x00;
  else
    userData.push( 0x01, sleepAfterFrc.time & 0xFF, sleepAfterFrc.time >> 8, sleepAfterFrc.control );

  var result = [];
  result[0] = selectedNodes === undefined || selectedNodes.length === 0 ? iqrf.embed.frc.Send_Request( frcCommand, userData ) : iqrf.embed.frc.SendSelective_Request( frcCommand, selectedNodes, userData );
  result[1] = iqrf.embed.frc.ExtraResult_Request();
  return result;
};

/* Function: iqrf.sensor.Frc_ConvertValue
Converts standard sensor FRC value into standard common sensor value.
_Requires FRC embedded peripheral driver_.

Parameters:
  sensorType - number: Type of sensor (quantity) to convert value of. Use iqrf.sensor.STD_SENSOR_TYPE_* constant.
  frcValue - number: sensor (quantity) FRC value to convert.
  frcCommand - number: One of predefined standard sensor FRC commands. See iqrf.sensor.STD_SENSOR_FRC_* constants.

Returns:
  number: sensor value.
*/
iqrf.sensor.Frc_ConvertValue = function ( sensorType, frcValue, frcCommand )
{
  if ( frcCommand !== iqrf.sensor.STD_SENSOR_FRC_2BITS )
    switch ( frcValue )
    {
      case 0:
        throw new Error( 'iqrf.sensor.Frc_ConvertValue: Unsupported FRC value 0' );

      case 1:
        return undefined;

      case 2:
        return NaN;

      case 3:
        throw new Error( 'iqrf.sensor.Frc_ConvertValue: Unsupported FRC value 3' );

      default:
        break;
    }
  else
    switch ( frcValue )
    {
      case 0:
        throw new Error( 'iqrf.sensor.Frc_ConvertValue: Unsupported FRC value 0' );

      case 1:
        return undefined;

      case 2:
      case 3:
        break;

      default:
        throw new Error( 'iqrf.sensor.Frc_ConvertValue: Error FRC value ' + frcValue );
    }

  var value;
  switch ( sensorType )
  {
    default:
      throw new Error( 'iqrf.sensor.Frc_ConvertValue: Unimplemented sensor type value ' + sensorType );

    case iqrf.sensor.STD_SENSOR_TYPE_TEMPERATURE:
      switch ( frcCommand )
      {
        case iqrf.sensor.STD_SENSOR_FRC_1BYTE:
          value = frcValue / 2.0 - 22;
          break;

        case iqrf.sensor.STD_SENSOR_FRC_2BYTES:
          value = iqrf.UInt16toInt16( frcValue ^ 0x8000 ) / 16.0;
          break;
      }
      break;

    case iqrf.sensor.STD_SENSOR_TYPE_LOW_VOLTAGE:
      value = iqrf.UInt16toInt16( frcValue ^ 0x8000 ) / 16.0;
      break;

    case iqrf.sensor.STD_SENSOR_TYPE_ATMOSPHERIC_PRESSURE:
    case iqrf.sensor.STD_SENSOR_TYPE_SOUND_PRESSURE_LEVEL:
    case iqrf.sensor.STD_SENSOR_TYPE_TIMESPAN_LONG:
      value = ( frcValue - 4 ) / 16.0;
      break;

    case iqrf.sensor.STD_SENSOR_TYPE_CO2:
    case iqrf.sensor.STD_SENSOR_TYPE_VOC:
      switch ( frcCommand )
      {
        case iqrf.sensor.STD_SENSOR_FRC_1BYTE:
          value = ( frcValue - 4 ) * 16;
          break;

        case iqrf.sensor.STD_SENSOR_FRC_2BYTES:
          value = frcValue - 4;
          break;
      }
      break;

    case iqrf.sensor.STD_SENSOR_TYPE_COLOR_TEMPERATURE:
    case iqrf.sensor.STD_SENSOR_TYPE_TIMESPAN:
    case iqrf.sensor.STD_SENSOR_TYPE_ILLUMINANCE:
    case iqrf.sensor.STD_SENSOR_TYPE_CONSUMPTION:
    case iqrf.sensor.STD_SENSOR_TYPE_DATETIME:
    case iqrf.sensor.STD_SENSOR_TYPE_TVOC:
    case iqrf.sensor.STD_SENSOR_TYPE_NOX:
    case iqrf.sensor.STD_SENSOR_TYPE_ACTIVITY_CONCENTRATION:
      value = frcValue - 4;
      break;

    case iqrf.sensor.STD_SENSOR_TYPE_EXTRA_LOW_VOLTAGE:
    case iqrf.sensor.STD_SENSOR_TYPE_CURRENT:
      value = iqrf.UInt16toInt16( frcValue ^ 0x8000 ) / 1000.0;
      break;

    case iqrf.sensor.STD_SENSOR_TYPE_MAINS_FREQUENCY:
    case iqrf.sensor.STD_SENSOR_TYPE_NO2:
    case iqrf.sensor.STD_SENSOR_TYPE_SO2:
    case iqrf.sensor.STD_SENSOR_TYPE_METHANE:
    case iqrf.sensor.STD_SENSOR_TYPE_SHORT_LENGTH:
      value = ( frcValue - 4 ) / 1000.0;
      break;

    case iqrf.sensor.STD_SENSOR_TYPE_EARTHS_MAGNETIC_FIELD:
      value = iqrf.UInt16toInt16( frcValue ^ 0x8000 ) / 10000000.0;
      break;

    case iqrf.sensor.STD_SENSOR_TYPE_POWER:
    case iqrf.sensor.STD_SENSOR_TYPE_PARTICULATES_PM25:
    case iqrf.sensor.STD_SENSOR_TYPE_PARTICULATES_PM1:
    case iqrf.sensor.STD_SENSOR_TYPE_PARTICULATES_PM4:
    case iqrf.sensor.STD_SENSOR_TYPE_PARTICULATES_PM10:
    case iqrf.sensor.STD_SENSOR_TYPE_PARTICULATES_PM40:
      value = ( frcValue - 4 ) / 4.0;
      break;

    case iqrf.sensor.STD_SENSOR_TYPE_CO:
      value = ( frcValue - 4 ) / 100.0;
      break;

    case iqrf.sensor.STD_SENSOR_TYPE_O3:
      value = ( frcValue - 4 ) / 10000.0;
      break;

    case iqrf.sensor.STD_SENSOR_TYPE_ALTITUDE:
      value = iqrf.UInt16toInt16( frcValue - 4 ) / 4.0 - 1024;
      break;

    case iqrf.sensor.STD_SENSOR_TYPE_ACCELERATION:
      value = iqrf.UInt16toInt16( frcValue ^ 0x8000 ) / 256.0;
      break;

    case iqrf.sensor.STD_SENSOR_TYPE_NH3:
      value = ( frcValue - 4 ) / 10.0;
      break;

    case iqrf.sensor.STD_SENSOR_TYPE_RELATIVE_HUMIDITY:
      value = ( frcValue - 4 ) / 2.0;
      break;

    case iqrf.sensor.STD_SENSOR_TYPE_BINARYDATA7:
      switch ( frcCommand )
      {
        case iqrf.sensor.STD_SENSOR_FRC_2BITS:
          value = frcValue & 0x01;
          break;

        case iqrf.sensor.STD_SENSOR_FRC_1BYTE:
          value = frcValue - 4;
          break;
      }
      break;

    case iqrf.sensor.STD_SENSOR_TYPE_POWER_FACTOR:
      value = ( frcValue - 4 ) / 200.0;
      break;

    case iqrf.sensor.STD_SENSOR_TYPE_UV_INDEX:
      value = ( frcValue - 4 ) / 8.0;
      break;

    case iqrf.sensor.STD_SENSOR_TYPE_PH:
      value = ( frcValue - 4 ) / 16.0;
      break;

    case iqrf.sensor.STD_SENSOR_TYPE_RSSI:
      value = ( frcValue - 258 ) / 2.0;
      break;

    case iqrf.sensor.STD_SENSOR_TYPE_BINARYDATA30:
      switch ( frcCommand )
      {
        case iqrf.sensor.STD_SENSOR_FRC_2BYTES:
          value = frcValue - 4;
          break;

        case iqrf.sensor.STD_SENSOR_FRC_4BYTES:
          value = frcValue - 4;
          break;
      }
      break;

    case iqrf.sensor.STD_SENSOR_TYPE_LATITUDE:
    case iqrf.sensor.STD_SENSOR_TYPE_LONGITUDE:
      value = ( ( frcValue >>> 24 ) & 0xFF ) + ( ( ( frcValue >>> 16 ) & 0x3F ) + ( frcValue & 0xFfFf ) / 10000 ) / 60;
      if ( ( frcValue & 0x800000 ) !== 0 )
        value = -value;
      break;

    case iqrf.sensor.STD_SENSOR_TYPE_TEMPERATURE_FLOAT:
    case iqrf.sensor.STD_SENSOR_TYPE_LENGTH:
      var float32 = new Float32Array( 1 );
      var bytes = new Uint8Array( float32.buffer );
      frcValue -= 4;
      bytes[0] = ( frcValue >>> 0 ) & 0xFF;
      bytes[1] = ( frcValue >>> 8 ) & 0xFF;
      bytes[2] = ( frcValue >>> 16 ) & 0xFF;
      bytes[3] = ( frcValue >>> 24 ) & 0xFF;
      value = float32[0];
      break;
  }
  return value;
}

/* Function: iqrf.sensor.Frc_Response
Parses FRC response into sensor values. See <iqrf.sensor.Frc_Request> for more details.
_Requires FRC embedded peripheral driver_.

Parameters:
  sensorType - number: Type of sensor (quantity) to read values of. The type must be alway specified even when 0 was passed as sensorType parameter at iqrf.sensor.Frc_Request. If an optional parameter frcSendRequest, then the sensorType parameter is internally assigned from frcSendRequest so it does not have to be specified at all.
  frcCommand - number: One of predefined standard sensor FRC commands. Use the same value that was passed to iqrf.sensor.Frc_Request. If an optional parameter frcSendRequest, then the frcCommand parameter is internally assigned from frcSendRequest so it does not have to be specified at all.
  responseFrcSend - object: Response for the 1st request from iqrf.sensor.Frc_Request.
  responseFrcExtraResult - object: [optional] Response for the 2nd request from iqrf.sensor.Frc_Request.
  frcSendRequest - string: [optional] FrcSend request that was used to generate specified responseFrcSend. It is a return value[0] from <iqrf.sensor.Frc_Request>. Although this parameter is optional, it is needed for breakdown of the "anonymous" sensor types "Binary Data7", "Binary Data30" and "Data Block" by the product specific driver. Please see iqrf.sensor.FinalizeSensor for more information.

Returns:
  array: Array of objects for every read sensor. Object array index represent the node address (or index+1 in case selectedNodes parameter was used). The object has the same fields as object at the return  value of <iqrf.sensor.ReadSensorsWithTypes_Response>. If Node does not response then the array item equals null. Please note there is always a dummy item at the index 0.
*/
iqrf.sensor.Frc_Response = function ( sensorType, frcCommand, responseFrcSend, responseFrcExtraResult, frcSendRequest )
{
  var responseFrcExtraResultParsed;
  if ( responseFrcExtraResult !== undefined )
    responseFrcExtraResultParsed = iqrf.embed.frc.ExtraResult_Response( responseFrcExtraResult );
  var parsedFrc = iqrf.embed.frc.ParseResponses( frcCommand, iqrf.embed.frc.Send_Response( responseFrcSend ), responseFrcExtraResultParsed );

  if ( frcSendRequest !== undefined )
  {
    var requestData = iqrf.ParseStringBytes( frcSendRequest.rdata );

    // Get FRC command from request
    frcCommand = requestData[0];

    // user-data offset according to original the FRC command (plain or selective FRC)
    var userDataOffset = frcSendRequest.pcmd === '00' ? 0 : 30;

    // If specific sensor type requested, use it
    if ( requestData[2 + userDataOffset] !== 0 )
      sensorType = requestData[2 + userDataOffset];

    if ( iqrf.sensor.FinalizeSensor !== undefined && iqrf.sensor.SensorsList !== undefined )
    {
      // Sensor index from request
      var sensorIndex = requestData[3 + userDataOffset] & 0x1f;
      // Index from the list
      var listIndex = 0;
      for ( ; listIndex < iqrf.sensor.SensorsList.length; listIndex++ )
      {
        // Any sensor type or exact match?
        if ( requestData[2 + userDataOffset] === 0 || requestData[2 + userDataOffset] === iqrf.sensor.SensorsList[listIndex] )
        {
          // Index counted?
          if ( sensorIndex === 0 )
          {
            // Got the sensor index!
            sensorIndex = listIndex;

            // Flag the sensor index was found
            listIndex = undefined;
            break;
          }
          // Next index
          sensorIndex--;
        }
      }

      // Sensor index not found?
      if ( listIndex !== undefined )
        sensorIndex = undefined;
    }
  }

  var sensorObj = iqrf.sensor.SensorTypes[sensorType];
  if ( sensorObj === undefined )
    throw new Error( 'iqrf.sensor.Frc_Response: Unknown sensor type ' + sensorType );

  if ( -1 === iqrf.indexOf( sensorObj.frcs, frcCommand ) )
    throw new Error( 'iqrf.sensor.Frc_Response: Unsupported FRC command ' + frcCommand + ' by sensor type ' + sensorType + '=' + sensorObj.name );

  var result = [];
  for ( var index in parsedFrc )
  {
    var frcValue = parsedFrc[index];
    if ( frcValue === 0 )
      result[index] = null;
    else
    {
      var value = iqrf.sensor.Frc_ConvertValue( sensorType, frcValue, frcCommand );

      if ( value !== undefined )
      {
        var oneSensor =
        {
          id: sensorObj.id,
          type: sensorType,
          name: sensorObj.name,
          shortName: sensorObj.shortName,
          value: value,
          unit: sensorObj.unit,
          decimalPlaces: sensorObj.decimalPlaces
        };

        if ( iqrf.sensor.FinalizeSensor !== undefined )
          iqrf.sensor.FinalizeSensor(
            // Sensor object
            oneSensor,
            // Sensor index
            sensorIndex,
            // FRC command
            frcCommand,
            // Extended FRC data if specified
            requestData !== undefined ? ( requestData[3] >> 5 ) : undefined );

        result[index] = oneSensor;
      }
    }
  }

  return result;
};

/* Function: iqrf.sensor.ReadSensorsWithTypesFrcValue_AsyncResponse
Decodes asynchronous DPA standard sensor beaming response.
_Requires FRC embedded peripheral driver_.

Parameters:
  response - iqrf.DpaRawHdpMessage: DPA response object.

Returns:
  array: Array of objects for every read sensor. See return value of <iqrf.sensor.ReadSensorsWithTypes_Response> for details.
*/

iqrf.sensor.ReadSensorsWithTypesFrcValue_AsyncResponse = function ( response )
{
  var responseData = iqrf.CheckResponsePnumPcmdDlen( response, iqrf.sensor.PNUM, '7B' );

  var result = [];
  var responseData_length = responseData.length;
  var sensorIndex = 0;
  for ( var index = 0; index < responseData_length; sensorIndex++ )
  {
    var sensorType = responseData[index++];
    var sensorObj = iqrf.sensor.SensorTypes[sensorType];
    if ( sensorObj === undefined )
      throw new Error( 'iqrf.sensor.ReadSensorsWithTypesFrcValue_Response: Unknown sensor type ' + sensorType );

    var sensorDataLength, frcCommand;
    if ( ( sensorType & 0x80 ) === 0x00 )
    {
      sensorDataLength = 2;
      frcCommand = iqrf.sensor.STD_SENSOR_FRC_2BYTES;
    }
    else
      switch ( sensorType & 0xE0 )
      {
        default:
          throw new Error( 'iqrf.sensor.ReadSensorsWithTypesFrcValue_Response: Invalid sensor type ' + sensorType );

        case 0x80:
          sensorDataLength = 1;
          frcCommand = iqrf.sensor.STD_SENSOR_FRC_1BYTE;
          break;

        case 0xA0:
          sensorDataLength = 4;
          frcCommand = iqrf.sensor.STD_SENSOR_FRC_4BYTES;
          break;
      }

    var frcValue = 0;
    for ( var bytesLoop = sensorDataLength, order = 1; bytesLoop !== 0; bytesLoop--, order *= 0x100 )
      frcValue += responseData[index++] * order;

    var sensorValue = iqrf.sensor.Frc_ConvertValue( sensorType, frcValue, frcCommand );

    var oneSensor =
    {
      id: sensorObj.id,
      type: sensorType,
      name: sensorObj.name,
      shortName: sensorObj.shortName,
      value: sensorValue,
      unit: sensorObj.unit,
      decimalPlaces: sensorObj.decimalPlaces
    };

    if ( iqrf.sensor.FinalizeSensor !== undefined )
      iqrf.sensor.FinalizeSensor( oneSensor, sensorIndex );

    result[result.length] = oneSensor;
  }
  return result;
}

/* Function: iqrf.sensor.FinalizeSensor
This function should be implemented in the product driver in order to breakdown "anonymous" sensor types "Binary Data7", "Binary Data30" and "Data Block". The function is called by <iqrf.sensor.ReadSensorsWithTypes_Response>, <iqrf.sensor.Enumerate_Response>, and <iqrf.sensor.Frc_Response>. Also variable <iqrf.sensor.SensorsList> should be declared in the product driver. The function might also add custom product specific properties to the sensor object.

When the function is called from <iqrf.sensor.Enumerate_Response> then sensor.value is undefined, therefore "value" property should not be created at the resulting breakdown sensor object.

This function might modify the sensor object. It must not alter the properties "id", "type", "name", "shortName", "value", "unit" and "decimalPlaces", but it might add custom product specific properties. In case of anonymous sensor data types it is supposed to add property "breakdown" that has the same format as return value at <iqrf.sensor.ReadSensorsWithTypes_Response>. Each array member must share the same sensor type as the "sensor" parameter. Please also note, that the "value" property must be number.

Parameters:
  sensor - object: See member of the return array at <iqrf.sensor.ReadSensorsWithTypes_Response>.
  index - number: Sensor index. It might be undefined if the required parameters were not passed to the functions that call this function.
  frcCommand - number: [optional] When sensor value from the FRC result is to be processed, then this parameter is FRC command.
  extendedFrcData - number: [optional] When sensor value from the FRC result is to be processed, then this parameter is extended FRC data according to the IQRF Sensor standards FRC that is used by some standard quantities.

Example:
--- Text
// "sensor" parameter in entry:
{
    "id" : "BINARYDATA7",
    "type": 129,
    "name": "Binary data7",
    "shortName": "bin7",
    "value": 88,
    "unit": "?",
    "decimalPlaces": 0
}
---
--- Text
// "sensor" parameter on exit:
{
    "id" : "BINARYDATA7",
    "type": 129,
    "name": "Binary data7",
    "shortName": "bin7",
    "value": 88,
    "unit": "?",
    "decimalPlaces": 0,
    "breakdown":
    [
      {
        "id" : "BINARYDATA7",
        "type": 129,
        "name": "Potentiometer",
        "shortName": "pot",
        "value": 30.7,
        "unit": "%",
        "decimalPlaces": 1
      }
    ]
}
---
*/

/* Variable: iqrf.sensor.SensorsList
This variable declared in the product driver is an array listing all supported sensor types in the right order. The variable should be implemented in order to breakdown "anonymous" sensor types "Binary Data7", "Binary Data30" and "Data Block".

Example:
--- Text
// This product sensors list
iqrf.sensor.SensorsList =
  [
    iqrf.sensor.STD_SENSOR_TYPE_TEMPERATURE,  // 0
    iqrf.sensor.STD_SENSOR_TYPE_TEMPERATURE,  // 1
    iqrf.sensor.STD_SENSOR_TYPE_BINARYDATA7,  // 2 : Light indicator
    iqrf.sensor.STD_SENSOR_TYPE_BINARYDATA7   // 3 : Potentiometer
  ];
---
*/

//############################################################################################
