﻿//############################################################################################

/* Title: Embedded DPA exploration driver
See also: <https://www.iqrf.org/DpaTechGuide/>
*/

/*DriverDescription
{ 'ID' : 0xFF,
  'Type' : 'Standard',
  'Internal' : false,
  'Versions' : [
    { 'Version' : 0, 'Notes' : [
        '1st line',
        '2nd line' ]
    }
  ] }
DriverDescription*/

"use strict";
// Namespace: iqrf.embed.explore
namespace( 'iqrf.embed.explore' );

/* Function: iqrf.embed.explore.Enumerate_Request
Encodes DPA request to enumerate DPA peripherals.

Returns:
  iqrf.DpaRawHdpMessage: DPA request object.
*/
iqrf.embed.explore.Enumerate_Request = function ()
{
  return new iqrf.DpaRawHdpMessage( iqrf.PNUM_Enumeration, '3f' );
};

/* Function: iqrf.embed.explore.Enumerate_Response
Decodes DPA response from enumerating DPA peripherals.

Parameters:
  response - iqrf.DpaRawHdpMessage: DPA response object.

Returns:
  object: Object with the following fields (see DPA documentation https://www.iqrf.org/DpaTechGuide/ for details):

* dpaVer - number:
* perNr - number:
* embeddedPers - number:
* hwpid - number:
* hwpidVer - number:
* flags - number:
* userPer - array:
*/
iqrf.embed.explore.Enumerate_Response = function ( response )
{
  var responseData = iqrf.CheckResponsePnumPcmdDlen( response, iqrf.PNUM_Enumeration, 'bf', -12 );
  var result =
    {
      dpaVer: responseData[0] + ( responseData[1] * 0x100 ),
      perNr: responseData[2],
      embeddedPers: iqrf.BitmapToIndexes( responseData, 3, 6 ),
      hwpid: responseData[7] + ( responseData[8] * 0x100 ),
      hwpidVer: responseData[9] + ( responseData[10] * 0x100 ),
      flags: responseData[11],
      userPer: iqrf.BitmapToIndexes( responseData, 12, responseData.length - 1, 0x20 )
    };

  return result;
};

/* Function: iqrf.embed.explore.PeripheralInformation_Request
Encodes DPA request to get information about a peripheral.

Parameters:
  per - number: Peripheral to get information about.

Returns:
  iqrf.DpaRawHdpMessage: DPA request object.
*/
iqrf.embed.explore.PeripheralInformation_Request = function ( per )
{
  return new iqrf.DpaRawHdpMessage( iqrf.ToHexStringByte( per ), '3f' );
};

/* Function: iqrf.embed.explore.PeripheralInformation_Response
Decodes DPA response from getting information about a peripheral.

Parameters:
  per - number: Peripheral the information is about.
  response - iqrf.DpaRawHdpMessage: DPA response object.

Returns:
  object: Object with the following fields (see DPA documentation https://www.iqrf.org/DpaTechGuide/ for details):

* perTe - number:
* perT - number:
* par1 - number:
* par2 - number:
*/
iqrf.embed.explore.PeripheralInformation_Response = function ( per, response )
{
  var responseData = iqrf.CheckResponsePnumPcmdDlen( response, iqrf.ToHexStringByte( per ), 'bf', 4 );
  var result =
    {
      perTe: responseData[0],
      perT: responseData[1],
      par1: responseData[2],
      par2: responseData[3]
    };

  return result;
};

/* Function: iqrf.embed.explore.MorePeripheralsInformation_Request
Encodes DPA request to get information about more peripherals.

Parameters:
  per - number: 1st peripheral to get information about.

Returns:
  iqrf.DpaRawHdpMessage: DPA request object.
*/
iqrf.embed.explore.MorePeripheralsInformation_Request = function ( per )
{
  return new iqrf.DpaRawHdpMessage( 'ff', iqrf.ToHexStringByte( per ) );
};

/* Function: iqrf.embed.explore.MorePeripheralsInformation_Response
Decodes DPA response from getting information about more peripherals.

Parameters:
  per - number: 1st peripheral the information is about.
  response - iqrf.DpaRawHdpMessage: DPA response object.

Returns:
  array: Array of objects with the following fields (see DPA documentation https://www.iqrf.org/DpaTechGuide/ for details):

* perTe - number:
* perT - number:
* par1 - number:
* par2 - number:
*/
iqrf.embed.explore.MorePeripheralsInformation_Response = function ( per, response )
{
  var responseData = iqrf.CheckResponsePnumPcmdDlen( response, 'ff', iqrf.ToHexStringByte( parseInt( per, 16 ) | 0x80 ) );

  var result = [];
  var responseData_length = responseData.length;
  for ( var index = 0; index < responseData_length; index += 4 )
    result[result.length] =
      {
        perTe: responseData[index + 0],
        perT: responseData[index + 1],
        par1: responseData[index + 2],
        par2: responseData[index + 3]
      };

  return result;
};

//############################################################################################
